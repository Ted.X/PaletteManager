package ge.init5.spektra;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import ge.init5.spektra.models.FireUser;
import ge.init5.spektra.services.FirebasePaletteDatabase.PaletteBase;
import ge.init5.spektra.utils.Logger;

/**
 * A login screen that offers login via email/password.
 */
public class AuthActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener{

    private static final String TAG = AuthActivity.class.getSimpleName();

    private static final int RC_SIGN_IN = 9001;

    @BindView(R.id.sign_in_btn_google)
    public SignInButton googleSignInButton;

    @BindView(R.id.sign_in_btn_facebook)
    public LoginButton facebookSignInButton;


    @BindView(R.id.email_log_in_button)
    public Button emailLoginButton;

    @BindView(R.id.sign_up_button)
    public Button signUpButton;

    @BindView(R.id.auth_flipper_txt)
    public TextView authFlipperText;

    @BindView(R.id.auth_form_flipper)
    public ViewFlipper authFormFlipper;

    @BindView(R.id.include_loading)
    public View loader;

    private static int CURRENT_VIEW_FLIPPER_CHILD = 0;

    private FirebaseAuth mAuth;

    private FirebaseAuth.AuthStateListener mAuthListener;

    private CallbackManager mCallBackManager;

    public static GoogleApiClient mGoogleApiClient;



    private boolean loginEmailWritten = false;
    private boolean loginPasswordWritten = false;

    private boolean autoLogin = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        Logger.FireLog(TAG, "Auth Activity Started.");

        ButterKnife.bind(this);

        // Firebase Persistence
        PaletteBase.getInstance().setPersistenceEnabled();

        // Initialize Facebook Sign-In
        mCallBackManager = CallbackManager.Factory.create();
        facebookSignInButton.setReadPermissions("email", "public_profile");

        facebookSignInButton.registerCallback(mCallBackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Logger.FireLog(TAG, "Facebook login succeeded with result: " + loginResult);
                loader.setVisibility(View.VISIBLE);


                firebaseAuthWithFacebook(AccessToken.getCurrentAccessToken());
            }

            @Override
            public void onCancel() {
                Logger.FireLog(TAG, "Facebook login canceled.");
                Toast.makeText(AuthActivity.this, "Facebook Log In Canceled!", Toast.LENGTH_SHORT).show();
                loader.setVisibility(View.GONE);
            }

            @Override
            public void onError(FacebookException error) {
                Logger.FireLog(TAG, "Facebook login did not succeed with error: " + error.toString());
                Toast.makeText(AuthActivity.this, "Facebook Log In Failed!", Toast.LENGTH_SHORT).show();
                loader.setVisibility(View.GONE);
            }
        });

        Logger.FireLog(TAG, "Facebook Sign In Initialized!");

        // Initialize Google Sign-In
        googleSignInButton.setOnClickListener(this);
        googleSignInButton.setSize(SignInButton.SIZE_STANDARD);

        TextView textView = (TextView) googleSignInButton.getChildAt(0);
        textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        textView.setPadding(0,0,0,0);

        GoogleSignInOptions gso = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken("752543778104-udub443kce25koh3bfoerrepj0uui4pc.apps.googleusercontent.com")
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();



        Logger.FireLog(TAG, "Google Sign In Initialized!");

        emailLoginButton.setOnClickListener(this);
        signUpButton.setOnClickListener(this);
        authFlipperText.setOnClickListener(this);


        // Initialize Firebase Sign-In
        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener(){
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // FireUser is signed in
                    Logger.FireLog(TAG, "User with Uid: " + user.getUid() + " has signed in automatically.");
                    PaletteBase.getInstance().firebaseUser = user;
                    if(autoLogin) {
                        StartActivity(MainActivity.class);
//                        finish();
                    }
                } else {
                    autoLogin = false;
                    // FireUser is signed out
                    Logger.FireLog(TAG, "User didn't sign in automatically.");
                }
            }
        };

        Logger.FireLog(TAG, "Firebase Sign In Initialized!");

        EditText email = (EditText) findViewById(R.id.login_email);
        EditText password = (EditText) findViewById(R.id.login_password);
        email.addTextChangedListener(fieldListener(email, "email"));
        password.addTextChangedListener(fieldListener(password, "password"));
    }



    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);

    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mAuthListener != null){
            mAuth.removeAuthStateListener(mAuthListener);
        }
//        if(mAuthSessionHelper != null)
//            mAuthSessionHelper.onStop();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
//        if(mAuthSessionHelper != null)
//            mAuthSessionHelper.onPause();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
//        if(mAuthSessionHelper != null)
//            mAuthSessionHelper.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);




        if (resultCode == RESULT_OK) {

            if(mCallBackManager.onActivityResult(requestCode, resultCode, data)){
                loader.setVisibility(View.VISIBLE);
            }

            if (requestCode == RC_SIGN_IN) {
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

                if (result.isSuccess()) {
                    firebaseAuthWithGoogle(result.getSignInAccount());
                }


            }
//            else if(requestCode == REQ_CODE_CSDK_USER_AUTH){
//
//                final AdobeAuthUserProfile profile = mUXAuthManager.getUserProfile();
//
//                Logger.LogDebug(TAG, "USER: " + profile.getEmail() + " Password: " + profile.getAdobeID());
//
//
//                // Get next query for single value listener on users
//                Query usersQueryByUid = PaletteBase.getInstance().getFirebaseDatabaseRoot().child("users").orderByChild("email").startAt(profile.getEmail()).endAt(profile.getEmail());
//                usersQueryByUid.addListenerForSingleValueEvent(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        if(dataSnapshot.exists()){
//                            signInWithEmailAndPassword(profile.getEmail(), profile.getAdobeID());
//                            mAuthSessionHelper = null;
//                        }
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//                        //user doesnt excists
//                        mAuth.createUserWithEmailAndPassword(profile.getEmail(), profile.getAdobeID()).
//                                addOnCompleteListener(new OnCompleteListener<AuthResult>() {
//                                    @Override
//                                    public void onComplete(@NonNull Task<AuthResult> task) {
//                                        if (!task.isSuccessful()) {
//                                            Logger.FireLog(TAG, "User registration with email failed.");
//                                            Toast.makeText(AuthActivity.this, R.string.auth_failed, Toast.LENGTH_SHORT).show();
//                                        }else{
//                                            mAuthSessionHelper = null;
//                                            Logger.FireLog(TAG, "User registration with email succeeded.");
//                                            Toast.makeText(AuthActivity.this, R.string.auth_success, Toast.LENGTH_SHORT).show();
//                                            // Write user
//                                            writeNewUser(profile.getDisplayName(), profile.getEmail());
//                                            startActivity(new Intent(AuthActivity.this, CropActivity.class));
//                                            finish();
//                                        }
//                                    }
//                                });
//                    }
//                });
//
//
//            }
        }
        loader.setVisibility(View.GONE);


    }

    public void StartActivity(Class c){
        Intent intent = new Intent(this, c);
        Intent receivedIntent = getIntent();
        Uri data = receivedIntent.getData();
        boolean hasColors = receivedIntent.hasExtra("colors");
        if(data != null || hasColors){
            intent.putExtras(receivedIntent.getExtras());
        }
        startActivity(intent);
    }
    public void firebaseAuthWithGoogle(final GoogleSignInAccount account){
        Logger.FireLog(TAG, "Attempting user registration/login with Google account ID: " + account.getId());

        final AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            Toast.makeText(AuthActivity.this, R.string.auth_failed,
                                    Toast.LENGTH_SHORT).show();
                            Logger.FireLog(TAG, "Google registration/login failed.");
                        }else{
                            Toast.makeText(AuthActivity.this, "Logged In", Toast.LENGTH_LONG).show();
                            Logger.FireLog(TAG, "Google registration/login succeeded.");
                            writeNewUser(account.getDisplayName(), account.getEmail());

                            StartActivity(MainActivity.class);
//                            finish();
                        }
                        loader.setVisibility(View.GONE);
                    }
                });
    }

    private void firebaseAuthWithFacebook(final AccessToken token) {
        Logger.FireLog(TAG, "Attempting user registration/login with Facebook.");
        Logger.LogDebug(TAG, "TOKEN: " + token);
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            Logger.LogWarn(TAG, "signInWithCredential", task.getException());
                            Logger.FireLog(TAG, "Facebook registration/login failed.");
                            Toast.makeText(AuthActivity.this, "Authentication  failed.",
                                    Toast.LENGTH_SHORT).show();
                        }else{

//                            final String[] fbProfile = new String[2];
//
                            // Send request to Graph to get user data
                            GraphRequest request = GraphRequest.newMeRequest(
                                    token,
                                    new GraphRequest.GraphJSONObjectCallback(){
                                        @Override
                                        public void onCompleted(JSONObject object, GraphResponse response){
                                            String name = "";
                                            String email = "";
                                            try{
                                                name = object.getString("name");
                                                email = object.getString("email");
//                                                finish();
                                            } catch (JSONException e) {
                                                if(email == "")
                                                    email = name + "@facebook.com";
                                            }finally {
                                                loader.setVisibility(View.GONE);
                                                Logger.FireLog(TAG,
                                                        "Facebook registration/login succeeded. (User: "
                                                                + name + ", Email: " + email + ")");
                                                writeNewUser(name, email);
                                                StartActivity(MainActivity.class);
                                            }


                                        }
                                    });

                            Bundle parameters = new Bundle();
                            parameters.putString("fields", "name,email,birthday ");
                            request.setParameters(parameters);
                            request.executeAsync();
                        }
                    }
                });
    }

    public boolean signUpWithEmailAndUsername(){
        Logger.FireLog(TAG, "Attempting user registration using Email.");

        // Get some shiet
        final EditText usernameTxt = (EditText)findViewById(R.id.signup_username);
        EditText emailTxt = (EditText)findViewById(R.id.signup_email);
        EditText passwordTxt = (EditText)findViewById(R.id.signup_password);
        EditText passwordRepeatTxt = (EditText)findViewById(R.id.signup_password_repeat);

        final String username = usernameTxt.getText().toString();
        final String email = emailTxt.getText().toString();
        final String password = passwordTxt.getText().toString();
        String passwordRepeat = passwordRepeatTxt.getText().toString();


        Pattern np = Pattern.compile("[^a-zA-Z0-9]+", Pattern.CASE_INSENSITIVE);

        // Validate Username
        if (username == null || username.isEmpty()){
            usernameTxt.setError("Required");
            return false;
        }else {
            if (username.length() < 6 || username.length() > 30){
                usernameTxt.setError("Length must be between 6-30");
                return false;
            }else if (np.matcher(username).find()){
                usernameTxt.setError("Avoid using non-alphabetic characters in username.");
                return false;
            }
        }

        // Require Email
        if (email == null || email.isEmpty()){
            emailTxt.setError("Required");
            return false;
        }

        // Require Password
        if (password == null || password.isEmpty()){
            passwordTxt.setError("Required");
            return false;
        }else {
            // Check password for regex
            Pattern p = Pattern.compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$");
            Matcher m = p.matcher(password);

            if (!m.find()) {
                passwordTxt.setError("Password Constraints:\n" +
                        "* At least 8 chars\n" +
                        "* Contains at least one digit\n" +
                        "* Contains at least one lower/upper alpha char\n" +
                        "* Does not contain space, tab, etc.");
                return false;
            }
        }

        if (password.equals(passwordRepeat)){

            mAuth.createUserWithEmailAndPassword(emailTxt.getText().toString(),
                    passwordTxt.getText().toString()).
                    addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (!task.isSuccessful()) {
                                Logger.FireLog(TAG, "User registration with email failed.");
                                Toast.makeText(AuthActivity.this, R.string.auth_failed, Toast.LENGTH_SHORT).show();
                            }else{
                                Logger.FireLog(TAG, "User registration with email succeeded. (User: " + username + ", Email: " + email + ")");
                                Toast.makeText(AuthActivity.this, R.string.auth_success, Toast.LENGTH_SHORT).show();
                                // Write user
                                writeNewUser(username, email);
                                ChangeFlipper();
                                loader.setVisibility(View.GONE);
                            }
                        }
                    });
        }else{
            passwordRepeatTxt.setError("Passwords Don't Match!");
            return false;
        }
        return true;

    }

    public boolean logInWithEmailAndPassword(){
        // Get some sheet
        final EditText emailText = (EditText) findViewById(R.id.login_email);
        final String email = emailText.getText().toString();
        final EditText passwordText = (EditText) findViewById(R.id.login_password);
        final String password = passwordText.getText().toString();

        // Require Password
        if (password.isEmpty()){
            passwordText.setError("Required");
            return false;
        }

        // Require Email
        if (email.isEmpty()){
            emailText.setError("Required");
            return false;
        }

        if (email.indexOf('@') == -1){
            Logger.FireLog(TAG, "Attempting user login using username && password.");

            Query usernamesQuery = PaletteBase.getInstance().getFirebaseDatabaseRoot().child("usernames").child(email);
            usernamesQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String userId = dataSnapshot.getValue(String.class);
                    if (userId == null){
                        Logger.FireLog(TAG, "User login failed with no reference found in /usernames.");
                        Toast.makeText(AuthActivity.this, "Wrong Username", Toast.LENGTH_SHORT).show();
                        loader.setVisibility(View.GONE);
                    }else{
                        Logger.FireLog(TAG, "UserId: " + userId);
                        // Get next query for single value listener on users
                        Query usersQueryByUid = PaletteBase.getInstance().getFirebaseDatabaseRoot().child("users").child(userId);
                        usersQueryByUid.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                FireUser user = dataSnapshot.getValue(FireUser.class);
                                Logger.FireLog(TAG, "User found attempting to login.");
                                signInWithEmailAndPassword(user.email, password);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                loader.setVisibility(View.GONE);
                            }
                        });
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    loader.setVisibility(View.GONE);
                }
            });
        }else{
            Logger.FireLog(TAG, "Attempting user login using email && password.");

            Pattern p = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(email);

            if (!m.find()){
                emailText.setError("Wrong Email");
                return false;
            }

            signInWithEmailAndPassword(email, password);
        }
        return true;
    }

    private void signInWithEmailAndPassword(final String email, final String password){
        mAuth.signInWithEmailAndPassword(email, password).
                addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        loader.setVisibility(View.GONE);
                        if (!task.isSuccessful()) {
                            Toast.makeText(AuthActivity.this, "Log In Failed!",
                                    Toast.LENGTH_SHORT).show();
                        }else{
                            Logger.FireLog(TAG, "User login succeeded. (User: " + email + ", Email: " + password + ")");
                            StartActivity(MainActivity.class);
                        }
                    }
                });
    }

    public void startGoogleSignInActivityForResult(){
        Intent sendnudes = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(sendnudes, RC_SIGN_IN);
        Logger.FireLog(TAG, "Nudes Sent to Google.");
    }

    private void writeNewUser(final String username, final String email){
        final String userId = PaletteBase.getInstance().getUid();

        final DatabaseReference usersReference = PaletteBase.getInstance().getFirebaseDatabaseRoot()
                .child("users");
        final DatabaseReference userNamesReference = PaletteBase.getInstance().getFirebaseDatabaseRoot()
                .child("usernames");

        PaletteBase.getInstance().getUserQuery().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                FireUser user = dataSnapshot.getValue(FireUser.class);
                if (user == null){
                    FireUser newUser = new FireUser(username, email);
                    usersReference
                            .child(userId).setValue(newUser);
                    userNamesReference
                            .child(username).setValue(userId);
                    Logger.FireLog(TAG, "New User written to the database. (User: "
                            + username + ", Email: " + email + ")");
                }else{
                    Logger.FireLog(TAG, "Not creating user. Already exists.");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "Connection Failed!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {

        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(),
                InputMethodManager.RESULT_UNCHANGED_SHOWN);

        switch (v.getId()){
            case R.id.email_log_in_button:
                loader.setVisibility(View.VISIBLE);
                Logger.FireLog(TAG, "Email login button clicked.");
                boolean userLogged = logInWithEmailAndPassword();
                loader.setVisibility(userLogged ? View.VISIBLE : View.GONE);
                break;
            case R.id.sign_in_btn_google:
                loader.setVisibility(View.VISIBLE);
                Logger.FireLog(TAG, "Google sign in button clicked.");
                startGoogleSignInActivityForResult();
                break;
            case R.id.sign_up_button:
                loader.setVisibility(View.VISIBLE);
                Logger.FireLog(TAG, "Email signup button clicked.");
                boolean userCreated = signUpWithEmailAndUsername();
                loader.setVisibility(userCreated ? View.VISIBLE : View.GONE);
                break;
            case R.id.auth_flipper_txt:
                Logger.FireLog(TAG, "Login/SignUp flipper button clicked.");
                ChangeFlipper();
                break;
            default:
                break;
        }
    }

    private void ChangeFlipper() {

        CURRENT_VIEW_FLIPPER_CHILD = (CURRENT_VIEW_FLIPPER_CHILD + 1) % 2;
        googleSignInButton.setVisibility(CURRENT_VIEW_FLIPPER_CHILD == 1 ? View.GONE : View.VISIBLE);
        facebookSignInButton.setVisibility(CURRENT_VIEW_FLIPPER_CHILD == 1 ? View.GONE : View.VISIBLE);


        authFormFlipper.setDisplayedChild(CURRENT_VIEW_FLIPPER_CHILD);
        authFlipperText.setText(CURRENT_VIEW_FLIPPER_CHILD == 0 ?
                        getString(R.string.no_account_text) :
                        getString(R.string.have_account_text));
    }

    private TextWatcher fieldListener(EditText editText, final String field){
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() > 0){
                    if(field.equals("email")){
                        loginEmailWritten = true;
                    }else{
                        loginPasswordWritten = true;
                    }
                }else{
                    if(field.equals("email")){
                        loginEmailWritten = false;
                    }else{
                        loginPasswordWritten = false;
                    }
                }

                if(loginPasswordWritten && loginEmailWritten){
                    emailLoginButton.setEnabled(true);
                }else{
                    emailLoginButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
    }

}

