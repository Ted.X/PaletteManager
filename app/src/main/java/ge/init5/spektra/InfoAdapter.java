package ge.init5.spektra;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by alta on 6/8/17.
 */

class Contact{
    public String Title;
    public String User;
    public String WebURL;
    public String AppURL;
    public String Package;

    public Contact(String t, String u, String wu, String au, String p){
        Title = t;
        User = u;
        WebURL = wu;
        AppURL = au;
        Package = p;
    }
}

public class InfoAdapter extends ArrayAdapter<Contact> {

    private List<Integer> images;
    private Context context;
    public InfoAdapter(Context context) {
        super(context, android.R.layout.select_dialog_item);
        this.context = context;
        images = new ArrayList<>();
    }

    public InfoAdapter(Context context, Contact[] items, Integer[] images) {
        super(context, android.R.layout.select_dialog_item, items);
        this.context = context;
        this.images = Arrays.asList(images);
    }

    public void add(@Nullable Contact object, int image) {
        super.add(object);
        images.add(image);
    }

    //    public ArrayAdapterWithIcon(Context context, int items, int images) {
//        super(context, android.R.layout.select_dialog_item, context.getResources().getTextArray(items));
//
//        final TypedArray imgs = context.getResources().obtainTypedArray(images);
//        this.images = new ArrayList<Integer>() {{ for (int i = 0; i < imgs.length(); i++) {add(imgs.getResourceId(i, -1));} }};
//
//        // recycle the array
//        imgs.recycle();
//    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        TextView textView = (TextView) view.findViewById(android.R.id.text1);

        textView.setCompoundDrawablesRelativeWithIntrinsicBounds(images.get(position), 0, 0, 0);

        textView.setCompoundDrawablePadding(
                (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, getContext().getResources().getDisplayMetrics()));

        textView.setText(getItem(position).Title);
        return view;
    }

    public void Click(Contact item) {
        Uri uri = Uri.parse(item.AppURL + item.User);
        Intent intent;// = new Intent(Intent.ACTION_VIEW, uri);

//        likeIng.setPackage(item.Package);
        try {
            context.getPackageManager().getPackageInfo(item.Package, 0);
            intent = new Intent(Intent.ACTION_VIEW, uri);
        } catch (Exception e) {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://"+item.WebURL+item.User));
        }
        context.startActivity(intent);

    }
}
