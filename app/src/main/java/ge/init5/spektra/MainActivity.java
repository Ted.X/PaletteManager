package ge.init5.spektra;

// Android

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.gordonwong.materialsheetfab.MaterialSheetFab;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.BaseDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.Nameable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ge.init5.spektra.models.*;
import ge.init5.spektra.paletteCreator.AddPaletteActivity;
import ge.init5.spektra.paletteEditor.crop.CropActivity;
import ge.init5.spektra.palettes.palette.PaletteAdapter;
import ge.init5.spektra.services.FirebasePaletteDatabase.PaletteBase;
import ge.init5.spektra.utils.PaletteComparator;
import ge.init5.spektra.utils.FabButton.ExpandableFabButton;
import ge.init5.spektra.utils.Logger;
import ge.init5.spektra.utils.Tutorial;

// Facebook SDK
// Firebase Auth
// Firebase Realtime Database
// Icons Typeface
// Material Drawer
// Java
// Palette Manager Classes

public class MainActivity extends AppCompatActivity {

    static {
        System.loadLibrary("dominantcolors");
        Logger.LogDebug("Static iMport", "dominantcolors imported");
    }

    private static final String TAG = CropActivity.class.getSimpleName();

    public RecyclerView mRecyclerView;


    private Drawer drawer;

    public static final int SELECT_PICTURE_FROM_GALLERY = 1;
    public static final int SELECT_PICTURE_FROM_CAMERA = 2;
    public static final int PALETTE_CREATED = 3;
    public static final int PALETTE_CHANGED = 4;
    public static final int CAMERA_REQUEST_CODE = 3213232;
    public static final int EXTERNAL_READ_CODE = 6;

    public static int PREVIOUS_SORTING_OPTION;

    private List<String> previousFilterList;

    private Toolbar toolbar;
    private MaterialSheetFab materialSheetFab;

    List<FirePalette> publicPalettes;

    public static int drawerSelectedIndex = 1;

    public Menu optionsMenu;

    private SearchView searchView;

    private SwipeRefreshLayout refreshLayout;

    private AdView mBannerAdView;

    public static InterstitialAd mInterstitialAd;

    public Map<String, Uri> mMemoryCache;

    /**
     * Sets content view for this class activity_main
     * Initializes toolbar
     * Sets floating action buttons
     * Sets Recycler View for Users Palettes
     * Fills Recycler View with User Palettes
     * Initializes Drawer
     *
     * @param savedInstanceState application state in bundle
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Logger.FireLog(TAG, "Main Activity Started.");

        // Prefs and downloads limit
        PaletteBase.getInstance().setSharedPreferences(this);


        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemViewCacheSize(20);
        mRecyclerView.setDrawingCacheEnabled(true);
        mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_AUTO);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.getItemAnimator().setChangeDuration(0);

        publicPalettes = new ArrayList<>();
        PREVIOUS_SORTING_OPTION = Integer.parseInt(PaletteBase.getInstance().sharedPreferences.getString("my_palette_order_type", "3"));

        changeMainToDrawerIndexedView(PREVIOUS_SORTING_OPTION, null);
        initDrawer();

//        toolbar = getSupportActionBar();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }

        setFloatingActionButtons();


        // Init Banner Ad
        mBannerAdView = (AdView) findViewById(R.id.main_activity_banner);
        AdRequest adRequest = new AdRequest.Builder().build();
        mBannerAdView.loadAd(adRequest);

        // Init Interstitial
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getResources().getString(R.string.ad_download_interstitial));

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                requestNewInterstitial();
            }
        });
        requestNewInterstitial();
        Logger.FireLog(TAG, "Main Activity Interstitial Ad Initialized.");


        previousFilterList = null;

        PaletteBase.getInstance().downloadColorNames();


        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_layout);

        // Set visibility of fab and banner and padding to refreshlayout
        Tutorial.fabButtonTutorial(this);
        toggleFabBannerAndAddPaddingToRefreshLayout();


        refreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        changeMainToDrawerIndexedView(PREVIOUS_SORTING_OPTION, previousFilterList);
                        refreshLayout.setRefreshing(false);
                    }
                });

//        FirebaseMessaging.getInstance().subscribeToTopic("hearts");
//        PaletteBase.getInstance().writeUsersMessagingToken(
//        PaletteBase.getInstance().writeUsersMessagingToken(
//                FirebaseInstanceId.getInstance().getToken());

        Intent i = getIntent();
        Uri data = i.getData();

        if (data != null || i.hasExtra("colors")) {
            Intent intent = new Intent(MainActivity.this, AddPaletteActivity.class);
            intent.putExtras(i.getExtras());
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivityForResult(intent, PALETTE_CREATED);
        }

        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = maxMemory / 8;

//        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
//            @Override
//            protected int sizeOf(String key, Bitmap bitmap) {
//                // The cache size will be measured in kilobytes rather than
//                // number of items.
//                return bitmap.getByteCount() / 1024;
//            }
//        };
        mMemoryCache = new HashMap<>();
    }

    public void addUriToMemoryCache(String key, Uri uri) {
        if (getUriFromMemCache(key) == null) {
            mMemoryCache.put(key, uri);
        }
    }

    public Uri getUriFromMemCache(String key) {
        return mMemoryCache.get(key);
    }



    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mInterstitialAd.loadAd(adRequest);
    }

    public void initDrawer() {

        // Get user name and email and image if possible
        PaletteBase.getInstance().getUserQuery().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get user data
                FireUser user = dataSnapshot.getValue(FireUser.class);

                if (user != null) {
                    AccountHeader headerResult = new AccountHeaderBuilder()
                            .withActivity(MainActivity.this)
                            .withHeaderBackground(R.color.accent)
                            .addProfiles(
                                    new ProfileDrawerItem().withName(user.name).withEmail(user.email)
                            )
                            .withOnAccountHeaderSelectionViewClickListener(null)
                            .withProfileImagesVisible(false)
                            .build();

                    final List<BaseDrawerItem> drawerItems = new ArrayList<BaseDrawerItem>() {{
                        add(new PrimaryDrawerItem().withIdentifier(1).withName(R.string.drawer_item_my)
                                .withIcon(GoogleMaterial.Icon.gmd_home)
                                .withIconColor(Color.rgb(117, 117, 117)).withTextColor(Color.rgb(75, 75, 75)));

                        add(new PrimaryDrawerItem().withIdentifier(2).withName(R.string.drawer_item_public)
                                .withIcon(GoogleMaterial.Icon.gmd_public)
                                .withIconColor(Color.rgb(117, 117, 117)).withTextColor(Color.rgb(75, 75, 75)));

                        add(new PrimaryDrawerItem().withIdentifier(3).withName(R.string.drawer_item_favorites)
                                .withIcon(GoogleMaterial.Icon.gmd_favorite_border)
                                .withIconColor(Color.rgb(117, 117, 117)).withTextColor(Color.rgb(75, 75, 75)));

                        add(new PrimaryDrawerItem().withIdentifier(4).withName("Card Palettes")
                                .withIcon(GoogleMaterial.Icon.gmd_credit_card)
                                .withIconColor(Color.rgb(117, 117, 117)).withTextColor(Color.rgb(75, 75, 75)));

                        add(new SecondaryDrawerItem().withIdentifier(5).withName(R.string.drawer_item_rating)
                                .withIcon(GoogleMaterial.Icon.gmd_star)
                                .withIconColor(Color.rgb(117, 117, 117)).withTextColor(Color.rgb(117, 117, 117)));

                        add(new SecondaryDrawerItem().withIdentifier(6).withName(R.string.drawer_item_settings)
                                .withIcon(GoogleMaterial.Icon.gmd_settings)
                                .withIconColor(Color.rgb(117, 117, 117)).withTextColor(Color.rgb(117, 117, 117)));

                        add(new SecondaryDrawerItem().withIdentifier(7).withName(R.string.drawer_item_logout)
                                .withIcon(GoogleMaterial.Icon.gmd_exit_to_app)
                                .withIconColor(Color.rgb(117, 117, 117)).withTextColor(Color.rgb(117, 117, 117)));
                    }};

                    // Initialize Drawer
                    drawer = new DrawerBuilder()
                            .withActivity(MainActivity.this)
                            .withToolbar(toolbar)
                            .withAccountHeader(headerResult)
                            .addDrawerItems(
                                    drawerItems.get(0),
                                    drawerItems.get(1),
                                    drawerItems.get(2),
                                    drawerItems.get(3)
//                                     new DividerDrawerItem(),
//                                    drawerItems.get(3),
//                                    drawerItems.get(4)
                            )
                            .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                                @Override
                                public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                                    if (drawerItem != null) {
                                        if (drawerItem instanceof Nameable) {
                                            searchView.onActionViewCollapsed();
                                            drawer.closeDrawer();

                                            // Reset Some things
//                                            PREVIOUS_SORTING_OPTION = PaletteComparator.SORT_BY_DATE;
                                            previousFilterList = null;
                                            optionsMenu.findItem(R.id.action_filter).setVisible(true);


                                            toolbar.setTitle(((Nameable) drawerItem).getName()
                                                    .getText(MainActivity.this));

                                            mRecyclerView.setAdapter(null);
                                            publicPalettes = new ArrayList<>();


                                            SharedPreferences shp = PaletteBase.getInstance().sharedPreferences;

                                            drawerSelectedIndex = ((int) drawerItem.getIdentifier());

                                            switch (drawerSelectedIndex) {
                                                case 1:
                                                    int mySort = Integer.parseInt(shp.getString("my_palette_order_type", "3"));
                                                    changeMainToDrawerIndexedView(mySort, null);
                                                    break;
                                                case 2:
                                                    int palSort = Integer.parseInt(shp.getString("public_palette_order_type", "3"));
                                                    changeMainToDrawerIndexedView(palSort, null);
                                                    break;
                                                case 3:
                                                    changeMainToDrawerIndexedView(PaletteComparator.SORT_BY_HEARTS_COUNT, null);
                                                    break;
                                                case 4:
                                                    changeMainToDrawerIndexedView(PaletteComparator.SORT_BY_DATE, null);
                                                    break;
                                                case 5:
                                                    sendUserToPlayStoreForRating();
                                                    drawer.setSelection(1);
                                                    return true;
                                                case 6:
                                                    Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                                                    startActivity(intent);
                                                    drawer.setSelection(1);
                                                    return true;
                                                case 7:
                                                    Logger.FireLog(TAG, "Options Menu: Signing out user.");
                                                    FirebaseAuth.getInstance().signOut();
//                                                    SpektraInstanceIDService.unAuth();
                                                    if (AccessToken.getCurrentAccessToken() != null)
                                                        LoginManager.getInstance().logOut();
                                                    drawerSelectedIndex = 1;
                                                    finish();
                                                    return true;

                                            }

                                            // Set visibilities
                                            toggleFabBannerAndAddPaddingToRefreshLayout();

                                        }
                                    }
                                    return true;
                                }
                            })
                            .withGenerateMiniDrawer(false).build();
                    // Add footer Rate Spektra
                    drawer.addStickyFooterItem(drawerItems.get(4));
                    // Add footer Settings
                    drawer.addStickyFooterItem(drawerItems.get(5));
                    // Add Logout
                    drawer.addStickyFooterItem(drawerItems.get(6));
                    Logger.FireLog(TAG, "Drawer initialized.");
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
//                Logger.LogDebug(TAG, "getUser:onCancelled", databaseError.toException());
            }
        });
    }

    private void sendUserToPlayStoreForRating() {
        Uri playStoreAppURI = Uri.parse("market://details?id=" + this.getPackageName());
        Intent goToPlayStore = new Intent(Intent.ACTION_VIEW, playStoreAppURI);

        goToPlayStore.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToPlayStore);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" +
                            this.getPackageName())));
        }
    }

    private void toggleFabBannerAndAddPaddingToRefreshLayout() {
        ExpandableFabButton fabMain =
                (ExpandableFabButton) findViewById(
                        R.id.fab);
        // Hide and show ad banner and
        mBannerAdView.setVisibility(drawerSelectedIndex == 1 ? View.GONE : View.VISIBLE);

        if (drawerSelectedIndex == 1) {
            fabMain.setClickable(true);
            fabMain.show();
        } else {
            fabMain.setClickable(false);
            fabMain.hide();
        }

        mRecyclerView.setPaddingRelative(0, 0, 0,
                AdSize.SMART_BANNER.getHeightInPixels(this) +
                        getResources().getDimensionPixelSize(R.dimen.card_margin));

    }

    /**
     * Change view setting adapters to the recycler view according to the selected index in drawer
     * this method is also called from PaletteBase class whenever the refresh is needed
     */
    public void changeMainToDrawerIndexedView(int orderBy, @Nullable final List<String> filterList) {
        Logger.FireLog(TAG, "Change main to drawer indexed view called with: " + orderBy);

        PREVIOUS_SORTING_OPTION = orderBy;
//        if (filterList != null)
            previousFilterList = filterList;

//        if (mInterstitialAd != null && mInterstitialAd.isLoaded()){
//            mInterstitialAd.show();
//        }

        Query userPalettesQuery = PaletteBase.getInstance().getUserPalettesQuery();
        Query publicPalettesQuery = PaletteBase.getInstance().getPublicPalettesQuery();
        Query favoritePublicPalettesQuery = PaletteBase.getInstance().getFavoritePalettesQuery();
        Query cardPalettesQuery = PaletteBase.getInstance().getCardPalettesQuery();

        // Remove event listeners
        PaletteBase.getInstance().removeUpdateChildEventListener(publicPalettesQuery);
        PaletteBase.getInstance().removeUpdateChildEventListener(favoritePublicPalettesQuery);
        PaletteBase.getInstance().removeUpdateChildEventListener(cardPalettesQuery);

        switch (drawerSelectedIndex) {
            case 1:
                PaletteBase.getInstance().getUsersPalettes(
                        userPalettesQuery,
                        MainActivity.this,
                        mRecyclerView, previousFilterList, orderBy);
                break;
            case 2:
                PaletteBase.getInstance().getPublicPalettes(publicPalettesQuery,
                        mRecyclerView, null, previousFilterList, orderBy, false);
                break;
            case 3:
                PaletteBase.getInstance().getPublicPalettes(favoritePublicPalettesQuery,
                        mRecyclerView, null, previousFilterList, orderBy, false);
                break;
            case 4:
                if (mInterstitialAd != null && mInterstitialAd.isLoaded()){
                    mInterstitialAd.show();
                }
                PaletteBase.getInstance().getPublicPalettes(cardPalettesQuery,
                        mRecyclerView, null, previousFilterList, orderBy, true);
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (materialSheetFab.isSheetVisible()) {
            materialSheetFab.hideSheet();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        try {
            PaletteAdapter paletteAdapter = ((PaletteAdapter) mRecyclerView.getAdapter());
            if (paletteAdapter != null) {
                paletteAdapter.onSaveInstanceState(outState);
            }
        } catch (ClassCastException ex) {
            Logger.FireCrash(ex);
        }

//        ((PaletteAdapter) mRecyclerView.getAdapter()).onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        try {
            PaletteAdapter paletteAdapter = ((PaletteAdapter) mRecyclerView.getAdapter());

            if (paletteAdapter != null) {
                paletteAdapter.onRestoreInstanceState(savedInstanceState);
            }
        } catch (ClassCastException ex) {
            Logger.FireCrash(ex);
        }

//        ((PaletteAdapter) mRecyclerView.getAdapter()).onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Logger.FireLog(TAG, "Options menu created.");
        try {
            getMenuInflater().inflate(R.menu.menu_main, menu);
            optionsMenu = menu;

            searchView = (SearchView) MenuItemCompat
                    .getActionView(menu.findItem(R.id.action_search));
            searchView.setQueryHint("Type . for names, # for tags");
            PaletteBase.getInstance().setSearchSystem(this, searchView, menu);
            menu.findItem(R.id.action_filter).setIcon(new IconicsDrawable(getApplicationContext())
                    .actionBar()
                    .icon(GoogleMaterial.Icon.gmd_filter_list)
                    .color(Color.WHITE)
                    .sizeDp(24));
        } catch (Exception ex) {
            Logger.FireCrash(ex);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int actionId = item.getItemId();

        switch (actionId) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            // Sort By Palette Title
            case R.id.filter_by_name:
                Logger.FireLog(TAG, "Options Menu: Sort by Name called.");
                PREVIOUS_SORTING_OPTION = PaletteComparator.SORT_BY_TITLE;
                changeMainToDrawerIndexedView(PREVIOUS_SORTING_OPTION, null);
                break;

            // Sort By Palette Hearts Count
            case R.id.filter_by_hearts:
                Logger.FireLog(TAG, "Options Menu: Sort by Hearts called.");
                PREVIOUS_SORTING_OPTION = PaletteComparator.SORT_BY_HEARTS_COUNT;
                changeMainToDrawerIndexedView(PREVIOUS_SORTING_OPTION, null);
                break;

            // Sort By Palette Creation Date
            case R.id.filter_by_date:
                Logger.FireLog(TAG, "Options Menu: Sort by Date called.");
                PREVIOUS_SORTING_OPTION = PaletteComparator.SORT_BY_DATE;
                changeMainToDrawerIndexedView(PREVIOUS_SORTING_OPTION, null);
                break;

            // Action About
            case R.id.action_about:
                Logger.FireLog(TAG, "Options Menu: About called.");
                AlertDialog.Builder builderSingle = new AlertDialog.Builder(this)
                        .setTitle("Spektra v"+BuildConfig.VERSION_NAME + " Developed by Studio Init 5")
//                        .setMessage("Developed by Studio Init 5 @ Tech Park Georgia")
                        .setIcon(R.mipmap.ic_launcher);

                Contact[] contacts = new Contact[]{
                        new Contact("Facebook", "spektraapp", "www.facebook.com/", "fb://facewebmodal/f?href=", "com.facebook.katana"),
                        new Contact("Instagram", "spektracolors", "www.instagram.com/", "instagram://user?username=", "com.instagram.android"),
                        new Contact("Twitter", "SpektraApp" , "www.twitter.com/", "twitter://user?screen_name=", "com.twitter.android"),
                        new Contact("Tumblr", "spektracolors", "www.tumblr.com/blog/", "tumblr://x-callback-url/", "com.tumblr.android"),
                        new Contact("Gmail", "studioinit5@gmail.com", "mailto:", "mailto:", "com.google.android.gm")
                };
                Integer[] images = new Integer[]{
                        R.drawable.facebook,
                        R.drawable.instagram,
                        R.drawable.twitter,
                        R.drawable.tumblr,
                        R.drawable.gmail,
                };
                final InfoAdapter arrayAdapter = new InfoAdapter(MainActivity.this, contacts, images);

                builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        Log.d(TAG, arrayAdapter.getItem(which).Title);
                        arrayAdapter.Click(arrayAdapter.getItem(which));
                    }
                });
                builderSingle.show();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (materialSheetFab.isSheetVisible()) {
            materialSheetFab.hideSheet();
        } else {
            moveTaskToBack(true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            Logger.FireLog(TAG, "On Activity Result Ok.");
            Bitmap bitmap = null;
            if (requestCode == SELECT_PICTURE_FROM_CAMERA) {
                Logger.FireLog(TAG, "On Activity Result picture from camera." );
                try {
                    Intent intent = new Intent(MainActivity.this, CropActivity.class);
                    bitmap = (Bitmap) data.getExtras().get("data");

                    File file = null;

                    file = File.createTempFile("crop", ".png", getExternalCacheDir());
                    FileOutputStream outputStream = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
                    Uri path = Uri.fromFile(file);

                    intent.setData(path);
                    startActivityForResult(intent, SELECT_PICTURE_FROM_GALLERY);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else if (requestCode == SELECT_PICTURE_FROM_GALLERY) {
                Logger.FireLog(TAG, "On Activity Result picture from gallery." );
                Intent intent = new Intent(this, AddPaletteActivity.class);
                intent.putExtras(data.getExtras());
//                intent.setData(data.getData());
                startActivityForResult(intent, PALETTE_CREATED);
            }
            else if (requestCode == PALETTE_CREATED || requestCode == PALETTE_CHANGED) {
                Logger.FireLog(TAG, "On Activity Result changed or created Palette. RequestCode: "
                        + requestCode);
                mRecyclerView.setAdapter(null);
                PaletteBase.getInstance().getUsersPalettes(
                        PaletteBase.getInstance().getUserPalettesQuery(),
                        MainActivity.this, mRecyclerView, null, PREVIOUS_SORTING_OPTION);
            }

//            if (bitmap != null) {
//
//                Intent intent = new Intent(MainActivity.this, AddPaletteActivity.class);
//                // Java
//                int[] colorsFromImage = DominantColors.getColors(bitmap);
//                intent.putExtra("colors", colorsFromImage);
//                startActivityForResult(intent, PALETTE_CREATED);
//
//            }
            else {
                Logger.FireLog(TAG, "Image not found.");
            }
        } else {
            Logger.FireLog(TAG, "Main Activity On Activity Result: " + resultCode);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Logger.FireLog(TAG, "Image Capture Permission Granted.");
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, SELECT_PICTURE_FROM_CAMERA);
            } else {
                Logger.FireLog(TAG, "Image Capture Permission Not Granted.");
                Toast.makeText(this, "Permission not granted", Toast.LENGTH_SHORT).show();
            }
        }else if(requestCode == EXTERNAL_READ_CODE){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Logger.FireLog(TAG, "Open Gallery");
                Intent intent = new Intent(MainActivity.this, CropActivity.class);
//                intent.setType("image/*");
//                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, SELECT_PICTURE_FROM_GALLERY);
            }else {
                Logger.FireLog(TAG, "Gallery Read Permission Not Granted.");
                Toast.makeText(this, "Permission not granted", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void setFloatingActionButtons() {

        View sheetView = findViewById(R.id.fab_sheet);
        View overlay = findViewById(R.id.overlay);
        LinearLayout layout = (LinearLayout) findViewById(R.id.sheet_layout);

        int sheetColor = getResources().getColor(R.color.def_white);
        int fabColor = getResources().getColor(R.color.accent);


        ExpandableFabButton fabMain = (ExpandableFabButton) findViewById(R.id.fab);
        TextView fabGallery = (TextView) findViewById(R.id.fab_sheet_item_gallery);
        TextView fabCamera = (TextView) findViewById(R.id.fab_sheet_item_camera);
        TextView fabManually = (TextView) findViewById(R.id.fab_sheet_item_manually);

        fabGallery.setCompoundDrawablesRelativeWithIntrinsicBounds(new IconicsDrawable(getApplicationContext())
                        .icon(GoogleMaterial.Icon.gmd_photo_library)
                        .color(Color.GRAY)
                        .sizeDp(24),
                null, null, null);

        fabCamera.setCompoundDrawablesRelativeWithIntrinsicBounds(new IconicsDrawable(getApplicationContext())
                        .icon(GoogleMaterial.Icon.gmd_camera)
                        .color(Color.GRAY)
                        .sizeDp(24),
                null, null, null);

        fabManually.setCompoundDrawablesRelativeWithIntrinsicBounds(new IconicsDrawable(getApplicationContext())
                        .icon(GoogleMaterial.Icon.gmd_edit)
                        .color(Color.WHITE)
                        .sizeDp(24),
                null, null, null);


        // Initialize material sheet FAB
        materialSheetFab = new MaterialSheetFab<>(fabMain, sheetView, overlay, sheetColor, fabColor);


        fabCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Logger.FireLog(TAG, "Fab image capture called.");
                if (Build.VERSION.SDK_INT >= 23 &&
                        checkSelfPermission(android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{android.Manifest.permission.CAMERA}, CAMERA_REQUEST_CODE);
                } else {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, SELECT_PICTURE_FROM_CAMERA);
                }
            }
        });

        fabGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Logger.FireLog(TAG, "Fab picture from gallery called.");
                if (Build.VERSION.SDK_INT >= 23 &&
                        checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, EXTERNAL_READ_CODE);
                } else {
                    Intent intent = new Intent(MainActivity.this, CropActivity.class);
//                intent.setType("image/*");
//                intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(intent, SELECT_PICTURE_FROM_GALLERY);
                }
            }
        });

        fabManually.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Logger.FireLog(TAG, "Fab create manually called.");
                Intent intent = new Intent(MainActivity.this, AddPaletteActivity.class);
                startActivityForResult(intent, PALETTE_CREATED);
            }
        });
    }
}
