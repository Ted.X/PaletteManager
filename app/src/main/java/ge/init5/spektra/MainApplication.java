package ge.init5.spektra;

import android.support.multidex.MultiDexApplication;

//import com.adobe.creativesdk.foundation.AdobeCSDKFoundation;
//import com.adobe.creativesdk.foundation.auth.IAdobeAuthClientCredentials;
import com.facebook.FacebookSdk;
import com.facebook.LoggingBehavior;
import com.google.android.gms.ads.MobileAds;


/**
 * Created by __ted__ on 1/27/17.
 */


public class MainApplication extends MultiDexApplication{ //implements IAdobeAuthClientCredentials {

    /* Be sure to fill in the two strings below. */
    private static final String CREATIVE_SDK_CLIENT_ID = "9cc47a9fa67443e3b508664568c05377";
    private static final String CREATIVE_SDK_CLIENT_SECRET = "dba17768-6ea9-41f9-9ca1-36bfa199e4e0";

    @Override
    public void onCreate() {
        super.onCreate();
//        AdobeCSDKFoundation.initializeCSDKFoundation(getApplicationContext());


        MobileAds.initialize(getApplicationContext(), getResources().getString(R.string.ad_public_palettes_banner));
//        Logger.FireLog(TAG, "Main Activity Ad Banner Initialized.");
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        FacebookSdk.setIsDebugEnabled(true);
//        FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
    }

//    @Override
//    public String getClientID() {
//        return CREATIVE_SDK_CLIENT_ID;
//    }
//
//    @Override
//    public String getClientSecret() {
//        return CREATIVE_SDK_CLIENT_SECRET;
//    }
}