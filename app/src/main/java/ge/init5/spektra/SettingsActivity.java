package ge.init5.spektra;

import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ge.init5.spektra.utils.Logger;

//import com.ullink.slack.simpleslackapi.SlackSession;
//import com.ullink.slack.simpleslackapi.events.SlackConnected;
//import com.ullink.slack.simpleslackapi.impl.SlackSessionFactory;
//import com.ullink.slack.simpleslackapi.listeners.SlackConnectedListener;


public class SettingsActivity extends AppCompatActivity {

    private static final String TAG = SettingsActivity.class.getSimpleName();

    static final int REQ_CODE_CSDK_USER_AUTH = 1001;


//    private AdobeUXAuthManager mUXAuthManager = AdobeUXAuthManager.getSharedAuthManager();
//    private AdobeAuthSessionHelper mAuthSessionHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
//
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        if (toolbar != null) {
//            setSupportActionBar(toolbar);
//        }
//        ActionBar actionBar = getSupportActionBar();
//        actionBar.getThemedContext().setTheme(R.style.AppTheme_AppBarOverlay);

        getFragmentManager().beginTransaction().replace(R.id.fragment,  new MyPreferenceFragment()).commit();

        //        adobeSingInButton.setOnClickListener(this);

//
//        mAuthSessionHelper = new AdobeAuthSessionHelper(mStatusCallback);
//        mAuthSessionHelper.onCreate(savedInstanceState);
    }

//    private AdobeAuthSessionHelper.IAdobeAuthStatusCallback mStatusCallback;
//
//    {
//        mStatusCallback = new AdobeAuthSessionHelper.IAdobeAuthStatusCallback() {
//            @Override
//            public void call(AdobeAuthSessionHelper.AdobeAuthStatus adobeAuthStatus, AdobeAuthException e) {
//                if (!mUXAuthManager.isAuthenticated()) {
//                    /* 3 */
//                    login();
//                } else {
//                    Logger.LogDebug(TAG, "Already logged in!");
//                    mUXAuthManager.logout();
//                }
//            }
//        };
//    }
//
//
//    /* 4 */
//    private void login() {
//        final String[] authScope = {"email", "profile", "address"};
//
//        AdobeAuthSessionLauncher authSessionLauncher = new AdobeAuthSessionLauncher.Builder()
//                .withActivity(this)
//                .withRedirectURI("ams+6e1061954356a309cc01dda747363e95d4d01684://adobeid/9cc47a9fa67443e3b508664568c05377")
//                .withAdditonalScopes(authScope)
//                .withRequestCode(REQ_CODE_CSDK_USER_AUTH) // Can be any int
//                .build();
//
//
//        mUXAuthManager.login(authSessionLauncher);
//    }



    public void LinkAccount(final View v) {

        Snackbar.make(v, "Coming Soon", Snackbar.LENGTH_SHORT).show();

        //            case R.id.sign_in_btn_adobe:
        Logger.FireLog(TAG, "Adobe sign in button clicked.");
//        mAuthSessionHelper.onResume();


//        try {
//            SlackSession session = SlackSessionFactory.getSlackSessionBuilder("my-bot-auth-token")
//                    .withProxy(Proxy.Type.HTTP, "my.proxy.address", 1234)
//                    .withAutoreconnectOnDisconnection(false)
//                    .withConnectionHeartbeat(10, TimeUnit.SECONDS)
//            session.addSlackConnectedListener(
//                    new SlackConnectedListener() {
//                        @Override
//                        public void onEvent(SlackConnected event, SlackSession session) {
//                            Snackbar.make(v, "Connected", Snackbar.LENGTH_SHORT).show();
//
//                        }
//                    });
//            session.connect();
//        } catch (IOException e) {
//            Snackbar.make(v, "Coming Soon", Snackbar.LENGTH_SHORT).show();
//            e.printStackTrace();
//        }
////        SlackSession session = SlackSessionFactory.createWebSocketSlackSession("Jw08eiPvPx7L13wVbd0CfxSF");
////        try {
////            session.connect();
////        } catch (IOException e) {
////            e.printStackTrace();
////        }
    }


//    @Override
//    protected void onStart() {
//        super.onStart();
//        if(mAuthSessionHelper != null)
//            mAuthSessionHelper.onStart();
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        if(mAuthSessionHelper != null)
//            mAuthSessionHelper.onStop();
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        if(mAuthSessionHelper != null)
//            mAuthSessionHelper.onDestroy();
//    }
//
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        if(mAuthSessionHelper != null)
//            mAuthSessionHelper.onPause();
//
//    }
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        mAuthSessionHelper.onActivityResult(requestCode, resultCode, data);
//
//        if(resultCode == RESULT_OK && requestCode == REQ_CODE_CSDK_USER_AUTH){
//
//            Logger.LogDebug(TAG, mUXAuthManager.isAuthenticated()+"");
//
//            final FireAdobeAuthUser profile = new FireAdobeAuthUser(mUXAuthManager.getUserProfile());
//
//            Logger.LogDebug(TAG, "USER: " + profile.getEmail() + " Password: " + profile.getAdobeID());
//
//
//            // Get next query for single value listener on users
//            final DatabaseReference ref = PaletteBase.getInstance().getUserQuery().getRef();
//            ref.addListenerForSingleValueEvent(new ValueEventListener() {
//                @Override
//                public void onDataChange(DataSnapshot dataSnapshot) {
//                    FireUser user = dataSnapshot.getValue(FireUser.class);
//                    if (user != null){
//                        user.creativeCloudUser = profile;
//
//                        ref.setValue(user);
//
//                        Logger.FireLog(TAG, "Linked Creative Cloud Account");
//                    }else{
//                        Logger.FireLog(TAG, "Cant Link Adobe Account");
//                    }
//                    mAuthSessionHelper = null;
//
//                }
//
//                @Override
//                public void onCancelled(DatabaseError databaseError) {
//
//                }
//            });
//
//        }
//    }
//


    public static class MyPreferenceFragment extends PreferenceFragment
    {
        @Override
        public void onCreate(final Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences);

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return super.onCreateView(inflater, container, savedInstanceState);
        }


    }

}
