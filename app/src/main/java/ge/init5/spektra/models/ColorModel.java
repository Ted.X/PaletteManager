package ge.init5.spektra.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by __ted__ on 1/13/17.
 */
@IgnoreExtraProperties
public class ColorModel {
    public String label;
    public int x;
    public int y;
    public int z;

    public ColorModel() {
    }

    public ColorModel(String label, int x, int y, int z) {

        this.label = label;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Exclude
    public short[] ToRGB(){
        return new short[]{(short)x, (short) y, (short)z};
    }
}
