package ge.init5.spektra.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.ServerValue;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ge.init5.spektra.utils.PaletteComparator.SORT_BY_DATE;
import static ge.init5.spektra.utils.PaletteComparator.SORT_BY_HEARTS_COUNT;
import static ge.init5.spektra.utils.PaletteComparator.SORT_BY_TITLE;

/**
 * FirePalette is the model that represents the palette in the firebase realtime database in both
 * palettes and user-palettes
 * Created by __ted__ on 12/25/16.
 */

@IgnoreExtraProperties
public class FirePalette {

    public String uid;
    public String author;

    public String title;

    public String col1;     // Hex Color 1
    public String col2;     // Hex Color 2
    public String col3;     // Hex Color 3
    public String col4;     // Hex Color 4
    public String col5;     // Hex Color 5

    public Object dateCreated;

    public boolean isPublic;
    public boolean hasImage;

    public int heartCount;
    public Map<String, Boolean> hearts = new HashMap<>();

    public List<String> tags;


    public FirePalette(){

    }

    public FirePalette(FirePalette fp, String author, String uid){
        this.title = fp.title;
        this.author = author;

        this.col1 = fp.col1;
        this.col2 = fp.col2;
        this.col3 = fp.col3;
        this.col4 = fp.col4;
        this.col5 = fp.col5;

        this.dateCreated = ServerValue.TIMESTAMP;
        this.heartCount = 0;

        this.isPublic = false;
        this.hasImage = false;
        this.uid = uid;

        if (fp.tags == null)
            this.tags = new ArrayList<String>();
        else
            this.tags = fp.tags;
    }

    public FirePalette(String name, String author, String col1, String col2, String col3, String col4,
                       String col5, boolean isPublic, boolean hasImage, String Uid, List<String> tags){
        this.title = name;
        this.author = author;

        this.col1 = col1;
        this.col2 = col2;
        this.col3 = col3;
        this.col4 = col4;
        this.col5 = col5;

        this.dateCreated = ServerValue.TIMESTAMP;

        this.hasImage = hasImage;
        this.isPublic = hasImage || isPublic;
        this.uid = Uid;

        this.heartCount = 0;

        if (tags == null)
            this.tags = new ArrayList<String>();
        else
            this.tags = tags;
    }

    /**
     * Used by firebase set value to get FirePalette representation in database
     * @return FirePalette to map
     */
    @Exclude
    public Map<String, Object> toMap(){

        HashMap<String, Object> result = new HashMap<>();

        result.put("uid", this.uid);
        result.put("author", this.author);
        result.put("title", this.title);
        result.put("dateCreated", this.dateCreated);
        result.put("isPublic", this.isPublic);
        result.put("hasImage", this.hasImage);
        result.put("heartCount", this.heartCount);
        result.put("hearts", this.hearts);
        result.put("col1", this.col1);
        result.put("col2", this.col2);
        result.put("col3", this.col3);
        result.put("col4", this.col4);
        result.put("col5", this.col5);
        result.put("tags", this.tags);

        return result;
    }

    @Exclude
    public int[] getColorInts(){
        return new int[]{
                Integer.parseInt(this.col1, 16),
                Integer.parseInt(this.col2, 16),
                Integer.parseInt(this.col3, 16),
                Integer.parseInt(this.col4, 16),
                Integer.parseInt(this.col5, 16)
        };
    }

    @Exclude
    public String[] getColorsHex(){
        return new String[]{
                this.col1,
                this.col2,
                this.col3,
                this.col4,
                this.col5
        };
    }

    @Exclude
    public String getDateCreated(){
        return new SimpleDateFormat("MMMM dd, yyyy").format(new Date((long)this.dateCreated));
    }

    @Exclude
    public int myCompareTo(FirePalette other, int orderBy){
        switch (orderBy){
            case SORT_BY_TITLE:
                return this.title.toLowerCase().compareTo(other.title.toLowerCase());
            case SORT_BY_HEARTS_COUNT:
                if (this.heartCount > other.heartCount)
                    return -1;
                else if (this.heartCount < other.heartCount)
                    return 1;
                else
                    return 0;
            case SORT_BY_DATE:
                if ((long)this.dateCreated < (long)other.dateCreated)
                    return 1;
                else if ((long)this.dateCreated > (long)other.dateCreated)
                    return -1;
                else
                    return 0;
        }
        return 0;
    }

}
