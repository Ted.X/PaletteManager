package ge.init5.spektra.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * Created by __ted__ on 1/12/17.
 */
@IgnoreExtraProperties
public class FireTag {
    public String paletteKey;
    public String authorUid;

    public FireTag(){
        this.paletteKey = "";
        this.authorUid = "";
    }

    public FireTag(String paletteKey, String authorUid){
        this.paletteKey = paletteKey;
        this.authorUid = authorUid;
    }

    /**
     * Used by firebase set value to get FireTag representation in database
     * @return
     */
    @Exclude
    public Map<String, Object> toMap(){

        HashMap<String, Object> result = new HashMap<>();

        result.put("authorUid", this.authorUid);
        result.put("paletteKey", this.paletteKey);

        return result;
    }
}
