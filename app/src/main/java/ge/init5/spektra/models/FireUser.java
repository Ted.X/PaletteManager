package ge.init5.spektra.models;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * FireUser is the model that represents the palette in the firebase realtime database in users
 * Created by __ted__ on 12/25/16.
 */

@IgnoreExtraProperties
public class FireUser {

    public String name;
    public String email;
//    public FireAdobeAuthUser creativeCloudUser;

    public FireUser(){ }

    public FireUser(String name, String email){
        this.name = name;
        this.email = email;
    }

//    public FireUser(String name, String email, FireAdobeAuthUser creativeCloudUser) {
//        this.name = name;
//        this.email = email;
//        this.creativeCloudUser = creativeCloudUser;
//    }
}
