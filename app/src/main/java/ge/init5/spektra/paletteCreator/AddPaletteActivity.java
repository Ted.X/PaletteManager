package ge.init5.spektra.paletteCreator;

import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.github.danielnilsson9.colorpickerview.dialog.ColorPickerDialogFragment;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import ge.init5.spektra.R;
import ge.init5.spektra.palettes.palette.PaletteColor;
import ge.init5.spektra.palettes.palette.RecyclerViewPalette;
import ge.init5.spektra.services.FirebasePaletteDatabase.PaletteBase;
import ge.init5.spektra.utils.Logger;
import ge.init5.spektra.utils.colorUtils.ColorConverter;
import mabbas007.tagsedittext.TagsEditText;

import static ge.init5.spektra.MainActivity.CAMERA_REQUEST_CODE;
import static ge.init5.spektra.MainActivity.EXTERNAL_READ_CODE;

public class AddPaletteActivity extends AppCompatActivity
        implements ColorPickerDialogFragment.ColorPickerDialogListener, TagsEditText.TagsEditListener,
            Switch.OnCheckedChangeListener{

    private static final String TAG = AddPaletteActivity.class.getSimpleName();

    @BindView(R.id.palette_name)
    public EditText mPaletteTitle;

    @BindView(R.id.public_switch)
    public SwitchCompat mPublicSwitch;
    private Boolean isPalettePublic = false;

    @BindView(R.id.submit_palette)
    public FloatingActionButton mSubmitButton;

    @BindView(R.id.palette_recycler_view)
    public RecyclerView recyclerView;

    @BindView(R.id.tagsEditText)
    public TagsEditText tagsEditText;

    @BindView(R.id.generate_button)
    public Button generateButton;

    @BindView(R.id.include_loading)
    public View loader;

    private List<String> paletteTagsNew = new ArrayList<>();

    private AddPaletteAdapter paletteAdapter;

    private RecyclerViewPalette paletteToEdit;

    @BindView(R.id.image_switch)
    public SwitchCompat mImageSwitch;
    private boolean hasImage = false;
    private Uri imageUri = null;
    private Uri imageScaledUri = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_palette);


        Logger.FireLog(TAG, "Add Palette Activity Started.");


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        ButterKnife.bind(this);

        Bundle bundle = getIntent().getExtras();
//        Uri uri = getIntent().getData();

        if(bundle != null)
            paletteToEdit = bundle.getParcelable("palette");

        int[] colorsFromBundle = null;


        if(paletteToEdit != null){
            mPaletteTitle.setText(paletteToEdit.getTitle());
            isPalettePublic = paletteToEdit.isPublic;
            hasImage = paletteToEdit.hasImage;
            if(paletteToEdit.tags != null) {
                String[] tags = paletteToEdit.tags.toArray(new String[paletteToEdit.tags.size()]);
                tagsEditText.setTags(tags);
            }

            colorsFromBundle = new int[5];
            for(int i = 0; i < 5; i++)
                colorsFromBundle[i] = paletteToEdit.colors[i].getInt();

        }else if(bundle != null ) {
            if(bundle.containsKey("colors"))
                colorsFromBundle = bundle.getIntArray("colors");
            if(bundle.containsKey("image_uri") && bundle.containsKey("image_scaled_uri")) {
                hasImage = true;
                imageUri = Uri.parse(bundle.getString("image_uri"));
                imageScaledUri = Uri.parse(bundle.getString("image_scaled_uri"));
            }
        }

        generateButton.setEnabled(!hasImage);

        paletteAdapter = new AddPaletteAdapter(this, hasImage, colorsFromBundle, getFragmentManager());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(paletteAdapter);

        tagsEditText.setTagsListener(this);
        tagsEditText.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line));
        tagsEditText.setThreshold(1);

        if (hasImage) {
            mImageSwitch.setVisibility(View.VISIBLE);
            mImageSwitch.setChecked(true);
            mImageSwitch.setOnCheckedChangeListener(this);

            mPublicSwitch.setVisibility(View.GONE);
            isPalettePublic = true;
//            mPublicSwitch.setEnabled(false);
//            mPublicSwitch.setText("");
        } else {
            mImageSwitch.setVisibility(View.GONE);
            mPublicSwitch.setChecked(isPalettePublic);
            mPublicSwitch.setText(isPalettePublic ? mPublicSwitch.getTextOn() : mPublicSwitch.getTextOff());
            mPublicSwitch.setOnCheckedChangeListener(this);
        }
    }


    // SUBMIT PALETTE
    public void submitPalette(View v) {

        Logger.FireLog(TAG, "Submit Palette Called.");

        final String title = mPaletteTitle.getText().toString();
        List<String> paletteTagsOld = new ArrayList<>();

        if (TextUtils.isEmpty(title)){
            mPaletteTitle.setError("Required");
            return;
        }

        Pattern p = Pattern.compile("[^a-zA-Z0-9 ]+", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(title);

        if (m.find()){
            mPaletteTitle.setError("Avoid using non-alphabetic characters in palette name.");
            return;
        }

        if(title.length() < 2){
            mPaletteTitle.setError("Too short");
            return;
        }

        if(title.equals("test")){
            mPaletteTitle.setError("Use some imagination");
            return;
        }

        final List<PaletteColor> colors = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            int color = paletteAdapter.holders.get(i).imageColor;
            if(color == Color.TRANSPARENT){
                Snackbar.make(v, "Choose All Colors", Snackbar.LENGTH_SHORT).show();
                return;
            }
            colors.add(new PaletteColor(paletteAdapter.holders.get(i).imageColor));
        }

        if (paletteTagsNew.size() > 0){

            paletteTagsNew.removeAll(Arrays.asList("", null));

            Set<String> hs = new HashSet<>();
            hs.addAll(paletteTagsNew);
            paletteTagsNew.clear();

            if (hs.size() <= 7){
                for (String each : hs){
                    if (each.length() <= 15){
                        if (p.matcher(each).find()){
                            tagsEditText.setError("Avoid using non-alphabetic characters in palette name.");
                            return;
                        }
                        paletteTagsNew.add(each);
                    }else{
                        tagsEditText.setError("No more than 15 characters in each tag.");
                        return;
                    }
                }
            }else{
                tagsEditText.setError("No more than 7 tags.");
                return;
            }
        }

        String key = null;
        String oldName = "";
        if(paletteToEdit != null) {
            key = paletteToEdit.firebaseKey;
            paletteTagsOld = paletteToEdit.tags;
            oldName = paletteToEdit.getTitle();
        }

        setEditingEnabled(false);

        loader.setVisibility(View.VISIBLE);
        Snackbar.make(v, "Sending...", Snackbar.LENGTH_SHORT).show();

        // Creating
        if(paletteToEdit == null) {
            PaletteBase.getInstance().writeNewUserPalette(this, v, title, oldName, key, isPalettePublic, hasImage, imageUri, imageScaledUri,
                    colors, paletteTagsNew, paletteTagsOld, loader);
        }
        // Changing
        else{
            PaletteBase.getInstance().updateUserPalette(this, key, title, paletteTagsNew, colors, loader);
        }
    }


    public void setEditingEnabled(boolean enabled){
        mPaletteTitle.setEnabled(enabled);
        if (enabled){
            mSubmitButton.setVisibility(View.VISIBLE);
        }else{
            mSubmitButton.setVisibility(View.GONE);
        }
    }


    public void generateColors(View v){

        Logger.FireLog(TAG, "Generate Colors Called.");

        Random rand = new Random();
        for (AddPaletteViewHolder holder : paletteAdapter.holders) {
            int r = rand.nextInt(256);
            int g = rand.nextInt(256);
            int b = rand.nextInt(256);
            holder.setImageColorAndValues(Color.rgb(r,g,b));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_REQUEST_CODE){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Logger.FireLog(TAG, "Open Camera");
                AddPaletteViewHolder.currentHolder.openCamera();
            }else {
                Logger.FireLog(TAG, "Image Capture Permission Not Granted.");
                Toast.makeText(this, "Permission not granted", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onColorSelected(int dialogId, int color) {
        Logger.FireLog(TAG, "On Color Selected Called.");

        paletteAdapter.holders.get(dialogId).setImageColorAndValues(color);
        Toast.makeText(this, "Selected: " + ColorConverter.IntToHEX(color), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDialogDismissed(int dialogId) {
        Logger.FireLog(TAG, "On Dialog Dismissed Called.");
    }

    @Override
    public void onTagsChanged(Collection<String> collection) {
        paletteTagsNew = (List<String>)collection;
    }

    @Override
    public void onEditingFinished() {
        Logger.FireLog(TAG, "On Editing Finished Called.");
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


        if (buttonView.getId() == R.id.public_switch){
            mPublicSwitch.setText(isChecked ? mPublicSwitch.getTextOn() : mPublicSwitch.getTextOff());
            Logger.FireLog(TAG, isChecked ? "Palette Made Public." : "Palette Made Private." );
            isPalettePublic = isChecked;
        }
        else{
            hasImage = isChecked;
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
