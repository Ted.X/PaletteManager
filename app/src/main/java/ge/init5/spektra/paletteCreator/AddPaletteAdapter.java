package ge.init5.spektra.paletteCreator;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.danielnilsson9.colorpickerview.dialog.ColorPickerDialogFragment;

import java.util.ArrayList;
import java.util.List;

import ge.init5.spektra.R;
import ge.init5.spektra.utils.Logger;

/**
 * Created by __ted__ on 12/25/16.
 */

class AddPaletteAdapter extends RecyclerView.Adapter<AddPaletteViewHolder> {

    private static final String TAG = AddPaletteAdapter.class.getSimpleName();
    List<AddPaletteViewHolder> holders;


    private Context context;
    private FragmentManager fm;
    private int[] colorsFromImage;

    private boolean hasImage;

    AddPaletteAdapter(Context context, boolean hasImage, int[] colorsFromBundle, FragmentManager fm){
        Logger.FireLog(TAG, "AddPaletteAdapter constructor called.");
        this.context = context;
        this.hasImage = hasImage;
        this.holders = new ArrayList<>();
        this.fm = fm;
        if(colorsFromBundle != null)
            this.colorsFromImage = colorsFromBundle;
    }

    @Override
    public AddPaletteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_palette_view,
                parent, false);
        AddPaletteViewHolder pvh = new AddPaletteViewHolder(context, v);
        this.holders.add(pvh);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final AddPaletteViewHolder holder, final int position) {
        if(colorsFromImage != null)
            holder.setImageColorAndValues(colorsFromImage[position]);

        holder.clickableMask.setVisibility(hasImage ? View.VISIBLE : View.GONE);
        if(!hasImage) {
            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (holder.imageColor == Color.TRANSPARENT)
                        holder.imageColor = Color.BLACK;

                    ColorPickerDialogFragment f = ColorPickerDialogFragment
                            .newInstance(position, "Choose Color", "Ok", holder.imageColor, false);


                    f.setStyle(DialogFragment.STYLE_NORMAL, R.style.LightPickerDialogTheme);
                    f.show(fm, "Color Picker");
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return 5;
    }


}
