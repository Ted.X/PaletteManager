package ge.init5.spektra.paletteCreator;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.hardware.Camera;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.ViewFlipper;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import ge.init5.spektra.R;
import ge.init5.spektra.services.FirebasePaletteDatabase.PaletteBase;
import ge.init5.spektra.utils.Logger;
import ge.init5.spektra.utils.colorUtils.ColorConverter;
import ge.init5.spektra.utils.colorUtils.DominantColors;

import static ge.init5.spektra.MainActivity.CAMERA_REQUEST_CODE;


/**
 * Created by __ted__ on 12/29/16.
 */

class AddPaletteViewHolder extends RecyclerView.ViewHolder implements Camera.PreviewCallback{

    private static final String TAG = AddPaletteViewHolder.class.getSimpleName();
    private static boolean COLOR_VALUE_FIXED = false;

    @BindView(R.id.input_card_view)
    CardView cardView;

    @BindView(R.id.clickable_mask)
    ImageView clickableMask;

    @BindView(R.id.spinner)
    Spinner spinner;

    @BindView(R.id.app_palette_view_flipper)
    ViewFlipper colorInputTypeFlipper;

    @BindView(R.id.flipper)
    ViewFlipper colorValueFlipper;

    @BindView(R.id.color_view)
    ImageView imageView;

    @BindView(R.id.camera_button)
    ImageButton onCameraChangeButton;

    @BindView(R.id.color_capture_button)
    ImageButton captureRecordButton;

    @BindView(R.id.color_capture_disable)
    ImageButton captureCloseButton;

    @BindView(R.id.camera_colorpicker_view)
    CustomCameraView cameraView;

    public static AddPaletteViewHolder currentHolder;

    int imageColor = Color.TRANSPARENT;

    private Context context;
    private boolean changed = false;


    private Map<EditText, TextWatcher> textWatcherList;



    AddPaletteViewHolder(final Context context, final View itemView) {
        super(itemView);
        Logger.FireLog(TAG, "AddPaletteViewHolder constructor called.");

        ButterKnife.bind(this, itemView);

        this.context = context;

        SharedPreferences shp = PaletteBase.getInstance().sharedPreferences;
        int val = 2;
        if(shp != null)
            val = Integer.parseInt(shp.getString("color_input_type", "2"));

        spinner.setSelection(val);
        colorValueFlipper.setDisplayedChild(val);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                colorValueFlipper.setDisplayedChild(position);

                if(changed && imageColor != Color.TRANSPARENT)
                    SetColorValues(colorValueFlipper.getCurrentView());
                setColorValueChangeListeners(colorValueFlipper.getCurrentView());
                changed = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {}
        });

        Logger.FireLog(TAG, "Spinner listener set.");

        onCameraChangeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= 23 && context.checkSelfPermission(android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
                    ((AppCompatActivity) context).requestPermissions(new String[]{ android.Manifest.permission.CAMERA }, CAMERA_REQUEST_CODE);
                }else {
                    openCamera();
                }

            }
        });

        Logger.FireLog(TAG, "Camera change button listener set.");

        captureCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentHolder.colorInputTypeFlipper.setDisplayedChild(0);
                currentHolder.cameraView.releaseCamera();
                SetColorValues(colorValueFlipper.getCurrentView());
            }
        });

        Logger.FireLog(TAG, "Capture close button listener set.");

        captureRecordButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // Do something
                        currentHolder.cameraView.camera.setPreviewCallback(AddPaletteViewHolder.this);
                        return true;
                    case MotionEvent.ACTION_UP:
                        currentHolder.cameraView.camera.setPreviewCallback(null);
                        // No longer down
                        return true;
                }
                return false;
            }
        });

        Logger.FireLog(TAG, "Capture record button listener set.");

    }

    public void setImageColorAndValues(int color){
        imageColor = color;
        SetColorValues(colorValueFlipper.getCurrentView());
        imageView.setBackgroundColor(imageColor);
    }

    public void openCamera(){
        if (currentHolder != null) {
            currentHolder.colorInputTypeFlipper.setDisplayedChild(0);
            currentHolder.cameraView.releaseCamera();
        }

        currentHolder = AddPaletteViewHolder.this;
        colorInputTypeFlipper.setDisplayedChild(1);
        currentHolder.cameraView.setCamera();
    }


    private void setColorValueChangeListeners(View view){
        Logger.FireLog(TAG, "Color value change listener called.");


        if(textWatcherList != null && textWatcherList.size() > 0) {
            for (Map.Entry<EditText, TextWatcher> entry : textWatcherList.entrySet()) {
                entry.getKey().removeTextChangedListener(entry.getValue());
            }
        }

        textWatcherList = new HashMap<>();

        switch (view.getId()){
            case R.id.include_rgb:
                EditText redText = (EditText) view.findViewById(R.id.value_red);
                EditText greenText = (EditText) view.findViewById(R.id.value_green);
                EditText blueText = (EditText) view.findViewById(R.id.value_blue);


                TextWatcher redWatcher = setColorRangeChecker(view, redText);
                TextWatcher greenWatcher = setColorRangeChecker(view, greenText);
                TextWatcher blueWatcher = setColorRangeChecker(view, blueText);

                redText.addTextChangedListener(redWatcher);
                greenText.addTextChangedListener(greenWatcher);
                blueText.addTextChangedListener(blueWatcher);


                textWatcherList.put(redText, redWatcher);
                textWatcherList.put(greenText, greenWatcher);
                textWatcherList.put(blueText, blueWatcher);

                break;
            case R.id.include_hsv:
                EditText hueText = (EditText) view.findViewById(R.id.value_hue);
                EditText satText = (EditText) view.findViewById(R.id.value_sat);
                EditText valText = (EditText) view.findViewById(R.id.value_val);

                TextWatcher hueWatcher = setColorRangeChecker(view, hueText);
                TextWatcher satWatcher = setColorRangeChecker(view, satText);
                TextWatcher valWatcher = setColorRangeChecker(view, valText);


                hueText.addTextChangedListener(hueWatcher);
                satText.addTextChangedListener(satWatcher);
                valText.addTextChangedListener(valWatcher);

                textWatcherList.put(hueText, hueWatcher);
                textWatcherList.put(satText, satWatcher);
                textWatcherList.put(valText, valWatcher);


                break;
            case R.id.include_hex:
                EditText hexText = (EditText) view.findViewById(R.id.value_hex);

                TextWatcher hexWatcher = setColorRangeChecker(view, hexText);


                hexText.addTextChangedListener(hexWatcher);

                textWatcherList.put(hexText, hexWatcher);

                break;

        }
    }


    private TextWatcher setColorRangeChecker(final View parent, final EditText editText){
        return new TextWatcher(){
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String changedString = "";
                String fieldText = editText.getText().toString();
                if(COLOR_VALUE_FIXED || fieldText.length() < 1) {
                    COLOR_VALUE_FIXED = false;
                    return;
                }

                switch (parent.getId()) {
                    case R.id.include_rgb:
                        Logger.FireLog(TAG, "Include rgb.");

                        int value1 = Integer.parseInt(fieldText);
                        changedString = ColorConverter.fixRange(changedString, value1, 0, 255);
                        break;
                    case R.id.include_hsv:
                        Logger.FireLog(TAG, "Include hsv.");

                        try{
                            float value2 = Float.valueOf(fieldText);
                            switch (editText.getId()) {
                                case R.id.value_hue:
                                    changedString = ColorConverter.fixRange(changedString, (int)value2, 0, 360);
                                    break;
                                case R.id.value_sat:
                                    changedString = ColorConverter.fixRange(changedString, value2, 0, 1);
                                    break;
                                case R.id.value_val:
                                    changedString = ColorConverter.fixRange(changedString, value2, 0, 1);
                                    break;
                            }
                        }catch(NumberFormatException ex){
                            Logger.FireCrash(ex);
                            editText.setError("Invalid Format try: [0.0 - 1.0]");
                        }

                        break;
                    case R.id.include_hex:
                        Logger.FireLog(TAG, "Include hex.");

                        if(fieldText.length() == 6) {
                            short[] rgb = ColorConverter.HEXtoRGB(fieldText);
                            rgb[0] = ColorConverter.fixRange(rgb[0], 0, 255);
                            rgb[1] = ColorConverter.fixRange(rgb[1], 0, 255);
                            rgb[2] = ColorConverter.fixRange(rgb[2], 0, 255);
                            changedString = ColorConverter.RGBtoHEX(rgb);

                        }
                        break;
                }


                if(changedString.length() > 0) {
                    COLOR_VALUE_FIXED = true;
                    editText.setText(changedString);

                }
                SetHolderImageColor(parent);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        };
    }

    private void SetHolderImageColor(View parent){
        switch (parent.getId()){
            case R.id.include_rgb:
                EditText redText = (EditText) parent.findViewById(R.id.value_red);
                EditText greenText = (EditText) parent.findViewById(R.id.value_green);
                EditText blueText = (EditText) parent.findViewById(R.id.value_blue);

                imageColor = ColorConverter.RGBtoInt(redText.getText().toString(),
                        greenText.getText().toString(),
                        blueText.getText().toString());
                imageView.setBackgroundColor(imageColor);

                break;
            case R.id.include_hsv:
                EditText hueText = (EditText) parent.findViewById(R.id.value_hue);
                EditText satText = (EditText) parent.findViewById(R.id.value_sat);
                EditText valText = (EditText) parent.findViewById(R.id.value_val);

                imageColor = ColorConverter.HSVtoInt(hueText.getText().toString(),
                        satText.getText().toString(),
                        valText.getText().toString());

                imageView.setBackgroundColor(imageColor);
                break;
            case R.id.include_hex:
                EditText hexText = (EditText) parent.findViewById(R.id.value_hex);
                if(hexText.getText().toString().length() == 6) {
                    imageColor = ColorConverter.HEXtoInt(hexText.getText().toString());

                    imageView.setBackgroundColor(imageColor);
                }
                break;
        }
    }


    private void SetColorValues(View parent){
        switch (parent.getId()){
            case R.id.include_rgb:
                EditText redText = (EditText) parent.findViewById(R.id.value_red);
                EditText greenText = (EditText) parent.findViewById(R.id.value_green);
                EditText blueText = (EditText) parent.findViewById(R.id.value_blue);


                short[] rgb = ColorConverter.InttoRGB(imageColor);

                redText.setText(rgb[0] + "");
                greenText.setText(rgb[1] + "");
                blueText.setText(rgb[2] + "");

                break;
            case R.id.include_hsv:
                EditText hueText = (EditText) parent.findViewById(R.id.value_hue);
                EditText satText = (EditText) parent.findViewById(R.id.value_sat);
                EditText valText = (EditText) parent.findViewById(R.id.value_val);

                float[] hsv = ColorConverter.IntToHSV(imageColor);
                hueText.setText(hsv[0] + "");
                satText.setText(hsv[1] + "");
                valText.setText(hsv[2] + "");

                break;
            case R.id.include_hex:
                EditText hexText = (EditText) parent.findViewById(R.id.value_hex);

                String hex = ColorConverter.IntToHEX(imageColor);

                hexText.setText(hex);

                break;
        }
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        int frameHeight = camera.getParameters().getPreviewSize().height;
        int frameWidth = camera.getParameters().getPreviewSize().width;

        // number of pixels//transforms NV21 pixel data into RGB pixels
        int rgb[] = new int[frameHeight * frameWidth];


        // convertion
        decodeYUV420SP(rgb, data, frameWidth, frameHeight);

        imageColor = cropPictureData(rgb, frameWidth, frameHeight, 10, 10);

//        imageColor = rgb[frameHeight*frameWidth/2];
        imageView.setBackgroundColor(imageColor);
    }

    private void decodeYUV420SP(int[] rgb, byte[] yuv420sp, int width, int height) {
        // Pulled directly from:
        // http://ketai.googlecode.com/svn/trunk/ketai/src/edu/uic/ketai/inputService/KetaiCamera.java
        final int frameSize = width * height;

        for (int j = 0, yp = 0; j < height; j++) {
            int uvp = frameSize + (j >> 1) * width, u = 0, v = 0;
            for (int i = 0; i < width; i++, yp++) {
                int y = (0xff & ((int) yuv420sp[yp])) - 16;
                if (y < 0)
                    y = 0;
                if ((i & 1) == 0) {
                    v = (0xff & yuv420sp[uvp++]) - 128;
                    u = (0xff & yuv420sp[uvp++]) - 128;
                }

                int y1192 = 1192 * y;
                int r = (y1192 + 1634 * v);
                int g = (y1192 - 833 * v - 400 * u);
                int b = (y1192 + 2066 * u);

                if (r < 0)
                    r = 0;
                else if (r > 262143)
                    r = 262143;
                if (g < 0)
                    g = 0;
                else if (g > 262143)
                    g = 262143;
                if (b < 0)
                    b = 0;
                else if (b > 262143)
                    b = 262143;

                rgb[yp] = 0xff000000 | ((r << 6) & 0xff0000) | ((g >> 2) & 0xff00) | ((b >> 10) & 0xff);
            }
        }
    }

    private int cropPictureData(int[] data, int width, int height, int cropWidth, int cropHeight){

        int from = (int) Math.ceil(width * height/2f + width/2f);
        int to = from + cropWidth*cropHeight;

        Logger.LogDebug(TAG, "DataLength " + data.length + "FROM "  + from + " TO " + to);

        int[] colors = Arrays.copyOfRange(data, from, to);

        Bitmap b = Bitmap.createBitmap(colors, cropWidth, cropHeight, Bitmap.Config.ARGB_8888);

        return DominantColors.kmeans(b, 1)[0];
//        return DominantColors.kMeans(colors, 1, DominantColors.DEFAULT_MIN_DIFF)[0];

//        Logger.LogDebug("COLORS : " , colors.length+"§");
//        int redBucket = 0;
//        int greenBucket = 0;
//        int blueBucket = 0;
//
//        for (int z = 0; z < colors.length; z++) {
//            redBucket += Color.red(colors[z]);
//            greenBucket += Color.green(colors[z]);
//            blueBucket += Color.blue(colors[z]);
//        }
//
//        if(colors.length == 0)
//            return 0;
//
//        return Color.argb(255, redBucket/colors.length, greenBucket/colors.length, blueBucket/colors.length);

//        int[] cropped = new int[colors.size()];
//        for (int i = 0; i < colors.size(); i++) {
//            cropped[i] = colors.get(i);
//        }
//        return cropped;
    }
}
