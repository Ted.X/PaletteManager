package ge.init5.spektra.paletteCreator;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Build;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import ge.init5.spektra.utils.Logger;

import static ge.init5.spektra.MainActivity.CAMERA_REQUEST_CODE;

/**
 * Created by __ted__ on 1/14/17.
 */

public class CustomCameraView extends ViewGroup implements SurfaceHolder.Callback {

    private static final String TAG = CustomCameraView.class.getSimpleName();
    private SurfaceView mSurfaceView;

    Camera camera;

    boolean surfaceAdded = false;

    int cameraWidth;
    int cameraHeight;


    public CustomCameraView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mSurfaceView = new SurfaceView(context);
        mSurfaceView.getHolder().addCallback(this);
        addView(mSurfaceView);
        surfaceAdded = true;
    }



    public void releaseCamera(){
        mSurfaceView.getHolder().removeCallback(this);
        mSurfaceView.setVisibility(GONE);
        camera.release();
        removeView(mSurfaceView);
        surfaceAdded = false;
    }

    public void setCamera(){
        if(!surfaceAdded){
            addView(mSurfaceView);
            surfaceAdded = true;
        }
        mSurfaceView.setVisibility(View.VISIBLE);
        mSurfaceView.getHolder().addCallback(this);

        if (Build.VERSION.SDK_INT >= 23 &&
                getContext().checkSelfPermission(android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            try{
                ((AddPaletteActivity) getContext()).requestPermissions(new String[]{ android.Manifest.permission.CAMERA }, CAMERA_REQUEST_CODE);
            }catch (Exception e) {
                Logger.FireCrash(e);
            }
        }else {
            camera = Camera.open();
            if (camera == null) {
                Logger.LogError(TAG, "Cant Find Camera");
                return;
            }
        }

    }


    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

        if (mSurfaceView != null && camera != null) {
            Camera.Size size = camera.getParameters().getPreviewSize();

//            int lx = (getResources().getDisplayMetrics().widthPixels - size.height)/2;
//            int ty = (getResources().getDisplayMetrics().heightPixels - size.width)/2;
//            int rx = lx + size.width;
//            int by = ty + size.height;

            float width = getResources().getDisplayMetrics().widthPixels;
            float height = getResources().getDisplayMetrics().heightPixels;

//            size.height / width

            mSurfaceView.layout(0, 0, size.height, size.width);

            Logger.LogDebug(TAG, "Width : " + size.width + " Height: " + size.height);
            Logger.LogDebug(TAG, "POSITION Parent: " + this.getHeight() / 2 + "");
            Logger.LogDebug(TAG, "POSITION Child: " + size.height / 2 + "");


            mSurfaceView.setY(this.getHeight() / 2 - size.width/2);
            mSurfaceView.setX(this.getWidth() / 2 - size.height/ 2);
        }
    }


    @Override
    public void surfaceCreated(SurfaceHolder holder) {


        Camera.Parameters parameters = camera.getParameters();
        parameters.setPreviewFrameRate(20);
//        parameters.setPreviewSize(getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels);
//        camera.setParameters(parameters);
        camera.setDisplayOrientation(90);

        try {
            camera.setPreviewDisplay(holder);
            camera.startPreview();
        }catch (Exception e){
            Logger.FireCrash(e);
            e.printStackTrace();
        }


    }


    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        Logger.LogError(TAG, "Surface Changed");
//
//        Camera.Parameters parameters = camera.getParameters();
//
//
//        camera.setParameters(parameters);
//        camera.startPreview();

        this.requestLayout();
//
//
        try {
            camera.setPreviewDisplay(holder);
            camera.startPreview();

        } catch (Exception t) {
            Logger.LogError(TAG, t.getMessage());
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Logger.LogError(TAG, "Surface Destroyed");
        if(camera != null){
            camera.stopPreview();
            camera.setPreviewCallback(null);


            camera.release();
            camera = null;
        }


    }



}
