package ge.init5.spektra.paletteEditor.mediaAdapter;

import java.io.File;

/**
 * Created by __ted__ on 2/6/17.
 */

public interface GridAdapterListener {

    void onClickMediaItem(File file);

}

