package ge.init5.spektra.paletteEditor.mediaAdapter;

import java.io.File;

/**
 * Created by __ted__ on 2/6/17.
 */

interface MediaItemViewListener {
    void onClickItem(File file);
}
