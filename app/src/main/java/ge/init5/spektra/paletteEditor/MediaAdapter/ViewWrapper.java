package ge.init5.spektra.paletteEditor.mediaAdapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by __ted__ on 2/6/17.
 */

public class ViewWrapper<V extends View> extends RecyclerView.ViewHolder {

    private final V view;

    ViewWrapper(V itemView) {
        super(itemView);
        view = itemView;
    }

    public V getView() {
        return view;
    }
}
