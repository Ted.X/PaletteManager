package ge.init5.spektra.paletteEditor.crop;

import android.annotation.TargetApi;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.naver.android.helloyako.imagecrop.util.BitmapLoadUtils;
import com.naver.android.helloyako.imagecrop.view.ImageCropView;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;
import ge.init5.spektra.R;
import ge.init5.spektra.paletteEditor.mediaAdapter.GridAdapter;
import ge.init5.spektra.paletteEditor.mediaAdapter.GridAdapterListener;

public class CropActivity extends AppCompatActivity implements GridAdapterListener {

    public static final String TAG = CropActivity.class.getSimpleName();
    private static final float MIN_SCALE = 1f;
    private static final float MAX_SCALE = 2f;

    @BindView(R.id.mGalleryRecyclerView)
    RecyclerView mGalleryRecyclerView;

    @BindView(R.id.mPreview)
    ImageCropView mPreview;



    private static final String EXTENSION_JPG = ".jpg";
    private static final String EXTENSION_JPEG = ".jpeg";
    private static final String EXTENSION_PNG = ".png";
    private static final int MARGIN_GRID = 2;

    public static int RATIO_WIDTH = 10;
    public static int RATIO_HEIGHT = 7;

    private GridAdapter mGridAdapter;
    private ArrayList<File> mFiles;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop);
        ButterKnife.bind(this);



        mGridAdapter = new GridAdapter(getApplicationContext());

        mGridAdapter.setListener(this);
        mGalleryRecyclerView.setAdapter(mGridAdapter);
        mGalleryRecyclerView.setHasFixedSize(true);
		GridLayoutManager mGridLayoutManager = new GridLayoutManager(getApplicationContext(), 4);
		//mGridLayoutManager.setReverseLayout(true);
        mGalleryRecyclerView.setLayoutManager(mGridLayoutManager);
        mGalleryRecyclerView.addItemDecoration(addItemDecoration());

//        CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) mAppBarContainer.getLayoutParams();
//        lp.height = getResources().getDisplayMetrics().widthPixels;
//        mAppBarContainer.setLayoutParams(lp);

        fetchMedia();

        ChangeAspectRatio();

    }

    private RecyclerView.ItemDecoration addItemDecoration() {
        return new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view,
                                       RecyclerView parent, RecyclerView.State state) {
                outRect.left = MARGIN_GRID;
                outRect.right = MARGIN_GRID;
                outRect.bottom = MARGIN_GRID;
                if (parent.getChildLayoutPosition(view) >= 0 && parent.getChildLayoutPosition(view) <= 4) {
                    outRect.top = MARGIN_GRID;
                }
            }
        };
    }

    private void fetchMedia() {
        mFiles = new ArrayList<>();
        File dirPubDownloads = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        parseDir(dirPubDownloads);
        File dirPubDcim = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        parseDir(dirPubDcim);
        File dirPubPictures = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        parseDir(dirPubPictures);

//		File all = Environment.getExternalStorageDirectory();
//        parseDir(all);


        Collections.sort(mFiles, new Comparator<File>() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public int compare(File o1, File o2) {
                    return Long.compare(o2.lastModified(), o1.lastModified());
                }
            }
        );
        // Get intent, action and MIME type
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        String imagePath = null;

        if (mFiles.size() > 0) {
            mGridAdapter.setItems(mFiles);
            imagePath = Uri.fromFile(mFiles.get(0)).getPath();
        } else {
//            mMediaNotFoundWording.setVisibility(View.VISIBLE);
        }

        if(intent.hasExtra("data")){
            imagePath = intent.getData().getPath();
        }

        Uri data = intent.getData();
        if(data != null){
            imagePath = data.getPath();
        }

        if (Intent.ACTION_SEND.equals(action) && type != null && type.startsWith("image/")) {
            Uri uri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
            if (uri != null) {
                imagePath = getRealPathFromURI(uri);
            }
        }

        if(imagePath != null)
            SetPreviewImage(imagePath);
//            mPreview.setImageFilePath(imagePath);


    }


    public String getRealPathFromURI(Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private void parseDir(File dir) {
        File[] files = dir.listFiles();
        if (files != null) {
            parseFileList(files);
        }
    }

    private void parseFileList(File[] files) {
        for (File file : files) {
            if (file.isDirectory()) {
                if (!file.getName().toLowerCase().startsWith(".")) {
                    parseDir(file);
                }
            } else {
                if (file.getName().toLowerCase().endsWith(EXTENSION_JPG)
                        || file.getName().toLowerCase().endsWith(EXTENSION_JPEG)
                        || file.getName().toLowerCase().endsWith(EXTENSION_PNG)) {
                    mFiles.add(0, file);
                }
            }
        }
    }

    private void displayPreview(File file) {

        SetPreviewImage(Uri.fromFile(file).getPath());

    }

    private void ChangeAspectRatio(){
        int temp = RATIO_HEIGHT;
        RATIO_HEIGHT = RATIO_WIDTH;
        RATIO_WIDTH = temp;

        mPreview.setAspectRatio(RATIO_WIDTH, RATIO_HEIGHT);
    }

    public void SetPreviewImage(String imageFilePath){
        mPreview.setImageFilePath(imageFilePath);

        File imageFile = new File(imageFilePath);
        if (!imageFile.exists()) {
            throw new IllegalArgumentException("Image file does not exist");
        }
        int reqSize = 1000;
        Bitmap bitmap = BitmapLoadUtils.decode(imageFilePath, reqSize, reqSize, true);


        mPreview.setImageBitmap(bitmap, MIN_SCALE, MAX_SCALE);

    }



    @Override
    public void onClickMediaItem(File file) {
        displayPreview(file);
//        mAppBarContainer.setExpanded(true, true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem main_item1 = menu.add(0, 1, 0, "Change Layout");
        main_item1.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        MenuItem main_item2 = menu.add(0, 0, 0, "Next");
        main_item2.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case 0:
                if (!mPreview.isChangingScale()) {
                    Bitmap b = mPreview.getCroppedImage();
                    if (b != null) {
                        new CropAsync(CropActivity.this).execute(b);
                    } else {
                        Toast.makeText(CropActivity.this, "FAILED BITCH", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case 1:
                ChangeAspectRatio();
                break;
            case android.R.id.home:
                onBackPressed();
            default:
                break;
        }

        return true;
    }



}
