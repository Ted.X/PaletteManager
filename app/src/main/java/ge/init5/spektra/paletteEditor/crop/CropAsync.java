package ge.init5.spektra.paletteEditor.crop;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import ge.init5.spektra.paletteEditor.edit.EditActivity;

/**
 * Created by __ted__ on 2/10/17.
 */

public class CropAsync extends AsyncTask<Bitmap, Void, Void>{

    Context context;

    ProgressDialog mProgress;

    public CropAsync(Context context){
        this.context = context;
    }


    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected Void doInBackground(Bitmap... params) {
        Bitmap bitmap = params[0];
        try{
            File file = File.createTempFile("crop", ".png", context.getExternalCacheDir());
            FileOutputStream outputStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            Uri path = Uri.fromFile(file);

            Intent newIntent = new Intent(context, EditActivity.class);
            newIntent.setData(path);
            newIntent.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
            context.startActivity(newIntent);
            ((CropActivity) context).finish();
        } catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        mProgress = new ProgressDialog(context);
        mProgress.setMessage("Loading image...");
        mProgress.setCancelable(false);
        mProgress.show();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        if (mProgress.getWindow() != null) {
            mProgress.dismiss();
        }
    }


}
