package ge.init5.spektra.paletteEditor.edit;

import android.support.design.widget.Snackbar;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ViewFlipper;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ge.init5.spektra.R;
import ge.init5.spektra.paletteEditor.crop.CropActivity;
import ge.init5.spektra.utils.Logger;

import static ge.init5.spektra.paletteEditor.edit.EditActivity.SHOW_VALUES;
import static ge.init5.spektra.paletteEditor.edit.EditActivity.currentHolder;
import static ge.init5.spektra.paletteEditor.edit.EditActivity.currentTrackBall;

/**
 * Created by __ted__ on 2/23/17.
 */

public class ColorSwapAdapter extends RecyclerView.Adapter<ColorSwapAdapter.ColorHolder> implements ItemTouchHelperAdapter {



    public static final String TAG = ColorSwapAdapter.class.getSimpleName();


    private List<Integer> colors;
    private List<ImageView> trackballs;

    private int count;

    ItemTouchHelper touchHelper;



    public ColorSwapAdapter(List<ImageView> trackballs, List<Integer> data) {
        this.trackballs = trackballs;
        this.colors = data;
        this.count = 0;
    }

    public List<Integer> getColors() {
        return colors;
    }

    public void setColors(List<Integer> colors) {
        this.colors = colors;
    }

    @Override
    public ColorHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View view = inflater.inflate(R.layout.draggable_color_view, parent, false);
        view.post(new Runnable() {
            @Override
            public void run() {
                if(CropActivity.RATIO_WIDTH > CropActivity.RATIO_HEIGHT) {
                    view.getLayoutParams().width = parent.getWidth() / 5;
                    view.getLayoutParams().height = parent.getHeight();
                }else {
                    view.getLayoutParams().height = parent.getHeight() / 5;
                    view.getLayoutParams().width = parent.getWidth();
                }
                Logger.LogDebug(TAG, "Changed" + view.getLayoutParams().width + " " + view.getLayoutParams().height);

            }

        });

        final ColorHolder holder = new ColorHolder(view, count);
        count++;


        if(currentHolder == null){
            currentHolder = holder;
        }


        return holder;
    }

    @Override
    public void onBindViewHolder(final ColorHolder holder, final int position) {

        Logger.LogDebug(TAG, "BIND");
        if(colors != null) {
            int c = colors.get(position);

            if (SHOW_VALUES) {
                holder.colorButton.setText(String.format("#%06X", 0xFFFFFF & c));
                holder.colorButton.setTextColor(EditActivity.GetRightColor(c));
            } else {
                holder.colorButton.setTextColor(c);
            }

            holder.colorButton.setBackgroundColor(c);
//            // NOTE: check for getDraggingId() match to set an "invisible space" while dragging
//            holder.container.setVisibility(getDraggingId() == color ? View.INVISIBLE : View.VISIBLE);
//            holder.container.postInvalidate();
        }
    }

    @Override
    public long getItemId(int position) {
        return colors.get(position);
    }

    @Override
    public int getItemCount() {
        return colors.size();
    }




    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {

        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(colors, i, i + 1);
                Collections.swap(trackballs, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(colors, i, i - 1);
                Collections.swap(trackballs, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    public void setTouchHelper(ItemTouchHelper touchHelper) {
        this.touchHelper = touchHelper;
    }

    class ColorHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.container)
        ViewFlipper container;

        @BindView(R.id.color_button)
        Button colorButton;



        public ColorHolder(View itemView, int index) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            setOnClickListener();
        }

        public void setOnClickListener(){
            colorButton.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {

                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            touchHelper.startDrag(ColorHolder.this);
                            break;
                        case MotionEvent.ACTION_CANCEL:
//                            Snackbar.make(v, "Click "  + getAdapterPosition(), Snackbar.LENGTH_SHORT).show();
                            currentHolder = ColorHolder.this;
                            for (int i = 0; i < trackballs.size(); i++) {
                                if(i == getAdapterPosition()) {
                                    currentTrackBall = trackballs.get(i);
                                    trackballs.get(i).setVisibility(View.VISIBLE);
                                }
                                else
                                    trackballs.get(i).setVisibility(View.INVISIBLE);
                            }
                            break;

                    }
//                    if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
//                    }else if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_UP){
//
//                    }

                    return false;
                }
            });
//
//            colorButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Snackbar.make(v, "Click "  + getAdapterPosition(), Snackbar.LENGTH_SHORT).show();
//                    currentHolder = ColorHolder.this;
//                    for (int i = 0; i < trackballs.size(); i++) {
//                        if(i == getAdapterPosition()) {
//                            currentTrackBall = trackballs.get(i);
//                            trackballs.get(i).setVisibility(View.VISIBLE);
//                        }
//                        else
//                            trackballs.get(i).setVisibility(View.INVISIBLE);
//                    }
//                }
//            });


        }

    }

}
