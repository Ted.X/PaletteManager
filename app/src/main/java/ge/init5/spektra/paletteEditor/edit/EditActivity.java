package ge.init5.spektra.paletteEditor.edit;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ViewFlipper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ge.init5.spektra.R;
import ge.init5.spektra.paletteEditor.share.ShareActivity;
import ge.init5.spektra.paletteEditor.crop.CropActivity;
import ge.init5.spektra.utils.Logger;

public class EditActivity extends AppCompatActivity {

    public static boolean SHOW_VALUES = false;

    public static int CARD_DIM = 1000;


    @BindView(R.id.editor)
    CardView editorLayout;

    @BindView(R.id.editor_flipper)
    ViewFlipper flipper;

//    @BindView(R.id.mask)
//    ImageView mask;

    @BindView(R.id.color_generator_button)
    Button generatorButton;

    @BindView(R.id.values_switcher)
    SwitchCompat valueSwitcher;

    @BindView(R.id.watermark_switcher)
    SwitchCompat watermarkSwitcher;

    FrameLayout frame;


    ImageView mainImage;

    List<ImageView> trackballs;
    RecyclerView dragRecyclerView;



    public static View currentTrackBall;
    public static ColorSwapAdapter.ColorHolder currentHolder;

    public List<Integer> colors;


    private Bitmap scaledBitmap;



    private float dX = 0;
    private float dY = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        ButterKnife.bind(this);
        SHOW_VALUES = false;

        if(CropActivity.RATIO_WIDTH > CropActivity.RATIO_HEIGHT) {
            Logger.LogDebug("EDITACTIVITY", "Landscape");
            flipper.setDisplayedChild(1);
        }



        valueSwitcher.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SHOW_VALUES = valueSwitcher.isChecked();
                dragRecyclerView.getAdapter().notifyDataSetChanged();
            }
        });

        watermarkSwitcher.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Snackbar.make(buttonView, "You Cant Right Now",Snackbar.LENGTH_LONG).show();
                watermarkSwitcher.setChecked(true);
            }
        });


        final View currentView = flipper.getCurrentView();

        frame = (FrameLayout) currentView.findViewById(R.id.frame_layout);
        mainImage = (ImageView) currentView.findViewById(R.id.main_picture);


        trackballs = new ArrayList<>();
        dragRecyclerView = (RecyclerView) currentView.findViewById(R.id.color_list_view);


        for (int i=0; i<5; i++) {
            View view = currentView.findViewById(getResources().getIdentifier("color_" + (i + 1), "id", getPackageName()));

            trackballs.add((ImageView) currentView.findViewById(getResources().getIdentifier("trackball" + (i + 1), "id", getPackageName())));
        }


//        dragLinearLayout.setOnViewSwapListener(new DragLinearLayout.OnViewSwapListener() {
//            @Override
//            public void onSwap(View firstView, int firstPosition, View secondView, int secondPosition) {
////                final int firstIndex = getColorViewIndex(firstView);
////                final int secondIndex = getColorViewIndex(secondView);
////                setColorViewIndex(firstView, secondIndex);
////                setColorViewIndex(secondView, firstIndex);
//
//                Collections.swap(colors, firstPosition, secondPosition);
//
//            }
//        });

        colors = new ArrayList<Integer>();
        Collections.addAll(colors, new Integer[]{0,0,0,0,0});


        ColorSwapAdapter adapter = new ColorSwapAdapter(trackballs, colors);

        dragRecyclerView.setHasFixedSize(true);
        dragRecyclerView.setAdapter(adapter);
        dragRecyclerView.setLayoutManager(new LinearLayoutManager(this,
                CropActivity.RATIO_WIDTH > CropActivity.RATIO_HEIGHT ?
                        LinearLayoutManager.HORIZONTAL :
                        LinearLayoutManager.VERTICAL,
                false){
            @Override
            public boolean canScrollVertically() {
                return false;
            }

            @Override
            public boolean canScrollHorizontally() {
                return false;
            }
        });
        dragRecyclerView.setItemAnimator(new DefaultItemAnimator());

        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(dragRecyclerView);
        adapter.setTouchHelper(touchHelper);



//        editorLayout.setLayoutParams(new RelativeLayout.LayoutParams(editorLayout.getWidth(), editorLayout.getWidth()));

        currentTrackBall = trackballs.get(0);

        Intent i = getIntent();
        Uri uri = i.getData();


        if(uri != null) {


            mainImage.setImageURI(uri);


            new File(uri.getPath()).delete();

            mainImage.post(new Runnable() {
                @Override
                public void run() {

                    RelativeLayout.LayoutParams paramss = (RelativeLayout.LayoutParams) editorLayout.getLayoutParams();
                    paramss.height = editorLayout.getWidth();

                    editorLayout.setLayoutParams(paramss);

//                    Logger.LogDebug("EDITORLAYOUT", mainImage.getWidth() + "");
//                    Logger.LogDebug("EDITORLAYOUT", mainImage.getHeight() + "");


                    if(mainImage.getDrawable() != null) {
                        scaledBitmap = Bitmap.createScaledBitmap(((BitmapDrawable) mainImage.getDrawable()).getBitmap(),
                                mainImage.getWidth(), mainImage.getWidth() * CropActivity.RATIO_HEIGHT / CropActivity.RATIO_WIDTH, true);


                        mainImage.setImageBitmap(scaledBitmap);
                        GenerateColors();

                    }
//                    ViewGroup.LayoutParams params = mask.getLayoutParams();
//                    params.height = paramss.height;
//
//                    mask.setLayoutParams(params);

                }
            });
        }

        frame.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    

                    float maxWidth = frame.getWidth() - currentTrackBall.getWidth()/2 - 1;
                    float maxHeight = frame.getHeight() - currentTrackBall.getHeight()/2 - 1;

                    float minWidth = -currentTrackBall.getWidth()/2 + 1;
                    float minHeight = -currentTrackBall.getHeight()/2 + 1;


                    float x = event.getX();
                    float y = event.getY();

                    switch(event.getAction())
                    {
                        case MotionEvent.ACTION_DOWN :
                            dX = currentTrackBall.getX() - x;
                            dY = currentTrackBall.getY() - y;

                            return true;
                        case MotionEvent.ACTION_MOVE :

                            float mX = x+dX;
                            float mY = y+dY;
//                            Logger.LogDebug("TOUCH Action " , "Move, Delta: (" + mX + ", " + mY+")");


                            mX = CheckBounds(mX, minWidth, maxWidth);
                            mY = CheckBounds(mY, minHeight, maxHeight);

                            currentTrackBall.setX(mX);
                            currentTrackBall.setY(mY);

                            GetColor(currentTrackBall);

                            return true;
                        case MotionEvent.ACTION_UP :
//                            int c = colors.get(currentIndex);//(ColorDrawable)currentColorView.getBackground()).getColor();
//
////                            dragRecyclerView.getAdapter().notifyItemChanged();
//                            if(!SHOW_VALUES)
//                                currentColorView.setTextColor(c);
//                            else{
//
//                                currentColorView.setTextColor(GetRightColor(c));
//                            }
                            dragRecyclerView.getAdapter().notifyDataSetChanged();
                            return true;
                    }
                    return false;
                }
            }

        );

    }







    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.main_menu, menu);
        MenuItem edit_item = menu.add(0, 0, 0, "Next");
        edit_item.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case 0:

                Intent newIntent = new Intent(EditActivity.this, ShareActivity.class);
                Uri uri = GenerateImage();
                if(uri != null) {
                    newIntent.putExtra("image_uri", uri.toString());
                    int[] cc = new int[5];

                    for (int i = 0; i < 5; i++) {
                        cc[i] = colors.get(i);

                    }
                    newIntent.putExtra("colors", cc);
                    newIntent.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                    startActivity(newIntent);
                    finish();
                }

                break;
            case android.R.id.home:
                onBackPressed();
            default:
                break;
        }

        return true;
    }


    public void GenerateColors(){
        Bitmap bitmap = ((BitmapDrawable)mainImage.getDrawable()).getBitmap();
        new KmeanAsync(EditActivity.this, dragRecyclerView, trackballs).execute(bitmap);

    }


    public Uri GenerateImage(){

        boolean IsPortrait = CropActivity.RATIO_WIDTH < CropActivity.RATIO_HEIGHT;


        float imageWidth = CARD_DIM * CropActivity.RATIO_WIDTH/10f;
        float imageHeight = CARD_DIM * CropActivity.RATIO_HEIGHT/10f;
        float padding = CARD_DIM*0.015f;

        float lastX = 0;
        float lastY = 0;

        float colorWidth = CARD_DIM * (IsPortrait ? 0.285f : 0.188f);
        float colorHeight= CARD_DIM * (IsPortrait ? 0.188f : 0.285f);

        float watermarkWidth = Math.max(colorWidth, colorHeight);
        float watermarkHeight = Math.min(colorWidth, colorHeight);

        Bitmap watermark = Bitmap.createScaledBitmap(((BitmapDrawable) getResources().getDrawable(R.drawable.apptitlelogo)).getBitmap(),
                (int)watermarkWidth, (int)watermarkHeight, true);


        Bitmap bitmap = Bitmap.createScaledBitmap(scaledBitmap, (int)imageWidth, (int)imageHeight, true);

        Bitmap imageBitmap = Bitmap.createBitmap(CARD_DIM, CARD_DIM, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(imageBitmap);
        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
        Paint white = new Paint();
        white.setColor(Color.WHITE);

        paint.setAntiAlias(true);

        // draw main picture
        canvas.drawBitmap(bitmap, 0, 0, paint);


        if(IsPortrait)
            lastX = imageWidth+padding;
        else
            lastY = imageHeight+padding;

        // draw vertical/horizontal padding line
        if(IsPortrait)
            canvas.drawRect(imageWidth, 0, lastX, imageHeight, white);
        else
            canvas.drawRect(0, imageHeight, imageWidth, lastY, white);


        Paint colorBrush = new Paint();
        Paint eyeColor = new Paint();
        Paint watermarkColor = new Paint();

        eyeColor.setAntiAlias(true);
        eyeColor.setTypeface(Typeface.create("sans-serif-light", Typeface.NORMAL));
        eyeColor.setTextSize(26);


        // draw color rects
        for (int i = 0; i < 5; i++){
            int colorValue = colors.get(i);
            colorBrush.setColor(colorValue);
            eyeColor.setColor(GetRightColor(colorValue));
            if(i!=0) {
                if(IsPortrait) {
                    canvas.drawRect(lastX, lastY, lastX + colorWidth, lastY + padding, white);
                    lastY += padding;
                }else{
                    canvas.drawRect(lastX, lastY, lastX + padding, lastY + colorHeight, white);
                    lastX += padding;
                }
            }else{
                watermarkColor.setColor(eyeColor.getColor());
            }

            canvas.drawRect(lastX, lastY, lastX + colorWidth, lastY+colorHeight, colorBrush);

            if(SHOW_VALUES){
                if(IsPortrait)
                    canvas.drawText(String.format("#%06X", 0xFFFFFF & colorValue), lastX + colorWidth/10, lastY + colorHeight - colorHeight/5, eyeColor);
                else
                    canvas.drawText(String.format("#%06X", 0xFFFFFF & colorValue), lastX + colorWidth/5, lastY + colorHeight - colorHeight/10, eyeColor);
            }

            if(IsPortrait)
                lastY += colorHeight;
            else
                lastX += colorWidth;


        }

        // draw watermark
        if(IsPortrait)
            canvas.drawBitmap(watermark, CARD_DIM - watermarkWidth, 0, watermarkColor);
        else
            canvas.drawBitmap(watermark, CARD_DIM - watermarkWidth, imageHeight - watermarkHeight, watermarkColor);
        try {

            File file = File.createTempFile("crop", ".png", this.getExternalCacheDir());
            FileOutputStream outputStream = new FileOutputStream(file);
            imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);


            Logger.LogDebug("TAG", "Created Temp File");
            return Uri.fromFile(file);


        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;

    }


    private float CheckBounds(float c, float lBound, float hBound){

        if (c < lBound){
            c = lBound;
        }
        else if(c > hBound){
            c = hBound;
        }

        return c;
    }


    private void GetColor(View trackBall){
        int cX = (int)trackBall.getX()+trackBall.getWidth()/2;
        int cY = (int)trackBall.getY()+trackBall.getHeight()/2;


        Bitmap bitmap = ((BitmapDrawable)mainImage.getDrawable()).getBitmap();

        int pixel = bitmap.getPixel(cX,cY);

        GradientDrawable bgShape = (GradientDrawable)trackBall.getBackground();
        bgShape.setColor(pixel);

        colors.set(currentHolder.getAdapterPosition(), pixel);
        currentHolder.colorButton.setBackgroundColor(pixel);
        currentHolder.colorButton.setText(String.format("#%06X", 0xFFFFFF & pixel));
        currentHolder.colorButton.setTextColor(pixel);



//        trackBall.setBackgroundColor(pixel);
    }

    public static int GetRightColor(int c){
        int d = 0;

        double a = 1 - ( 0.299 * Color.red(c) + 0.587 * Color.green(c) + 0.114 * Color.blue(c))/255;

        if (a < 0.5)
            d = 0; // bright colors - black font
        else
            d = 255; // dark colors - white font

        return Color.argb(255, d, d, d);
    }


    public void GenerateButton(View v){
        GenerateColors();
    }


}
