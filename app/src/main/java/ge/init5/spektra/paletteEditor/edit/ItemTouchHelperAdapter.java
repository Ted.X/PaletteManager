package ge.init5.spektra.paletteEditor.edit;

/**
 * Created by __ted__ on 2/23/17.
 */
public interface ItemTouchHelperAdapter {

    boolean onItemMove(int fromPosition, int toPosition);
}