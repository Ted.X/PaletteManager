package ge.init5.spektra.paletteEditor.edit;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ge.init5.spektra.utils.Logger;
import ge.init5.spektra.utils.colorUtils.DominantColors;

/**
 * Created by __ted__ on 2/10/17.
 */

public class KmeanAsync extends AsyncTask<Bitmap, Void, SparseArray<List<Point>>> {

    RecyclerView recyclerView;
    List<ImageView>  trackballs;

    ProgressDialog mProgress;
    Context context;

    long time = 0;


    public KmeanAsync(Context context, RecyclerView recyclerView, List<ImageView> trackballs){
        this.recyclerView = recyclerView;
        this.trackballs = trackballs;
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        time = System.currentTimeMillis();
        mProgress = new ProgressDialog(context);
        mProgress.setMessage("Generating Colors...");
        mProgress.setCancelable(false);
        mProgress.show();
    }

    @Override
    protected void onPostExecute(SparseArray<List<Point>> map) {
        super.onPostExecute(map);



        List<Integer> colors = ((EditActivity)context).colors;

        for(int i = 0; i < map.size(); i++) {
            int key = map.keyAt(i);
            // get the object by the key.
            List<Point> points = map.get(key);

            colors.set(i, key);
            Point middle = points.get(points.size()/2);
            SetColor(trackballs.get(i), middle);
        }

        recyclerView.getAdapter().notifyDataSetChanged();


        if (mProgress.getWindow() != null) {
            mProgress.dismiss();
        }
    }

    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected SparseArray<List<Point>> doInBackground(Bitmap... params) {

        Bitmap bitmap = params[0];

        int[] cc = DominantColors.getColorsJNI(bitmap, 5);

        Logger.LogDebug("KMEANTIME: ", ""+ (System.currentTimeMillis() - time));
        for (int c: cc){
            Logger.LogDebug("KMEAN: ", c+"");
        }

        SparseArray<List<Point>> map = new SparseArray<>();

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int[] allColors = new int[width*height];

        bitmap.getPixels(allColors, 0, width, 0, 0, width, height);


        for (int ind = 0; ind < 5; ind++) {

            int color = cc[ind];
            map.put(color, new ArrayList<Point>());

            double minDiff = 1;

            while (true) {

                List<Point> pointList = map.get(color);

                int j = 0;
                for (int i = 0; i < allColors.length; i++) {
                    int col = allColors[i];

                    if (DominantColors.calculateDistance(col, color) < minDiff) {
                        pointList.add(new Point(i % width, j, col));
                    }
                    if(i % width == 0) j++;

                }
                if(pointList.size() == 0) {
                    minDiff *= 2;
                    Logger.LogDebug("KMEANLoop:" , ""+ minDiff);
                }
                else break;

            }
        }



        return map;
    }

    private void SetColor(ImageView trackBall, Point point){

        trackBall.setX(point.x-trackBall.getWidth()/2);
        trackBall.setY(point.y-trackBall.getHeight()/2);


        GradientDrawable bgShape = (GradientDrawable)trackBall.getBackground();
        bgShape.setColor(point.color);

    }
}