package ge.init5.spektra.paletteEditor.edit;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import ge.init5.spektra.paletteEditor.crop.CropActivity;
import ge.init5.spektra.paletteEditor.edit.ColorSwapAdapter;

/**
 * Created by __ted__ on 2/23/17.
 */

public class SimpleItemTouchHelperCallback extends ItemTouchHelper.Callback {

    private final ColorSwapAdapter mAdapter;

    public SimpleItemTouchHelperCallback(ColorSwapAdapter adapter) {
        mAdapter = adapter;
    }

    @Override
    public boolean isLongPressDragEnabled() {
        return false;
    }

    @Override
    public boolean isItemViewSwipeEnabled() {
        return false;
    }

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        int dragFlags;
        if(CropActivity.RATIO_WIDTH > CropActivity.RATIO_HEIGHT)
            dragFlags = ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;
        else
            dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
        int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
        return makeMovementFlags(dragFlags, swipeFlags);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                          RecyclerView.ViewHolder target) {
        mAdapter.onItemMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {}

}
