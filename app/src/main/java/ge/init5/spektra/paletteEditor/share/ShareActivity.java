package ge.init5.spektra.paletteEditor.share;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ge.init5.spektra.R;
import ge.init5.spektra.paletteCreator.AddPaletteActivity;
import ge.init5.spektra.paletteEditor.edit.EditActivity;
import ge.init5.spektra.utils.Logger;

public class ShareActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int CREATE_CARD_PALETTE = 99;
    private static final String TAG = ShareActivity.class.getSimpleName();

    @BindView(R.id.share_button)
    Button shareButton;

    @BindView(R.id.download_button)
    Button downloadButton;

    @BindView(R.id.save_button)
    Button createPalette;

    @BindView(R.id.start_over_button)
    Button startOverButton;

    @BindView(R.id.exported_image)
    ImageView exportedImage;

    @BindView(R.id.appbar_layout)
    AppBarLayout appBarLayout;
//
//    private Uri exportedUri;
//
//    private int[] exportedColors;

    private Intent exportedIntent;

    private Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        shareButton.setOnClickListener(this);
        shareButton.setCompoundDrawables(null, new IconicsDrawable(this)
                .icon(GoogleMaterial.Icon.gmd_share)
                .color(ContextCompat.getColor(this, R.color.cardview_light_background))
                .sizeDp(30), null, null);

        downloadButton.setOnClickListener(this);
        downloadButton.setCompoundDrawables(null, new IconicsDrawable(this)
                .icon(GoogleMaterial.Icon.gmd_save)
                .color(ContextCompat.getColor(this, R.color.cardview_light_background))
                .sizeDp(30), null, null);


        if (Build.VERSION.SDK_INT < 21){
            shareButton.setBackgroundColor(getResources().getColor(R.color.md_indigo_A200));
            downloadButton.setBackgroundColor(getResources().getColor(R.color.accent));
        }

        createPalette.setOnClickListener(this);
//        createPalette.setCompoundDrawables(null, new IconicsDrawable(this)
//                .icon(GoogleMaterial.Icon.gmd_desktop_mac)
//                .color(ContextCompat.getColor(this, R.color.cardview_light_background))
//                .sizeDp(30), null, null);

        startOverButton.setOnClickListener(this);

        Intent i = getIntent();
        imageUri = Uri.parse(i.getStringExtra("image_uri"));

        int[] colors = i.getIntArrayExtra("colors");

        if (imageUri != null && colors != null) {

//            exportedUri = uri;
//            exportedColors = colors;
            exportedIntent = i;

            exportedImage.setImageURI(imageUri);

            exportedImage.post(new Runnable() {
                @Override
                public void run() {
                    CoordinatorLayout.LayoutParams paramss = (CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();
                    paramss.height = appBarLayout.getWidth();

                    appBarLayout.setLayoutParams(paramss);
                }
            });

        }
    }

    @Override
    public void onOptionsMenuClosed(Menu menu) {
        super.onOptionsMenuClosed(menu);
        MenuItem main_item2 = menu.add(0, 0, 0, "Start Over");
        main_item2.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case android.R.id.home:
                new File(imageUri.getPath()).delete();
                onBackPressed();
                return true;
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){
            if(requestCode == CREATE_CARD_PALETTE){
                Logger.LogDebug(TAG, "start Over");
                StartOver();
            }
        }
    }

    public static Intent SharingToSocialMedia(Uri imageUri) {


        final Intent share = new Intent(Intent.ACTION_SEND);
        share.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        share.putExtra(Intent.EXTRA_STREAM, imageUri);
        share.setType("image/png");

//        startActivity(Intent.createChooser(share, "Title of the dialog the system will open"));
        return share;
    }

    public void SharingPalette(){
        Logger.LogDebug(TAG, "Sharing");

        Bitmap bitmap = ((BitmapDrawable) exportedImage.getDrawable()).getBitmap();
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, EditActivity.CARD_DIM/2, EditActivity.CARD_DIM/2, false);

        File file = null;
        try {
            file = File.createTempFile("scaled", ".png", this.getExternalCacheDir());
            FileOutputStream outputStream = new FileOutputStream(file);

            scaledBitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);

            Uri scaledUri =  Uri.fromFile(file);
            exportedIntent.putExtra("image_scaled_uri", scaledUri.toString());

            setResult(RESULT_OK, exportedIntent);

        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            finish();
        }

    }

    public static void ShareExcludingApp(Context context, Uri imageUri) {

        List<Intent> targetedShareIntents = new ArrayList<Intent>();
//        Intent share = new Intent(android.content.Intent.ACTION_SEND);
//        share.setType("image/*");
        List<ResolveInfo> resInfo = context.getPackageManager().queryIntentActivities(SharingToSocialMedia(imageUri), 0);
        if (!resInfo.isEmpty()) {
            for (ResolveInfo info : resInfo) {
                Intent targetedShare = SharingToSocialMedia(imageUri);

                if (!info.activityInfo.packageName.contains("spektra") &&
                        !info.activityInfo.packageName.contains("paletter")) {
                    targetedShare.setPackage(info.activityInfo.packageName);
                    targetedShareIntents.add(targetedShare);
                }
            }

            Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(0),
                    "Select app to share");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
                    targetedShareIntents.toArray(new Parcelable[]{}));
            context.startActivity(chooserIntent);
        }

    }

    private void StartOver() {
        new File(imageUri.getPath()).delete();
        setResult(RESULT_CANCELED);
        finish();
    }

    public static File bitmapConvertToFile(Context context, Uri imageUri) {

        File file = new File(Environment.getExternalStoragePublicDirectory("Paletter"), "");

        if (!file.exists()) {
            file.mkdir();
        }

        File bitmapFile = new File(imageUri.getPath());

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inMutable = true;
        Bitmap imageBitmap = BitmapFactory.decodeFile(bitmapFile.getPath(), options);
//        Bitmap roundCorners = Bitmap.createScaledBitmap(((BitmapDrawable) getResources().getDrawable(R.drawable.round)).getBitmap(),
//                EditActivity.CARD_DIM, EditActivity.CARD_DIM, true);
//
//        Canvas canvas = new Canvas(imageBitmap);
//
//        Paint eraser = new Paint();
//        eraser.setAntiAlias(true);
//        eraser.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
//        canvas.drawBitmap(roundCorners, 0, 0, eraser);


        File downloadedFIle = new File(file, "IMG_" + (new SimpleDateFormat("yyyyMMddHHmmss")).format(Calendar.getInstance().getTime()) + ".jpg");
        try {

            FileOutputStream outputStream = new FileOutputStream(downloadedFIle);
            imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);

            Toast.makeText(context, "Image Saved", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
        }

        MediaScannerConnection.scanFile(context, new String[]{downloadedFIle.getAbsolutePath()}, null, new MediaScannerConnection.MediaScannerConnectionClient() {
            @Override
            public void onMediaScannerConnected() {
            }

            @Override
            public void onScanCompleted(final String path, Uri uri) {
            }
        });

        return bitmapFile;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.share_button:
                ShareExcludingApp(this, imageUri);
                break;
            case R.id.download_button:
                bitmapConvertToFile(this, imageUri);
                break;
            case R.id.save_button:
                // TODO: move this to teh submit palette
//                exportedImage.setDrawingCacheEnabled(true);
//                exportedImage.buildDrawingCache();
//                ImageStorage.getInstance().UploadImage("shigaru", exportedImage.getDrawingCache());
                SharingPalette();
//                Toast.makeText(this, "Calm your pussy down", Toast.LENGTH_LONG).show();
                break;
            case R.id.start_over_button:
                StartOver();
                break;
        }
    }
}
