package ge.init5.spektra.palettes.cardPalette;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ge.init5.spektra.MainActivity;
import ge.init5.spektra.R;
import ge.init5.spektra.models.FirePalette;
import ge.init5.spektra.services.FirebasePaletteDatabase.ImageStorage;
import ge.init5.spektra.services.FirebasePaletteDatabase.PaletteBase;
import ge.init5.spektra.utils.PaletteComparator;
import ge.init5.spektra.utils.Logger;
import mabbas007.tagsedittext.TagsEditText;

/**
 * Created by __ted__ on 2/16/17.
 */
public class CardPaletteViewHolder extends RecyclerView.ViewHolder {

    private static final String TAG = CardPaletteViewHolder.class.getSimpleName();
    @BindView(R.id.card_image)
    public SquareImageView imageView;

    @BindView(R.id.heart_image)
    public ImageButton heartImageView;

    @BindView(R.id.heart_count)
    public TextView heartCountView;

    @BindView(R.id.download_button_view)
    public ImageButton downloadButton;

    @BindView(R.id.share_button_view)
    public ImageButton shareButton;

    @BindView(R.id.report_button_view)
    public ImageButton reportButton;

    @BindView(R.id.palette_button_view)
    public ImageButton paletteButton;

    @BindView(R.id.close_button_view)
    public ImageButton closeButton;

    @BindView(R.id.card)
    CardView cardView;

    @BindView(R.id.option_panel)
    FrameLayout optionPanel;

    @BindView(R.id.tagsText)
    TextView tagsTextView;

    @BindView(R.id.tags)
    TagsEditText tagsView;

    @BindView(R.id.palette_name)
    TextView paletteName;

    @BindView(R.id.author)
    TextView author;

    private String paletteKey;
    private String paletteOwnerUID;

    public boolean isMine = false;


    Context context;


    public CardPaletteViewHolder(Context context, View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

        this.context = context;

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                optionPanel.setVisibility(View.VISIBLE);
            }
        });
        heartImageView.setOnClickListener(HeartListener());
        downloadButton.setOnClickListener(DownloadPalette());
        shareButton.setOnClickListener(SharePalette());
        reportButton.setOnClickListener(ReportPalette());
        paletteButton.setOnClickListener(ViewPalette());

        optionPanel.setOnClickListener(ClosePanel());
        closeButton.setOnClickListener(ClosePanel());

    }

    public void setCard(final FirePalette palette, final String paletteKey){
//        if (!imageDownloaded){
//        String p = ImageStorage.getInstance().GetCardsStorage(paletteKey).getPath();
//        Logger.LogDebug("CardPalette", "Download Request " + p );
//        }



        this.paletteKey = paletteKey;
        this.paletteOwnerUID = palette.uid;

        Uri uri = ((MainActivity)context).getUriFromMemCache(paletteKey);
        if (uri == null) {
            ImageStorage.getInstance().DownloadFeedImage(context, paletteKey, imageView);
        } else {
            Glide.with(context)
                    .load(uri)
                    .into(imageView);
//            imageView.setImageBitmap(bitmap);
//            Picasso.with(adapter).load(uri).placeholder(R.drawable.rectpic).into(imageView);
//            ImageStorage.getInstance().DownloadImage(adapter, paletteKey, this);
        }

        tagsView.setEnabled(false);
        tagsView.setKeyListener(null);
        tagsView.setFocusable(false);


        if(palette.tags != null) {
            String[] tags = palette.tags.toArray(new String[palette.tags.size()]);
            tagsView.setTags(tags);
        }else{
            tagsTextView.setVisibility(View.GONE);
            tagsView.setTags(new String[0]);
        }


//        Picasso.with(adapter)
//                .load(p)
//                .placeholder(R.drawable.rectpic).into(imageView);
        paletteName.setText(palette.title);
        author.setText("by " + palette.author);
        heartCountView.setText(palette.heartCount + " Likes");
    }


    private View.OnClickListener ViewPalette() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> filterList = new ArrayList<String>();
                filterList.add(paletteKey);
                MainActivity.drawerSelectedIndex = isMine ? 1 : 2;
                ((MainActivity) context).changeMainToDrawerIndexedView(PaletteComparator.NO_SORT, filterList);
                ClosePanel();
            }
        };
    }


    private View.OnClickListener HeartListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Animation heart = AnimationUtils.loadAnimation(v.getContext(), R.anim.heart_animation);

                heart.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        Logger.FireLog(TAG, "HeartAnimation started.");
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        Logger.FireLog(TAG, "HeartAnimation ended.");

                        heartImageView.setAlpha(0f);
                        PaletteBase.getInstance().updateHearts(paletteOwnerUID, paletteKey);

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }

                });

                heartImageView.startAnimation(heart);
            }
        };
    }

    private View.OnClickListener DownloadPalette() {
        return new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

//                ShareActivity.bitmapConvertToFile(adapter, adapter.getUriFromMemCache(paletteKey));

                ImageStorage.getInstance().DownloadOriginalImage(context, paletteKey, false);
                if (MainActivity.mInterstitialAd.isLoaded()){
                    MainActivity.mInterstitialAd.show();
                }
                closeButton.performClick();
            }
        };
    }

    private View.OnClickListener ReportPalette(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Uri path = Uri.fromFile(file);
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto","studioinit5@gmail.com", null));
//                emailIntent.setType("vnd.android.cursor.dir/email");

                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Report Card Palette");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "Card Palette " + paletteName.getText() + " " + author.getText());
                context.startActivity(Intent.createChooser(emailIntent, "Send email..."));

            }
        };
    }


    private View.OnClickListener SharePalette() {
        return new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                ImageStorage.getInstance().DownloadOriginalImage(context, paletteKey, true);
                closeButton.performClick();
            }
        };
    }

    private View.OnClickListener ClosePanel(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                optionPanel.setVisibility(View.GONE);
            }
        };
    }
}
