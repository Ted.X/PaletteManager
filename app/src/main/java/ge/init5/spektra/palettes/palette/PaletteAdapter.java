package ge.init5.spektra.palettes.palette;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

import ge.init5.spektra.MainActivity;
import ge.init5.spektra.R;
import ge.init5.spektra.utils.Tutorial;
import ge.init5.spektra.utils.colorUtils.ColorConverter;

/**
 * Created by __ted__ on 11/12/16.
 */

public class PaletteAdapter extends ExpandableRecyclerViewAdapter<PaletteHolder, PaletteInfoHolder> {

    private static boolean TUTORIAL = true;
    // PUBLIC
    public Context context;


    // PRIVATE
    private int startPos = -1;
    private static int INFO_LAYOUT = 0;
    private static int VALUES_LAYOUT = 1;
    private PaletteInfoHolder currentInfoHolder;
    private LinearLayoutManager layoutManager;

    // PACKAGE LOCAL
//    int countParentHolder = 0;
    PaletteHolder currentHolder;
    int lastGroupIndex = -1;
    PaletteHolder lastHolder;


    public PaletteAdapter(Context context, RecyclerView recyclerView, List<? extends ExpandableGroup> groups) {
        super(groups);
        this.context = context;
        this.layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());
    }


    @Override
    public void onGroupExpanded(final int positionStart, int itemCount) {
        super.onGroupExpanded(positionStart, itemCount);

        layoutManager.scrollToPositionWithOffset(positionStart-2, getItemCount());

        currentHolder.activeColor = -1;
//        currentHolder.selectedColor.setVisibility(View.VISIBLE);
//        currentHolder.selectedColor.setAnimation(AnimationUtils.loadAnimation(context, android.R.anim.fade_in));

        currentHolder.selectedColor.setBackgroundColor(Color.WHITE);
        currentHolder.revealLayout.setBackgroundColor(Color.WHITE);

        currentHolder.setIsRecyclable(false);

        int i = 0;
        final RecyclerViewPalette pal = (RecyclerViewPalette) PaletteAdapter.this.getGroups().get(currentHolder.index);
        final PaletteInfo info = pal.getItems().get(0);
        for (PaletteImageView im : currentHolder.mImageViews) {
            final int finalI = i;

            im.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        currentHolder.activeColor = finalI;

                        if (currentInfoHolder.flipper != null)
                            if(currentInfoHolder.flipper.getDisplayedChild() == INFO_LAYOUT)
                                currentInfoHolder.flipper.setDisplayedChild(VALUES_LAYOUT);

                        PaletteColor color = pal.getColors()[finalI];

                        info.setName(finalI, ColorConverter.RGBtoColorName(color.getRGB()));

                        info.setRgbValue(finalI, ColorConverter.RGBtoString(color.getRGB()));

                        info.setHsvValue(finalI, ColorConverter.HSVtoString(color.getHSV()));

                        info.setHexValue(finalI, ColorConverter.HEXtoString(color.getHEX()));

                        currentInfoHolder.setInfo(info, finalI, pal);

                        currentHolder.showSelectedColorView(finalI, color.getInt());

                    }
            });
            i++;
        }

        if (startPos != -1 && positionStart != startPos) {
            onGroupClick(expandableList.getFlattenedGroupIndex(getGroups().get(lastGroupIndex)));
        }
        startPos = positionStart;

        Tutorial.myPaletteCardTutorial(context, currentHolder);

    }

    @Override
    public void onGroupCollapsed(int positionStart, int itemCount) {
        super.onGroupCollapsed(positionStart, itemCount);
        for (ImageView im : lastHolder.mImageViews) {
            im.setOnClickListener(null);
            im.setClickable(false);
//            im.setLayoutParams(paramsColapse);
        }

        if (Build.VERSION.SDK_INT < 21) {
            lastHolder.selectedColor.setVisibility(View.GONE);
            lastHolder.setIsRecyclable(true);
            lastHolder.cardView.setCardElevation(2);
            startPos = -1;
        }else {

            Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_up);
            currentInfoHolder.itemView.startAnimation(animation);

            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {

                    lastHolder.selectedColor.setVisibility(View.GONE);
                    lastHolder.setIsRecyclable(true);
                    lastHolder.cardView.setCardElevation(2);
                    startPos = -1;
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
        }



    }

    @Override
    public boolean onGroupClick(int flatPos) {
        boolean c = super.onGroupClick(flatPos);
        return c;
    }


    @Override
    public PaletteHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_palette_view, parent, false);

        PaletteHolder vh = new PaletteHolder(context, this, v);
//        countParentHolder++;
        return vh;
    }

    @Override
    public PaletteInfoHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.expandable_recycler_child_view, parent, false);
        PaletteInfoHolder vh = new PaletteInfoHolder(context, v);

        return vh;
    }

    @Override
    public void onBindChildViewHolder(PaletteInfoHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        RecyclerViewPalette pal = (RecyclerViewPalette) group;
        PaletteInfo info = pal.getItems().get(childIndex);

        currentInfoHolder = holder;
        holder.flipper.setDisplayedChild(INFO_LAYOUT);
        holder.setInfo(info, currentHolder.activeColor, pal);

        if (Build.VERSION.SDK_INT >= 21) {
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_from_top);
            holder.itemView.startAnimation(animation);
        }
    }


    @Override
    public void onBindGroupViewHolder(PaletteHolder holder, int flatPosition, ExpandableGroup group) {
        holder.index = getGroups().indexOf(group);
        holder.setPalette((RecyclerViewPalette)group);
//        Toast.makeText(context, flatPosition+"", Toast.LENGTH_SHORT).show();
    }

}
