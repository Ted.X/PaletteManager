package ge.init5.spektra.palettes.palette;

import android.os.Parcel;
import android.os.Parcelable;

import ge.init5.spektra.utils.Logger;
import ge.init5.spektra.utils.colorUtils.ColorConverter;

/**
 * Created by __ted__ on 12/4/16.
 */

public class PaletteColor implements Parcelable{

    public static final String TAG = PaletteColor.class.getSimpleName();

    public int value = 0;
    private transient short[] rgb = new short[3];
    private transient float[] hsv = new float[3];
    private transient String hex = "000000";

    public PaletteColor(){ }

    /**
     * Construct FirePalette Color with int value and title
     * @param value int Color value
     */
    public PaletteColor(int value) {
        Logger.FireLog(TAG, "Palette color created with int value: " + value);
        // Set int
        setInt(value);
        // Set RGB
        setRGB(ColorConverter.InttoRGB(value));
        // Set HSV
        setHSV(ColorConverter.IntToHSV(value));
        // Set HEX
        setHEX(ColorConverter.IntToHEX(value));
    }

    /**
     * Create color with red green and blue
     * @param r [0-255] red
     * @param g [0-255] green
     * @param b [0-255] blue
     */
    public PaletteColor(short r, short g, short b){
        Logger.FireLog(TAG, "Palette color created with rgb(" + r + ", " + g + ", " + b + ")");
        // Set RGB at first
        setRGB(r, g, b);
        // Set int
        setInt(ColorConverter.RGBtoInt(r, g, b));
        // Set HSV
        setHSV(ColorConverter.RGBtoHSV(r, g, b));
        // Set HEX
        setHEX(ColorConverter.RGBtoHEX(r, g, b));
    }

    /**
     * Create color with red green and blue
     * @param rgb short[] { red, green, blue }
     */
    public PaletteColor(short[] rgb){
        Logger.FireLog(TAG, "Palette color created with rgb(" + rgb[0] + ", " + rgb[1] + ", " + rgb[2] + ")");
        // Set RGB at first
        setRGB(rgb[0], rgb[1], rgb[2]);
        // Set int
        setInt(ColorConverter.RGBtoInt(rgb[0], rgb[1], rgb[2]));
        // Set HSV
        setHSV(ColorConverter.RGBtoHSV(rgb[0], rgb[1], rgb[2]));
        // Set HEX
        setHEX(ColorConverter.RGBtoHEX(rgb[0], rgb[1], rgb[2]));
    }

    /**
     * Create color with hue saturation and value
     * @param h hue
     * @param s saturation
     * @param v value
     */
    public PaletteColor(float h, float s, float v){
        Logger.FireLog(TAG, "Palette color created with hsv(" + h + ", " + s + ", " + v + ")");
        // Set HSV at first
        setHSV(h, s, v);
        // Set int
        setInt(ColorConverter.HSVtoInt(h, s, v));
        // Set RGB
        setRGB(ColorConverter.HSVtoRGB(h, s, v));
        // Set HEX
        setHEX(ColorConverter.HSVtoHEX(h, s, v));
    }

    /**
     * Create color with hue saturation and value
     * @param hsv float[] { hue, saturation, value }
     */
    public PaletteColor(float[] hsv){
        Logger.FireLog(TAG, "Palette color created with hsv(" + hsv[0] + ", " + hsv[1] + ", " + hsv[1] + ")");
        // Set HSV at first
        setHSV(hsv[0], hsv[1], hsv[2]);
        // Set int
        setInt(ColorConverter.HSVtoInt(hsv[0], hsv[1], hsv[2]));
        // Set RGB
        setRGB(ColorConverter.HSVtoRGB(hsv[0], hsv[1], hsv[2]));
        // Set HEX
        setHEX(ColorConverter.HSVtoHEX(hsv[0], hsv[1], hsv[2]));
    }


    /**
     * Create color with hue saturation and value
     * @param hex String "FFFFFF" without #
     */
    public PaletteColor(String hex){
        Logger.FireLog(TAG, "Palette color created with hex: " + hex);
        // Set RGB at first
        setHEX(hex);
        // Set int
        setInt(ColorConverter.HEXtoInt(hex));
        // Set HSV
        setRGB(ColorConverter.HEXtoRGB(hex));
        // Set HEX
        setHSV(ColorConverter.HEXtoHSV(hex));
    }


    /**
     * Returns a string representation of the object. In general, the
     * {@code toString} method returns a string that
     * "textually represents" this object.
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        return getRGBString() + "\n"+ getHSVString() + "\n" + getHEXString()+"\n";
    }


    public String getRGBString(){
        return "RGB: (" + rgb[0] + ", " + rgb[1] + ", " + rgb[2] + ")";
    }

    public String getHSVString(){
        return "HSV: (" + hsv[0] + ", " + hsv[1] + ", " + hsv[2] + ")";
    }

    public String getHEXString(){
        return "Hex: #" + hex;
    }

    protected PaletteColor(Parcel in) {
        value = in.readInt();
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(value);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PaletteColor> CREATOR = new Creator<PaletteColor>() {
        @Override
        public PaletteColor createFromParcel(Parcel in) {
            return new PaletteColor(in);
        }

        @Override
        public PaletteColor[] newArray(int size) {
            return new PaletteColor[size];
        }
    };

    // Setters and Getters
    // -------- INT --------
    public int getInt() { return value; }
    public void setInt(int v) { value = v; }

    // -------- RGB --------
    public short getR() { return rgb[0]; }
    public short getG() { return rgb[1]; }
    public short getB() { return rgb[2]; }
    public short[] getRGB() { return rgb; }

    public void setR(short r) { rgb[0] = r; }
    public void setG(short g) { rgb[1] = g; }
    public void setB(short b) { rgb[2] = b; }
    public void setRGB(short r, short g, short b) { setR(r); setG(g); setB(b); }
    public void setRGB(short[] newRGB) { setR(newRGB[0]); setG(newRGB[1]); setB(newRGB[2]); }

    // -------- HSV --------
    public float getH() { return hsv[0]; }
    public float getS() { return hsv[1]; }
    public float getV() { return hsv[2]; }
    public float[] getHSV() { return hsv; }

    public void setH(float h) { hsv[0] = h; }
    public void setS(float s) { hsv[1] = s; }
    public void setV(float v) { hsv[2] = v; }
    public void setHSV(float h, float s, float v) { setH(h); setS(s); setV(v); }
    public void setHSV(float[] hsv) { setH(hsv[0]); setS(hsv[1]); setV(hsv[2]); }

    // -------- HEX --------
    public String getRhex() { return Integer.toHexString(getR()); }
    public String getGhex() { return Integer.toHexString(getG()); }
    public String getBhex() { return Integer.toHexString(getB()); }
    public String getHEX() { return hex; }

    public void setHEX(String hex) { this.hex = hex; }


}












