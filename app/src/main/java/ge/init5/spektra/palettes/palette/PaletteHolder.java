package ge.init5.spektra.palettes.palette;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import ge.init5.spektra.MainActivity;
import ge.init5.spektra.R;
import ge.init5.spektra.paletteCreator.AddPaletteActivity;
import ge.init5.spektra.services.FirebasePaletteDatabase.PaletteBase;
import ge.init5.spektra.utils.Logger;
import ge.init5.spektra.utils.Tutorial;
import ge.init5.spektra.utils.colorUtils.ColorConverter;
import io.codetail.animation.SupportAnimator;
import io.codetail.animation.ViewAnimationUtils;
import io.codetail.widget.RevealFrameLayout;


/**
 * Created by __ted__ on 1/7/17.
 */

public class PaletteHolder extends GroupViewHolder {
    public static final String TAG = PaletteHolder.class.getSimpleName();
    private static boolean TUTORIAL = true;

    private Context context;

    private PaletteAdapter adapter;

    private RecyclerViewPalette holderPalette;

    private SupportAnimator revealAnimation;


    @BindView(R.id.card_view)
    CardView cardView;

    @BindView(R.id.palette_name)
    TextView mTextView;

    @BindViews({R.id.palette_color_1, R.id.palette_color_2, R.id.palette_color_3, R.id.palette_color_4, R.id.palette_color_5})
    public List<PaletteImageView> mImageViews;

    @BindView(R.id.palette_share)
    public ImageButton shareButton;

    @BindView(R.id.palette_copy)
    public ImageButton copyButton;

    @BindView(R.id.palette_edit)
    public ImageButton editButton;

    @BindView(R.id.palette_delete)
    public ImageButton deleteButton;

    @BindView(R.id.reveal_frame_layout)
    RevealFrameLayout revealLayout;

    @BindView(R.id.selected_color)
    ImageView selectedColor;

    int index;

    int activeColor;

    boolean canClick = true;

    public PaletteHolder(final Context context, PaletteAdapter adapter, View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.context = context;
        this.adapter = adapter;

        shareButton.setOnClickListener(SharePaletteInfo());
        copyButton.setOnClickListener(CopyPaletteInfo(copyButton));
        editButton.setOnClickListener(EditPalette());
        deleteButton.setOnClickListener(DeletePalette());

        setIcon(shareButton, GoogleMaterial.Icon.gmd_share);
        setIcon(copyButton, GoogleMaterial.Icon.gmd_content_copy);
        setIcon(editButton, GoogleMaterial.Icon.gmd_edit);
        setIcon(deleteButton, GoogleMaterial.Icon.gmd_delete);

        mTextView.setTextColor(Color.rgb(75, 75, 75));
    }


    private void setIcon(ImageButton btn, GoogleMaterial.Icon icon){
        btn.setImageDrawable(new IconicsDrawable(context)
                .icon(icon)
                .color(Color.rgb(117, 117, 117))
                .sizeDp(24));
    }

    @Override
    public void onClick(View v) {
        adapter.currentHolder = this;
        super.onClick(v);
        adapter.lastGroupIndex = index;
        adapter.lastHolder = this;
    }

    public void setPalette(final RecyclerViewPalette pal){
        holderPalette = pal;
        mTextView.setText(holderPalette.getTitle());
//        selectedColor.setVisibility(View.GONE);

        for (int i = 0; i < mImageViews.size(); i++) {
            PaletteImageView m = mImageViews.get(i);
//            m.setOnClickListener(null);
//            m.setClickable(false);
            m.color = holderPalette.getColors()[i].getInt();
            m.palName = holderPalette.getTitle();
            if(m.getWidth() > 0)
                m.requestLayout();
        }


    }


    public void showSelectedColorView(int index, final int color){

        int finalRadius = Math.max(selectedColor.getWidth(), selectedColor.getHeight());
        int colorWidth = selectedColor.getWidth()/5;

        if(revealAnimation != null)
            revealAnimation.cancel();

        revealAnimation = ViewAnimationUtils.createCircularReveal(selectedColor, index * colorWidth + colorWidth/2, 0, 0, finalRadius);
        selectedColor.setBackgroundColor(color);
//        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        short[] a = ColorConverter.InttoRGB(color);
        Logger.LogDebug("SELECTED COLOR", a[0] + " " + a[1] + " " + a[2]);
        revealAnimation.setDuration(500);


        revealAnimation.addListener(new SupportAnimator.AnimatorListener() {
            @Override
            public void onAnimationStart() {

            }

            @Override
            public void onAnimationEnd() {
                revealLayout.setBackgroundColor(color);
            }

            @Override
            public void onAnimationCancel() {
                revealLayout.setBackgroundColor(color);
            }

            @Override
            public void onAnimationRepeat() {

            }
        });

        revealAnimation.start();

    }

    private View.OnClickListener SharePaletteInfo() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");

                // Write dem body boiiii
                String shareBody = holderPalette.toString();
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject here");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                context.startActivity(Intent.createChooser(sharingIntent, "Choose Sharing Option"));
            }
        };
    }

    private View.OnClickListener CopyPaletteInfo(final ImageButton button) {
        return new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                PopupMenu popupMenu = new PopupMenu(context, button, 0, R.attr.popupMenuStyle, R.style.AppTheme_PopupOverlay);
                popupMenu.getMenuInflater().inflate(R.menu.copy_menu, popupMenu.getMenu());

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        String text = "";

                        String data = "";

                        switch (item.getItemId()){
                            case R.id.rgb_copy:
                                data = "RGB Values";
                                text = holderPalette.GetValuesInString(0);
                                break;
                            case R.id.hsv_copy:
                                data = "HSV Values";
                                text = holderPalette.GetValuesInString(1);
                                break;
                            case R.id.hex_copy:
                                data = "HEX Values";
                                text = holderPalette.GetValuesInString(2);
                                break;
                        }

                        ClipData clip = ClipData.newPlainText("Copied Data", text);
                        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                        clipboard.setPrimaryClip(clip);

                        Snackbar.make(view, "Copied " + data, Snackbar.LENGTH_SHORT).show();


                        return true;
                    }
                });

                popupMenu.show();
//                    new AlertDialog.Builder(context).show();
            }
        };
    }

    private View.OnClickListener EditPalette() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddPaletteActivity.class);
                intent.putExtra("palette", holderPalette);
                ((MainActivity)context).startActivityForResult(intent, MainActivity.PALETTE_CHANGED);
            }
        };
    }

    private View.OnClickListener DeletePalette(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(adapter.isGroupExpanded(index))
                    adapter.onGroupClick(index);
                new AlertDialog.Builder(context)
                        .setTitle("Delete")
                        .setMessage("Are you sure?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String key = ((RecyclerViewPalette)adapter.getGroups().get(index)).getFirebaseKey();
                                List<String> tags = ((RecyclerViewPalette)adapter.getGroups().get(index)).getTags();
                                String title = ((RecyclerViewPalette)adapter.getGroups().get(index)).getTitle();
                                adapter.getGroups().remove(index);
                                adapter.notifyItemRemoved(index);
                                adapter.notifyDataSetChanged();
                                PaletteBase.getInstance().deleteUserPalette(key, tags, title);
                                Logger.LogDebug("PaletteAdapter: ", "Key : " + key);
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("Nope", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        };
    }

    @Override
    public void expand() {
        super.expand();
        selectedColor.setVisibility(View.VISIBLE);
        cardView.setCardElevation(30);
    }

    @Override
    public void collapse() {
        super.collapse();
    }

    //
//        private void animateExpand() {
//            RotateAnimation rotate =
//                    new RotateAnimation(360, 180, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
//            rotate.setDuration(300);
//            rotate.setFillAfter(true);
//            arrow.setAnimation(rotate);
//        }
//
//        private void animateCollapse() {
//            RotateAnimation rotate =
//                    new RotateAnimation(180, 360, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
//            rotate.setDuration(300);
//            rotate.setFillAfter(true);
//            arrow.setAnimation(rotate);
//        }


}