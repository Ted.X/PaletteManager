package ge.init5.spektra.palettes.palette;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by __ted__ on 1/6/17.
 */

public class PaletteImageView extends ImageView {

    public int color;
    public String palName;
    public PaletteImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        Bitmap myBitmap = getBitmap(color, getWidth(), getHeight());
        setImageBitmap(myBitmap);
    }

    public Bitmap getBitmap(int color, int width, int height){
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();

        paint.setColor(color);
        canvas.drawRect(0, 0, width, height, paint);

        return bitmap;
    }
}