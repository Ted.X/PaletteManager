package ge.init5.spektra.palettes.palette;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by __ted__ on 12/17/16.
 */

public class PaletteInfo implements Parcelable{

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public int getHeartCount() {
        return heartCount;
    }

    public void setHeartCount(int heartCount) {
        this.heartCount = heartCount;
    }

    private String dateCreated;
    private boolean isPublic;
    private int heartCount;
    private List<String> tags;

    private String[] names;

    private String[] rgbValues;
    private String[] hsvValues;
    private String[] hexValues;

    //default
    public PaletteInfo(String dateCreated, boolean isPublic, int heartCount, List<String> tags) {
        this.rgbValues = new String[5];
        this.hsvValues = new String[5];
        this.hexValues = new String[5];
        this.names = new String[5];

        for (int i = 0; i < 5; i++) {
            this.rgbValues[i] = "-";
            this.hsvValues[i] = "-";
            this.hexValues[i] = "-";
        }

        this.dateCreated = dateCreated;
        this.isPublic = isPublic;
        this.heartCount = heartCount;
        this.tags = tags;
    }

    public PaletteInfo(Parcel parcel) {

    }

    public String getName(int index) {
        return names[index];
    }

    public void setName(int index, String name) {
        this.names[index] = name;
    }

    public String[] getRgbValues() {
        return rgbValues;
    }

    public void setRgbValues(String[] rgbValues) {
        this.rgbValues = rgbValues;
    }

    public void setRgbValue(int index, String value){
        rgbValues[index] = value;
    }

    public String[] getHsvValues() {
        return hsvValues;
    }

    public void setHsvValues(String[] hsvValues) {
        this.hsvValues = hsvValues;
    }

    public void setHsvValue(int index, String value){
        hsvValues[index] = value;
    }

    public String[] getHexValues() {
        return hexValues;
    }

    public void setHexValues(String[] hexValues) {
        this.hexValues = hexValues;
    }

    public void setHexValue(int index, String value){
        hexValues[index] = value;
    }

    public static final Creator<PaletteInfo> CREATOR = new Creator<PaletteInfo>() {
        @Override
        public PaletteInfo createFromParcel(Parcel parcel) {
            return new PaletteInfo(parcel);
        }

        @Override
        public PaletteInfo[] newArray(int i) {
            return new PaletteInfo[i];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }
}
