package ge.init5.spektra.palettes.palette;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ge.init5.spektra.MainActivity;
import ge.init5.spektra.R;
import ge.init5.spektra.services.FirebasePaletteDatabase.PaletteBase;
import ge.init5.spektra.utils.AseCreator;
import ge.init5.spektra.utils.Logger;
import ge.init5.spektra.utils.PaletteComparator;
import mabbas007.tagsedittext.TagsEditText;

/**
 * Created by __ted__ on 1/7/17.
 */

public class PaletteInfoHolder extends ChildViewHolder{

    private Context context;

    public View itemView;

    @BindView(R.id.recycler_child_view_flipper)
    public ViewFlipper flipper;

    public PaletteInfoHolder(Context context, View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.context = context;
        this.itemView = itemView;
    }

    public void setInfo(final PaletteInfo info, final int index, final RecyclerViewPalette palette){
        View currentView = flipper.getCurrentView();

        if(currentView.getId() == R.id.palette_values_layout){

            TextView colorName = (TextView) currentView.findViewById(R.id.child_view_color_name_value);

            TextView rgbValue = (TextView) currentView.findViewById(R.id.child_view_rgb_value);
            TextView hsvValue = (TextView) currentView.findViewById(R.id.child_view_hsv_value);
            TextView hexValue = (TextView) currentView.findViewById(R.id.child_view_hex_value);

            ImageButton rgbCopyButton = (ImageButton) currentView.findViewById(R.id.child_view_rgb_copy_button);
            ImageButton hsvCopyButton = (ImageButton) currentView.findViewById(R.id.child_view_hsv_copy_button);
            ImageButton hexCopyButton = (ImageButton) currentView.findViewById(R.id.child_view_hex_copy_button);

            colorName.setText(index >=0 ? info.getName(index) : "-");
            rgbValue.setText(index >= 0 ? info.getRgbValues()[index] : "-");
            hsvValue.setText(index >= 0 ? info.getHsvValues()[index] : "-");
            hexValue.setText(index >= 0 ? info.getHexValues()[index] : "-");

            Animation fadeIn = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);
            fadeIn.setDuration(300);

            rgbValue.setAnimation(fadeIn);
            hsvValue.setAnimation(fadeIn);
            hexValue.setAnimation(fadeIn);

            SetCopyButton(rgbCopyButton, rgbValue);
            SetCopyButton(hsvCopyButton, hsvValue);
            SetCopyButton(hexCopyButton, hexValue);

        }else{

            TagsEditText tagsEditText = (TagsEditText) currentView.findViewById(R.id.my_palette_tags_edit_text);
            tagsEditText.setEnabled(false);
            tagsEditText.setKeyListener(null);
            tagsEditText.setFocusable(false);

            ImageView heartImage = (ImageView)currentView.findViewById(R.id.my_palette_info_heart);

            TextView heartCount = (TextView)currentView.findViewById(R.id.my_palette_info_heart_count);

            TextView dateCreated = (TextView)currentView.findViewById(R.id.my_palette_date_created);

            final SwitchCompat switchCompat = (SwitchCompat)currentView.findViewById(R.id.my_palette_switch_compat);

            ViewFlipper exportFlipper = (ViewFlipper) currentView.findViewById(R.id.export_flipper);

            SetExportButton(exportFlipper, palette);

            if(info.getTags() != null) {
                String[] tags = info.getTags().toArray(new String[info.getTags().size()]);
                tagsEditText.setTags(tags);
            }else{
                tagsEditText.setTags(new String[0]);
            }
            heartImage.setImageDrawable(new IconicsDrawable(context)
                    .icon(GoogleMaterial.Icon.gmd_favorite)
                    .color(ContextCompat.getColor(context, R.color.accent))
                    .sizeDp(24));
            heartCount.setText(info.getHeartCount() + "");
            dateCreated.setText("Date Created: " + info.getDateCreated());

            if(palette.hasImage){
                switchCompat.setVisibility(View.GONE);
                Button cardButton = (Button) currentView.findViewById(R.id.cardButton);
                cardButton.setVisibility(View.VISIBLE);
                SetCardButton(cardButton, palette);
            }else {

                switchCompat.setOnCheckedChangeListener(null);
                switchCompat.setChecked(info.isPublic());
                if (info.isPublic()) {
                    switchCompat.setText(switchCompat.getTextOn());
                } else {
                    switchCompat.setText(switchCompat.getTextOff());
                }

                switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        Logger.LogDebug("PALETTE_INFO:", info.isPublic() + "  " + palette.firebaseKey);

                        if (isChecked) {
                            switchCompat.setText(switchCompat.getTextOn());
                        } else {
                            switchCompat.setText(switchCompat.getTextOff());
                        }
                        info.setPublic(isChecked);

                        Logger.LogDebug("PALETTE_INFO:", "SWITCH UPDATE");

                        PaletteBase.getInstance().updatePalettePublicity(palette.firebaseKey);

                    }
                });
            }



        }

    }

    private void SetCardButton(Button b, final RecyclerViewPalette palette){
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Logger.FireLog("MyPalette", "On Card Button Click");
                Toast.makeText(context, "Coming soon", Toast.LENGTH_SHORT).show();
//                List<String> filterList = new ArrayList<String>();
//                filterList.add(palette.firebaseKey);
//                MainActivity.drawerSelectedIndex = 4; // Card Palettes
//                ((MainActivity) context).changeMainToDrawerIndexedView(PaletteComparator.NO_SORT, filterList);
            }
        });
    }

    private void SetCopyButton(ImageView button, final TextView value){
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = value.getText().toString();
                Snackbar.make(v, "Copied " + text, Snackbar.LENGTH_SHORT).show();

                ClipData clip = ClipData.newPlainText("Copied Data", text);
                ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                clipboard.setPrimaryClip(clip);
            }
        });
        button.setImageDrawable(new IconicsDrawable(context)
                .icon(GoogleMaterial.Icon.gmd_content_copy)
                .color(Color.rgb(117, 117, 117))
                .sizeDp(24));
    }

    private void SetExportButton(final ViewFlipper flipper, final RecyclerViewPalette palette){

        flipper.setDisplayedChild(0);

        Button exportButton = (Button) flipper.getCurrentView();

        exportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                flipper.setDisplayedChild(1);

                LinearLayout ll = ((LinearLayout) flipper.getCurrentView());

                exportListeners(ll.getChildAt(0), 0, palette);
                exportListeners(ll.getChildAt(1), 1, palette);
                exportListeners(ll.getChildAt(2), 2, palette);

            }
        });
    }

    private void exportListeners(View v, final int index, final RecyclerViewPalette palette){
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AseCreator.SendAse(context, palette, index);
            }
        });
    }



}