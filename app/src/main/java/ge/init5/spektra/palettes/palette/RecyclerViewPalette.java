package ge.init5.spektra.palettes.palette;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;

import com.google.firebase.database.Exclude;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.ArrayList;
import java.util.List;

import ge.init5.spektra.models.FirePalette;
import ge.init5.spektra.utils.Logger;
import ge.init5.spektra.utils.colorUtils.ColorConverter;

import static ge.init5.spektra.utils.PaletteComparator.SORT_BY_DATE;
import static ge.init5.spektra.utils.PaletteComparator.SORT_BY_HEARTS_COUNT;
import static ge.init5.spektra.utils.PaletteComparator.SORT_BY_TITLE;

/**
 * Created by __ted__ on 11/12/16.
 */

public class RecyclerViewPalette extends ExpandableGroup<PaletteInfo> implements Parcelable{

    public String firebaseKey;

    private String title;

    public PaletteColor[] colors;

    public boolean isPublic;
    public boolean hasImage;

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<String> tags = new ArrayList<>();

    /**
     * RecyclerViewPalette constructor
     * @param title title of the palette
     * @param infos palette infos
     * @param firebaseKey firebase key
     * @param isPublic is public or not
     * @param tags tags
     * @param paletteColors palette colors
     */
    public RecyclerViewPalette(String title, List<PaletteInfo> infos, String firebaseKey, boolean isPublic, boolean hasImage, List<String> tags, PaletteColor... paletteColors){
        super(title, infos);
        this.setColors(new PaletteColor[5]);
        this.title = title;

        Logger.LogDebug("PALETTEVIEW", title + " " + (tags != null ? tags.get(0) : ""));


        for (int i = 0; i < paletteColors.length; i++) {
            this.colors[i] = paletteColors[i];
        }
        this.firebaseKey = firebaseKey;
        this.isPublic = isPublic;
        this.hasImage = hasImage;
        this.tags = tags;
    }

    protected RecyclerViewPalette(Parcel in) {
        super(in);
        firebaseKey = in.readString();
        title = in.readString();
        colors = in.createTypedArray(PaletteColor.CREATOR);
        isPublic = in.readByte() != 0;
        hasImage = in.readByte() != 0;
        tags = in.createStringArrayList();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(firebaseKey);
        dest.writeString(title);
        dest.writeTypedArray(colors, flags);
        dest.writeByte((byte) (isPublic ? 1 : 0));
        dest.writeByte((byte) (hasImage ? 1 : 0));
        dest.writeStringList(tags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RecyclerViewPalette> CREATOR = new Creator<RecyclerViewPalette>() {
        @Override
        public RecyclerViewPalette createFromParcel(Parcel in) {
            return new RecyclerViewPalette(in);
        }

        @Override
        public RecyclerViewPalette[] newArray(int size) {
            return new RecyclerViewPalette[size];
        }
    };


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }



    public PaletteColor[] getColors() {
        return colors;
    }

    public void setColors(PaletteColor[] paletteColors) {
        colors = paletteColors;
    }

    public String getFirebaseKey() {
        return firebaseKey;
    }

    public void setFirebaseKey(String firebaseKey) {
        this.firebaseKey = firebaseKey;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Name: " + getTitle() + "\n");
        for (int i = 0; i < getColors().length; i++) {
            sb.append("- - Color #" + (i+1) + " - - \n");
            sb.append(getColors()[i].toString());
            sb.append("- - - - - - - - - - - \n\n");
        }

        return sb.toString();
    }

    // 0 = RGB,  1 = HSV,  2 = HEX
    public String GetValuesInString(int ind){
        StringBuilder sb = new StringBuilder();
        for (PaletteColor col : getColors()){
            switch (ind){
                case 0:
                    sb.append(col.getRGBString());
                    break;
                case 1:
                    sb.append(col.getHSVString());
                    break;
                case 2:
                    sb.append(col.getHEXString());
                    break;
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    public int[] getColorInts(){
        return new int[]{
                ColorConverter.RGBtoInt(colors[0].getRGB()),
                ColorConverter.RGBtoInt(colors[1].getRGB()),
                ColorConverter.RGBtoInt(colors[2].getRGB()),
                ColorConverter.RGBtoInt(colors[3].getRGB()),
                ColorConverter.RGBtoInt(colors[4].getRGB())
        };
    }

    public String[] getColorsHex(){
        return new String[]{
                this.colors[0].getHEX(),
                this.colors[1].getHEX(),
                this.colors[2].getHEX(),
                this.colors[3].getHEX(),
                this.colors[4].getHEX()
        };
    }

}
