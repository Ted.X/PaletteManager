package ge.init5.spektra.palettes.publicPalette;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import ge.init5.spektra.MainActivity;
import ge.init5.spektra.R;
import ge.init5.spektra.models.FirePalette;
import ge.init5.spektra.palettes.palette.PaletteColor;
import ge.init5.spektra.palettes.palette.PaletteImageView;
import ge.init5.spektra.palettes.palette.RecyclerViewPalette;
import ge.init5.spektra.services.FirebasePaletteDatabase.PaletteBase;
import ge.init5.spektra.utils.Logger;

/**
 * FirePaletteViewHolder is used to hold the downloaded data
 * Created by __ted__ on 1/7/17.
 */

public class FirePaletteViewHolder extends RecyclerView.ViewHolder{

    public static final String TAG = FirePaletteViewHolder.class.getSimpleName();

    @BindView(R.id.public_palette_name)
    public TextView paletteName;

    @BindViews({R.id.public_palette_color_1, R.id.public_palette_color_2,
            R.id.public_palette_color_3, R.id.public_palette_color_4, R.id.public_palette_color_5})
    public List<PaletteImageView> mImageViews;

    @BindView(R.id.public_palette_download)
    public ImageButton publicPaletteDownloadButton;

    @BindView(R.id.public_palette_heart)
    public ImageButton publicPaletteHeartButton;

    @BindView(R.id.public_palette_heart_count)
    public TextView publicPaletteHeartCount;

    @BindView(R.id.public_palette_author)
    public TextView publicPaletteAuthor;

    @BindView(R.id.hearts_layout)
    public RelativeLayout heartsLayout;

    private FirePalette paletteRef;

    private String paletteOwnerUID;

    private String paletteKey;


    /**
     * Constructor for the fire palette view holder
     * @param itemView item view
     */
    public FirePaletteViewHolder(View itemView){
        super(itemView);
        ButterKnife.bind(this, itemView);

        // Buttons
        publicPaletteDownloadButton.setOnClickListener(DownloadPalette());
        heartsLayout.setOnClickListener(HeartPalette());
    }

    /**
     * Heart Button Click listener, updates heart count and hearts field
     * user value to true
     * @return on click listener
     */
    private View.OnClickListener HeartPalette() {
        return new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
            Animation heart = AnimationUtils.loadAnimation(v.getContext(), R.anim.heart_animation);

            heart.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    Logger.FireLog(TAG, "HeartAnimation started.");
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    Logger.FireLog(TAG, "HeartAnimation ended.");

                    publicPaletteHeartButton.setAlpha(0f);
                    PaletteBase.getInstance().updateHearts(paletteOwnerUID, paletteKey);

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }

            });

            publicPaletteHeartButton.startAnimation(heart);
            }
        };
    }

    /**
     * Download Button Click listener, clones the palette into the current user's space
     * @return
     */
    private View.OnClickListener DownloadPalette() {
        return new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Animation download = AnimationUtils.loadAnimation(v.getContext(), R.anim.heart_animation);

                download.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        Logger.FireLog(TAG, "DownloadAnimation started.");
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        Logger.FireLog(TAG, "DownloadAnimation ended.");


                        publicPaletteDownloadButton.setAlpha(0f);

                        PaletteBase.getInstance().downloadPaletteToUser(FirePaletteViewHolder.this.paletteRef, publicPaletteDownloadButton);

                        if (MainActivity.mInterstitialAd.isLoaded()){
                            MainActivity.mInterstitialAd.show();
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }

                });

                publicPaletteDownloadButton.startAnimation(download);
            }
        };
    }

    /**
     * Bind to post assigns the downloaded values to the xml UI elements
     * @param palette palette for this view holder
     * @param paletteKey palette's key
     */
    public void bindToPost(FirePalette palette, String paletteKey) {
        Logger.FireLog(TAG, "Bind to post called.");

        paletteName.setText(palette.title);
        publicPaletteAuthor.setText("By " + palette.author);
        this.paletteRef = palette;
        this.paletteKey = paletteKey;
        this.paletteOwnerUID = palette.uid;
        publicPaletteAuthor.setTypeface(Typeface.defaultFromStyle(Typeface.ITALIC));

        publicPaletteHeartCount.setText(String.valueOf(palette.heartCount));

        RecyclerViewPalette rPal = new RecyclerViewPalette(palette.title,
                null,
                null,
                false,
                false,
                palette.tags,
                new PaletteColor(palette.col1),
                new PaletteColor(palette.col2),
                new PaletteColor(palette.col3),
                new PaletteColor(palette.col4),
                new PaletteColor(palette.col5));

        for (int i = 0; i < mImageViews.size(); i++) {
            PaletteImageView m = mImageViews.get(i);
            m.color = rPal.getColors()[i].getInt();
            m.palName = rPal.getTitle();
            if(m.getWidth() > 0)
                m.requestLayout();
        }
    }

}