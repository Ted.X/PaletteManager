package ge.init5.spektra.palettes.publicPalette;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.wang.avi.AVLoadingIndicatorView;

import ge.init5.spektra.R;

/**
 * Created by alta on 6/5/17.
 */

public class LoadingViewHolder extends RecyclerView.ViewHolder {
    public AVLoadingIndicatorView progressBar;

    public LoadingViewHolder(View view) {
        super(view);
        progressBar = (AVLoadingIndicatorView) view.findViewById(R.id.progressBar1);
    }
}