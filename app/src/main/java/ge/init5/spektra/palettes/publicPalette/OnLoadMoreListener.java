package ge.init5.spektra.palettes.publicPalette;

/**
 * Created by alta on 6/5/17.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}
