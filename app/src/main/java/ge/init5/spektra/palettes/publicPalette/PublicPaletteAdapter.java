package ge.init5.spektra.palettes.publicPalette;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.LruCache;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;

import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ge.init5.spektra.R;
import ge.init5.spektra.models.FirePalette;
import ge.init5.spektra.palettes.cardPalette.CardPaletteViewHolder;
import ge.init5.spektra.services.FirebasePaletteDatabase.PaletteBase;

/**
 * Created by __ted__ on 1/7/17.
 */
public class PublicPaletteAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = PublicPaletteAdapter.class.getSimpleName();

    private Context context;
    private Map<String, FirePalette> palettesMap;
    private boolean paletteViewImage = false;


    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;


    private OnLoadMoreListener onLoadMoreListener;

    private boolean isLoading;
    private int visibleThreshold = 10;
    private int lastVisibleItem, totalItemCount;
    /**
     * Constructor for the public palettesMap
     * @param palettes list of palettesMap
     */
    public PublicPaletteAdapter(RecyclerView recyclerView, Map<String, FirePalette> palettes, boolean hasImage) {
        this.context = recyclerView.getContext();
        this.palettesMap = palettes;
        this.paletteViewImage = hasImage;


        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    LoadMore();
                    isLoading = true;
                }
            }
        });
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    public void LoadMore(){
        if (onLoadMoreListener != null) {
            Log.d("LOADING", "LoadMore");
            onLoadMoreListener.onLoadMore();
        }
    }

    /**
     * Sets the icon using the GoogleMaterial typeface with given color and size of 24dp
     * @param dullImageButton dull and boring image button
     * @param awesomeIcon awesome font icon
     * @param colorNsize first parameter color, second parameter size
     */
    public void setIcon(ImageButton dullImageButton, GoogleMaterial.Icon awesomeIcon, int... colorNsize){
        int size = colorNsize.length == 1 ? 24 : colorNsize[1];
        dullImageButton.setImageDrawable(new IconicsDrawable(context)
                .icon(awesomeIcon)
                .color(colorNsize[0])
                .sizeDp(size));
    }

    /**
     * Inflates public_palette_view and return new FirePaletteHolder
     * @param parent view group
     * @param viewType view type
     * @return
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;


        if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.palette_loader, parent, false);
            return new LoadingViewHolder(view);
        }
        if(paletteViewImage){
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_palette_view, parent, false);
            return new CardPaletteViewHolder(context, itemView);
        }else{
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.public_palette_view, parent, false);
            return new FirePaletteViewHolder(itemView);
        }
    }

    /**
     * Sets heart icons and other animation stuff then calls bind to post to assign all the palette
     * values to the view holder
     * @param holder fire palette view holder
     * @param position on what position in recycler view
     */
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof CardPaletteViewHolder){
            BindCardPalette((CardPaletteViewHolder)holder, position);
        }else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.show();
        }else{
            BindPalette((FirePaletteViewHolder) holder, position);
        }

    }


    /**
     * Returns the item count in palettesMap
     * @return item count
     */
    @Override
    public int getItemCount() {
        return palettesMap.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

    @Override
    public int getItemViewType(int position) {
        List<String> pKeys = new ArrayList<>(palettesMap.keySet());
        String paletteKey = pKeys.get(position);
        FirePalette model = palettesMap.get(paletteKey);
        if(model == null)
            return VIEW_TYPE_LOADING;
        return VIEW_TYPE_ITEM;

    }


    private void BindPalette(FirePaletteViewHolder holder, int position){
        List<String> pKeys = new ArrayList<>(palettesMap.keySet());
        String paletteKey = pKeys.get(position);
        FirePalette model = palettesMap.get(paletteKey);

        int white = context.getResources().getColor(R.color.white);
        int accent = context.getResources().getColor(R.color.accent);

        // Determine if the current user has liked this post and set UI accordingly
        if (model.hearts.containsKey(PaletteBase.getInstance().getUid())) {
            setIcon(holder.publicPaletteHeartButton,
                    GoogleMaterial.Icon.gmd_favorite, white);
            holder.heartsLayout.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_square_background_active));
            holder.publicPaletteHeartCount.setTextColor(white);
        } else {
            setIcon(holder.publicPaletteHeartButton,
                    GoogleMaterial.Icon.gmd_favorite, accent);
            holder.heartsLayout.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_square_background_passive));
            holder.publicPaletteHeartCount.setTextColor(accent);
        }

        Animation open = AnimationUtils.loadAnimation(context, R.anim.fab_open);
        AlphaAnimation fadeIn = new AlphaAnimation(0.2f, 1.0f);
        fadeIn.setDuration(100);
        fadeIn.setFillAfter(true);


        holder.publicPaletteHeartButton.setAlpha(1f);
        holder.publicPaletteHeartButton.startAnimation(open);

        holder.heartsLayout.startAnimation(fadeIn);



        // IF Mine
        if(model.uid.equals(PaletteBase.getInstance().getUid())){
            setIcon(holder.publicPaletteDownloadButton, GoogleMaterial.Icon.gmd_save,
                    Color.rgb(174, 213, 129));
            holder.publicPaletteDownloadButton.setActivated(false);
            holder.publicPaletteDownloadButton.setOnClickListener(null);
        }else {
            setIcon(holder.publicPaletteDownloadButton, GoogleMaterial.Icon.gmd_save,
                    Color.rgb(117, 117, 117));
        }

        holder.bindToPost(model, paletteKey);
    }


    private void BindCardPalette(CardPaletteViewHolder holder, int position) {
        List<String> pKeys = new ArrayList<>(palettesMap.keySet());
        String paletteKey = pKeys.get(position);
        FirePalette model = palettesMap.get(paletteKey);

        int white = context.getResources().getColor(R.color.white);
        int grey = context.getResources().getColor(R.color.grey);
        int accent = context.getResources().getColor(R.color.accent);

        // Determine if the current user has liked this post and set UI accordingly
        if (model.hearts.containsKey(PaletteBase.getInstance().getUid())) {
            setIcon(holder.heartImageView,
                    GoogleMaterial.Icon.gmd_favorite, accent,30);
            holder.heartCountView.setTextColor(accent);
//            holder.heartImageView.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_square_background_active));
//            holder.publicPaletteHeartCount.setTextColor(white);
        } else {
            setIcon(holder.heartImageView,
                    GoogleMaterial.Icon.gmd_favorite, grey, 30);
            holder.heartCountView.setTextColor(grey);
//            holder.heartImageView.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_square_background_passive));
//            holder.publicPaletteHeartCount.setTextColor(accent);
        }

        holder.isMine = model.uid.equals(PaletteBase.getInstance().getUid());


        setIcon(holder.downloadButton, GoogleMaterial.Icon.gmd_file_download, white, 70);
        setIcon(holder.reportButton, GoogleMaterial.Icon.gmd_report, white, 70);
        setIcon(holder.shareButton, GoogleMaterial.Icon.gmd_share, white, 70);
        setIcon(holder.paletteButton, GoogleMaterial.Icon.gmd_palette, white, 70);
        setIcon(holder.closeButton, GoogleMaterial.Icon.gmd_close, white);


        Animation open = AnimationUtils.loadAnimation(context, R.anim.fab_open);
        AlphaAnimation fadeIn = new AlphaAnimation(0.2f, 1.0f);
        fadeIn.setDuration(100);
        fadeIn.setFillAfter(true);

        holder.heartImageView.setAlpha(1f);
        holder.heartImageView.startAnimation(open);


        holder.setCard(model, paletteKey);
    }


}
