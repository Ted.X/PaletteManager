package ge.init5.spektra.services.FirebasePaletteDatabase;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import ge.init5.spektra.MainActivity;
import ge.init5.spektra.R;
import ge.init5.spektra.paletteEditor.share.ShareActivity;
import ge.init5.spektra.utils.Logger;

/**
 * Created by redpix_ on 2/19/17.
 */

public class ImageStorage implements OnFailureListener, OnSuccessListener<UploadTask.TaskSnapshot>{

    private static final String TAG = ImageStorage.class.getSimpleName();
    private static ImageStorage singleton = new ImageStorage();
    private static FirebaseStorage storageRef = FirebaseStorage.getInstance();

    private final static String original_card_palettes_folder = "palette_pics";
    private final static String scaled_card_palettes_folder = "feed_palette_pics";

    public static ImageStorage getInstance() {
        return singleton;
    }

    public void UploadImage(String fileName, final Uri uri, final Uri scaledUri) {
        StorageReference newPaletteImageRef = GetCardsStorage(fileName);
        StorageReference newPaletteScaledImageRef = GetCardFeedStorage(fileName);

        UploadTask uploadTask = newPaletteImageRef.putFile(uri);
        UploadTask uploadFeedTask = newPaletteScaledImageRef.putFile(scaledUri);

        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                new File(uri.getPath()).delete();
                Logger.FireLog(TAG, "Original Image Uploaded Successfully");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Logger.LogDebug("ImageStorage", "Original Image Upload Failed");
            }
        });

        uploadFeedTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                new File(scaledUri.getPath()).delete();
                Logger.FireLog(TAG, "Scaled Image Uploaded Successfully");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Logger.LogDebug("ImageStorage", "Scaled Image Upload Failed");
            }
        });
    }

    public void DeleteImage(String fileName){

        StorageReference cardRef = GetCardsStorage(fileName);
        StorageReference cardFeedRef = GetCardFeedStorage(fileName);

        cardRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Logger.LogDebug(TAG, "Deleted Card");

                Logger.FireLog(TAG, "Card Palette Deleted");
            }
        });

		cardFeedRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Logger.LogDebug(TAG, "Deleted Feed Card");

                Logger.FireLog(TAG, "Feed Card Palette Deleted");
            }
        });

    }

//    public void DownloadFeedImage(final Context context, final String fileName, final ImageView target){
//        final long ONE_MEGABYTE = 1024 * 1024;
//        Logger.LogDebug(TAG, "Key " + fileName);
//        GetCardFeedStorage(fileName).getBytes(ONE_MEGABYTE * 2).addOnSuccessListener(new OnSuccessListener<byte[]>() {
//            @Override
//            public void onSuccess(byte[] bytes) {
//                Logger.LogDebug(TAG, "Downloaded Card");
//                Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
//
//                Logger.FireLog(TAG, "Card Palette Downloaded");
////                ((MainActivity) context).addUriToMemoryCache(fileName, bitmap);
//                target.setImageBitmap(bitmap);
//                ((MainActivity)context).addUriToMemoryCache(fileName, bitmap);
////                Picasso.with(context).
////                Picasso.with(context).load()
//
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Logger.LogDebug(TAG, "Failed Download Card");
//            }
//        });
//    }

    public void DownloadFeedImage(final Context context, final String fileName, final ImageView target){
            GetCardFeedStorage(fileName).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    // Got the download URL for 'users/me/profile.png'
                    Logger.LogDebug(TAG, uri.getPath());
                    Glide.with(context)
                            .load(uri)
                            .placeholder(R.drawable.rectpic)
                            .into(target);

                    ((MainActivity)context).addUriToMemoryCache(fileName, uri);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle any errors
                    Logger.LogDebug(TAG, "FAILED");
                }
            });

    }


    public void DownloadOriginalImage(final Context context, final String fileName, final boolean share){

        final long mgb = 1024 * 1024 * 2;
        Logger.LogDebug(TAG, "Key " + fileName);
        GetCardsStorage(fileName).getBytes(mgb).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                File file = new File(Environment.getExternalStoragePublicDirectory("Paletter"), "");

                if (!file.exists()) {
                    file.mkdir();
                }

                Bitmap imageBitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);


                try {
                    if(share) {
                        File sharedFile = File.createTempFile("shared", ".png", context.getExternalCacheDir());
                        FileOutputStream outputStream = new FileOutputStream(sharedFile);

                        imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);

                        Toast.makeText(context, "Sharing...", Toast.LENGTH_LONG).show();

                        ShareActivity.ShareExcludingApp(context, Uri.fromFile(sharedFile));
                    }else {
                        File downloadedFIle = new File(file, "IMG_" + (new SimpleDateFormat("yyyyMMddHHmmss")).format(Calendar.getInstance().getTime()) + ".jpg");


                            FileOutputStream outputStream = new FileOutputStream(downloadedFIle);
                            imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);

                            Toast.makeText(context, "Image Saved", Toast.LENGTH_LONG).show();


                        MediaScannerConnection.scanFile(context, new String[]{downloadedFIle.getAbsolutePath()}, null, null);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Logger.LogDebug(TAG, "Failed Download Card");
            }
        });
    }

    private StorageReference GetReferenceFromUrl(){
        return storageRef.getReferenceFromUrl("gs://spektra-80eda.appspot.com");
    }
    public StorageReference GetCardsStorage(String fileName){
        return GetReferenceFromUrl().child(original_card_palettes_folder).child(fileName + ".png");
    }

    public StorageReference GetCardFeedStorage(String fileName){
        return GetReferenceFromUrl().child(scaled_card_palettes_folder).child(fileName + ".png");
    }


    @Override
    public void onFailure(@NonNull Exception e) {
        // Handle supernova here
        Logger.LogDebug("ImageStorage", "Image Upload Failed");
    }

    @Override
    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
        Uri downloadUrl = taskSnapshot.getDownloadUrl();
        Logger.LogDebug("ImageStorage", "Image Upload Succeeded. Download URL: " + downloadUrl);
    }
}
