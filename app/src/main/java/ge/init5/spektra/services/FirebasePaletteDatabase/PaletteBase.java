package ge.init5.spektra.services.FirebasePaletteDatabase;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.BaseColumns;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CursorAdapter;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import ge.init5.spektra.MainActivity;
import ge.init5.spektra.R;
import ge.init5.spektra.models.ColorModel;
import ge.init5.spektra.models.FirePalette;
import ge.init5.spektra.models.FireTag;
import ge.init5.spektra.models.FireUser;
import ge.init5.spektra.paletteCreator.AddPaletteActivity;
import ge.init5.spektra.palettes.palette.PaletteAdapter;
import ge.init5.spektra.palettes.palette.PaletteColor;
import ge.init5.spektra.palettes.palette.PaletteInfo;
import ge.init5.spektra.palettes.palette.RecyclerViewPalette;
import ge.init5.spektra.palettes.publicPalette.OnLoadMoreListener;
import ge.init5.spektra.palettes.publicPalette.PublicPaletteAdapter;
import ge.init5.spektra.utils.Logger;
import ge.init5.spektra.utils.PaletteComparator;

/**
 * Singleton class for communicating with firebase and doing all sorts of stuff
 * Created by __ted__ on 1/3/17.
 */
public class PaletteBase {

    private static final String TAG = PaletteBase.class.getSimpleName();

    private static PaletteBase singleton = new PaletteBase();

    private Map<String, FirePalette> paletteKeyMap = new LinkedHashMap<>();
    private boolean isUser = false;
    private ChildEventListener publicChildEventListener = null;
    private static boolean isPersistanceEnabled = false;

    public SharedPreferences sharedPreferences;

    private final static String LAST_LOGIN_PREFS_KEY = "LAST_LOGIN_DATE";
    private final static String DOWNLOAD_COUNT_PREFS_KEY = "DOWNLOAD_COUNT";
    private final static int DAILY_DOWNLOAD_LIMIT = 5;

    public List<ColorModel> colorNameDatabase;

    public FirebaseUser firebaseUser;

    private PaletteBase() { }

    public static PaletteBase getInstance() {
        return singleton;
    }

    public DatabaseReference getFirebaseDatabaseRoot() {
        return FirebaseDatabase.getInstance().getReference();
    }

    public FirebaseUser getFirebaseUser() {
        return FirebaseAuth.getInstance().getCurrentUser();
    }

    public String getUid() {
        return getFirebaseUser().getUid();
    }

    // Firebase Persistence
    public void setPersistenceEnabled(){
        if (!isPersistanceEnabled){
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
            isPersistanceEnabled = true;
        }
    }

    public void setSharedPreferences(Context context){

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        long lastLogin = sharedPreferences.getLong(PaletteBase.LAST_LOGIN_PREFS_KEY, 0);

        long currentTimeMillis = System.currentTimeMillis();

        long daysCount = TimeUnit.MILLISECONDS.toDays(currentTimeMillis - lastLogin);

        SharedPreferences.Editor dora = sharedPreferences.edit();

        if (daysCount > 1){
            dora.putInt(DOWNLOAD_COUNT_PREFS_KEY, 0).apply();
            dora.putLong(LAST_LOGIN_PREFS_KEY, currentTimeMillis).apply();
        }


    }

    // Firebase Queries
    public Query getUserPalettesQuery() {
        return getFirebaseDatabaseRoot().child("user-palettes").child(getUid());
    }

    public Query getUserQuery() {
        return getFirebaseDatabaseRoot().child("users").child(getUid());
    }

    public Query getPublicPalettesQuery() {
        return getFirebaseDatabaseRoot().child("palettes").orderByChild("isPublic").equalTo(true);//.limitToLast(10);
    }

    public Query getFavoritePalettesQuery(){
        return getFirebaseDatabaseRoot().child("palettes").orderByChild("hearts/" + getUid()).equalTo(true);//.limitToLast(10);
    }

    public Query getCardPalettesQuery() {
        return getFirebaseDatabaseRoot().child("palettes").orderByChild("hasImage").equalTo(true);//.limitToLast(10);
    }


    public Query getUserPalettesQueryBy(int orderBy){
        switch (orderBy){
            case PaletteComparator.SORT_BY_TITLE:
                return getUserPalettesQuery().orderByChild("title");
            case PaletteComparator.SORT_BY_HEARTS_COUNT:
                return getUserPalettesQuery().orderByChild("heartsCount");
            case PaletteComparator.SORT_BY_DATE:
                return getUserPalettesQuery().limitToLast(Integer.MAX_VALUE);
            default:
                // NO SORT
                return getUserPalettesQuery();
        }
    }

    public Query getKeywordsQuery(String parent, String suggest) {
        Query q = getFirebaseDatabaseRoot().child("keywords").child(parent).orderByKey().startAt(suggest).endAt(suggest + "~");
        return q;
    }

    public void downloadColorNames() {
        colorNameDatabase = new ArrayList<>();
        getFirebaseDatabaseRoot().child("color-names").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot color : dataSnapshot.getChildren()){
                    ColorModel colorModel = color.getValue(ColorModel.class);
                    colorNameDatabase.add(colorModel);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void getUsersPalettes(Query query, final Context context, final RecyclerView recyclerView, @Nullable final List<String> filterList, final int orderBy){
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<RecyclerViewPalette> recyclerViewPaletteList = new ArrayList<>();
                Map<String, FirePalette> a = new HashMap<String, FirePalette>();

                if (dataSnapshot.hasChildren()) {
                    for (DataSnapshot ps : dataSnapshot.getChildren()) {
                        if (filterList == null || filterList.contains(ps.getKey())) {
                            FirePalette palette = ps.getValue(FirePalette.class);
                            a.put(ps.getKey(), palette);
                        }
                    }
                }

                List<Map.Entry<String, FirePalette>> bitch = new ArrayList<Map.Entry<String, FirePalette>>(a.entrySet());


//                if (orderBy != 0) {
//                    publicPalettes.sort(new PaletteComparator(orderBy));
                    Collections.sort(bitch, new PaletteComparator(orderBy));
                    for (Map.Entry<String, FirePalette> entry : bitch) {
                        FirePalette palette = entry.getValue();
                        String key = entry.getKey();
                        List<PaletteInfo> infos = new ArrayList<>();
                        infos.add(new PaletteInfo(palette.getDateCreated(), palette.isPublic, palette.heartCount, palette.tags));

                        Logger.LogDebug("PALETTEVIEW", "WAT? " + (palette.tags != null ? palette.tags.get(0) : ""));
                        RecyclerViewPalette pal = new RecyclerViewPalette(
                                palette.title,
                                infos,
                                key,
                                palette.isPublic,
                                palette.hasImage,
                                palette.tags,
                                new PaletteColor(palette.col1),
                                new PaletteColor(palette.col2),
                                new PaletteColor(palette.col3),
                                new PaletteColor(palette.col4),
                                new PaletteColor(palette.col5));


                        recyclerViewPaletteList.add(pal);
//                    }

                }
                recyclerView.setAdapter(new PaletteAdapter(context, recyclerView, recyclerViewPaletteList));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Logger.LogError("The read failed: ", databaseError.getMessage());
            }
        });
    }

    public void removeUpdateChildEventListener(Query query){
        Logger.FireLog(TAG, "Remove Listener Query: " + query.getRef().toString() + " Listener state: " +
                (this.publicChildEventListener != null ? "Not Null" : "Null"));
        if (this.publicChildEventListener != null){
            query.removeEventListener(this.publicChildEventListener);
        }
    }

    public void getPublicPalettes(final Query q, final RecyclerView recyclerView, PublicPaletteAdapter publicAdapter, final List<String> filterList, final int orderBy, final boolean hasImage) {
//        publicPaletteKeys = new ArrayList<>();
//        final List<FirePalette> publicPalettes = new ArrayList<>();
//
//        paletteKeyMap = orderBy == 0 ? new TreeMap<String, FirePalette>() :
//        paletteKeyMap = new TreeMap<String, FirePalette>(new PaletteComparator(orderBy));

        Query query;// = q.limitToLast(10);
        final PublicPaletteAdapter adapter;

        if(publicAdapter == null) {
            int limit = 15;
            if(filterList != null && filterList.size() > 0) limit = filterList.size() + 1;

            query = q.limitToLast(limit);
            paletteKeyMap = new LinkedHashMap<>();

            adapter = new PublicPaletteAdapter(recyclerView, paletteKeyMap, hasImage);

            recyclerView.setAdapter(adapter);
        }else{
            query = q.limitToLast(paletteKeyMap.size() + 10);
            adapter = publicAdapter;
        }

        paletteKeyMap.put("", null);
//        adapter.notifyItemInserted(paletteKeyMap.size() - 1);
        adapter.notifyDataSetChanged();

        adapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {

                if (paletteKeyMap.size() > 1) {
                    List<String> pKeys = new ArrayList<>(paletteKeyMap.keySet());
                    String paletteKey = pKeys.get(paletteKeyMap.size() - 1);
//                    q = q.limitToLast(10);

                    getPublicPalettes(q, recyclerView, adapter, filterList, orderBy, hasImage);
                }
            }

        });



        removeUpdateChildEventListener(query);

        PaletteBase.this.publicChildEventListener = query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Logger.FireLog(TAG, "Added " + dataSnapshot.getKey() + q.toString());
                if(filterList == null || filterList.contains(dataSnapshot.getKey())) {
//                    publicPalettes.add(pal);
//                    publicPaletteKeys.add(dataSnapshot.getKey());
                    paletteKeyMap.put(dataSnapshot.getKey(), dataSnapshot.getValue(FirePalette.class));

//                    if (recyclerView.getAdapter() != null) {
////                        recyclerView.getAdapter().notifyItemInserted(publicPaletteKeys.indexOf(dataSnapshot.getKey()));
//                        recyclerView.getAdapter().notifyItemInserted(
//                                new ArrayList<>(paletteKeyMap.keySet()).indexOf(dataSnapshot.getKey()));
//                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Logger.FireLog(TAG, "Changed " + dataSnapshot.getKey() + q.toString());
//                int pos = publicPaletteKeys.indexOf(dataSnapshot.getKey());
                paletteKeyMap.put(dataSnapshot.getKey(), dataSnapshot.getValue(FirePalette.class));

                int index = new ArrayList<>(paletteKeyMap.keySet()).indexOf(dataSnapshot.getKey());
                recyclerView.getAdapter().notifyItemChanged(index);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Logger.FireLog(TAG, "Removed " + dataSnapshot.getKey() + q.toString());

//                int index = publicPaletteKeys.indexOf(dataSnapshot.getKey());
                recyclerView.getAdapter().notifyItemRemoved(new ArrayList<>(paletteKeyMap.keySet()).indexOf(dataSnapshot.getKey()));
                paletteKeyMap.remove(dataSnapshot.getKey());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Logger.FireLog(TAG, "Moved " + dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Logger.FireLog(TAG, "Canceled");
            }
        });

        query.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                paletteKeyMap.remove("");
                adapter.notifyItemRemoved(paletteKeyMap.size());

                List<Map.Entry<String, FirePalette>> a = new ArrayList<>(paletteKeyMap.entrySet());
                if (orderBy != 0) {
//                    publicPalettes.sort(new PaletteComparator(orderBy));
                    Collections.sort(a, new PaletteComparator(orderBy));
                    paletteKeyMap.clear();
                    for (Map.Entry<String, FirePalette> entry : a)
                        paletteKeyMap.put(entry.getKey(), entry.getValue());
                }

                adapter.notifyDataSetChanged();
                adapter.setLoaded();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // Make event listener null

    }

    public void writeNewUserPalette(final Context context,
                                    final View view,
                                    final String title,
                                    final String oldTitle,
                                    final String firebaseKey,
                                    final Boolean isPublic,
                                    final Boolean hasImage,
                                    final Uri imageUri,
                                    final Uri imageScaledUri,
                                    final List<PaletteColor> colors,
                                    final List<String> paletteTags,
                                    final List<String> paletteTagsOld,
                                    final View loader) {
        getUserQuery().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get user data
                FireUser user = dataSnapshot.getValue(FireUser.class);

                boolean done = false;
                if (user == null) {
                    Snackbar.make(view, "Error: Couldn't fetch user.", Snackbar.LENGTH_LONG).show();
                } else {

                    String paletteKey;
                    if (firebaseKey == null) {
                        // Get new palette key
                        paletteKey = getFirebaseDatabaseRoot().child("palettes").push().getKey();
                    } else {
                        paletteKey = firebaseKey;
                    }

                    FirePalette palette = new FirePalette(
                            title,
                            user.name,
                            colors.get(0).getHEX(),
                            colors.get(1).getHEX(),
                            colors.get(2).getHEX(),
                            colors.get(3).getHEX(),
                            colors.get(4).getHEX(),
                            isPublic,
                            hasImage,
                            getUid(),
                            paletteTags);
                    Map<String, Object> paletteValues = palette.toMap();
                    Map<String, Object> childUpdates = new HashMap<>();

                    childUpdates.put("/palettes/" + paletteKey, paletteValues);
                    childUpdates.put("/user-palettes/" + getUid() + "/" + paletteKey, paletteValues);

                    getFirebaseDatabaseRoot().updateChildren(childUpdates);
                    updateKeywordsReference("tags", paletteKey, palette.tags, paletteTagsOld);
                    updateKeywordsReference("names", paletteKey,
                            new LinkedList<String>(Arrays.asList(title)), new LinkedList<>(Arrays.asList(oldTitle)));

                    if(hasImage && imageUri != null && imageScaledUri != null)
                        ImageStorage.getInstance().UploadImage(paletteKey, imageUri, imageScaledUri);
                    done = true;
                }

                loader.setVisibility(View.VISIBLE);

                ((AddPaletteActivity) context).setEditingEnabled(true);

                if(done) {
                    Intent returnIntent = ((AddPaletteActivity) context).getIntent();
                    ((AddPaletteActivity) context).setResult(AppCompatActivity.RESULT_OK, returnIntent);
                }
                ((AddPaletteActivity) context).finish();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Logger.LogWarn(TAG, "getUser:onCancelled", databaseError.toException());
                ((AddPaletteActivity) context).setEditingEnabled(true);
            }
        });
    }

    public void updateKeywordsReference(final String parentReference, final String paletteKey, List<String> newKeywords, List<String> oldKeywords) {
        List<String> newSaved = new ArrayList<>(newKeywords);

        if (oldKeywords != null && oldKeywords.size() > 0) {
            newKeywords.removeAll(oldKeywords);
            oldKeywords.removeAll(newSaved);

            for (final String oldTag : oldKeywords) {
                final DatabaseReference q = getFirebaseDatabaseRoot().child("keywords").child(parentReference);
                q.child(oldTag).orderByChild("paletteKey").equalTo(paletteKey).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        Map<String, Object> childUpdate = new HashMap<>();
                        for (DataSnapshot child : dataSnapshot.getChildren())
                            childUpdate.put("/" + oldTag + "/" + child.getKey(), null);

                        q.updateChildren(childUpdate);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        }

        for (String newTag : newKeywords) {
            FireTag keywordModelObject = new FireTag(paletteKey, getUid());
            String newKey = getFirebaseDatabaseRoot().child("keywords").child(parentReference).child(newTag).push().getKey();
            Map<String, Object> childUpdate = new HashMap<>();
            childUpdate.put(newTag + "/" + newKey, keywordModelObject.toMap());
            getFirebaseDatabaseRoot().child("keywords").child(parentReference).updateChildren(childUpdate);
        }
    }

    public void downloadPaletteToUser(final FirePalette firePalette, final ImageButton view) {
        final int downloadCount = sharedPreferences.getInt(PaletteBase.DOWNLOAD_COUNT_PREFS_KEY, 0);

        if (downloadCount < DAILY_DOWNLOAD_LIMIT){
            getUserQuery().addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    // Get user data
                    FireUser user = dataSnapshot.getValue(FireUser.class);

                    if (user == null) {
                        Snackbar.make(view, "Error: Couldn't fetch user.", Snackbar.LENGTH_LONG).show();
                    } else {
                        FirePalette newPalette = new FirePalette(firePalette, user.name, getUid());
                        String paletteKey = getFirebaseDatabaseRoot().child("palettes").push().getKey();

                        Map<String, Object> paletteValues = newPalette.toMap();
                        Map<String, Object> childUpdates = new HashMap<>();
                        childUpdates.put("/palettes/" + paletteKey, paletteValues);
                        childUpdates.put("/user-palettes/" + getUid() + "/" + paletteKey, paletteValues);

                        getFirebaseDatabaseRoot().updateChildren(childUpdates);
                    }
                    sharedPreferences.edit().putInt(DOWNLOAD_COUNT_PREFS_KEY, downloadCount + 1).apply();


                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Logger.LogWarn(TAG, "getUser:onCancelled", databaseError.toException());
                }
            });
        }else{
            Snackbar.make(view, "Daily download limit " + DAILY_DOWNLOAD_LIMIT + " reached. Try again tomorrow.",
                    Snackbar.LENGTH_LONG).show();
        }
        Animation open = AnimationUtils.loadAnimation(view.getContext(), R.anim.fab_open);
        view.setAlpha(1f);
        view.startAnimation(open);
    }

    public void setSearchSystem(final Context context, final SearchView searchView, final Menu menu) {

        final String[] from = new String[]{"keyWord"};
        final int[] to = new int[]{android.R.id.text1};
        final SimpleCursorAdapter mAdapter = new SimpleCursorAdapter(context,
                android.R.layout.simple_dropdown_item_1line,
                null,
                from,
                to,
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

        searchView.setSuggestionsAdapter(mAdapter);

//        final List<String> suggestedTags = new ArrayList<>();
//        final List<FireTag> suggestedModels = new ArrayList<>();

        final Map<String, List<String> > suggests = new HashMap<>();

        final String[] shebang = {""};


        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Logger.FireLog(TAG, "Search action clicked, hiding action_filter!");
                menu.findItem(R.id.action_filter).setVisible(false);
                isUser = MainActivity.drawerSelectedIndex == 1;
            }
        });

        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionClick(int position) {
                Cursor cursor = (Cursor) searchView.getSuggestionsAdapter().getItem(
                        position);
                String suggest = cursor.getString(cursor.getColumnIndex("keyWord"));
                searchView.setQuery(shebang[0] + suggest, true);

                ((MainActivity)context).changeMainToDrawerIndexedView(PaletteComparator.NO_SORT, suggests.get(suggest));

                return true;
            }

            @Override
            public boolean onSuggestionSelect(int position) {
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(final String query) {

                String parent = "";
                String keyword = query;
                if(query.length() > 1){
                    if(query.charAt(0) == '.'){
                        shebang[0] = ".";
                        parent = "names";
                    }else if(query.charAt(0) == '#'){
                        shebang[0] = "#";
                        parent = "tags";
                    }
                    if(query.length() == 1)
                        return false;
                    else
                        keyword = query.substring(1);
                }

                final String finalKeyword = keyword;
                getKeywordsQuery(parent, keyword).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        suggests.clear();
                        if(!dataSnapshot.exists()) return;
//                        suggestedModels.clear();
                        for (DataSnapshot snap : dataSnapshot.getChildren()) {

                            for (DataSnapshot tag : snap.getChildren()) {
                                FireTag m = tag.getValue(FireTag.class);
                                if (isUser) {
                                    if (m.authorUid.equals(getUid())) {
                                        if(!suggests.containsKey(snap.getKey()))
                                            suggests.put(snap.getKey(), new ArrayList<String>());
                                        suggests.get(snap.getKey()).add(m.paletteKey);

                                    }
                                } else {
                                    if(!suggests.containsKey(snap.getKey()))
                                        suggests.put(snap.getKey(), new ArrayList<String>());
                                    suggests.get(snap.getKey()).add(m.paletteKey);

                                }
                            }
                        }

                        final MatrixCursor c = new MatrixCursor(new String[]{BaseColumns._ID, "keyWord"});
                        int i = 0;
                        for(Map.Entry<String, List<String> > tag : suggests.entrySet()){
                            if (tag.getKey().toLowerCase().startsWith(finalKeyword.toLowerCase()))
                                c.addRow(new Object[]{i, tag.getKey()});
                            i++;
                        }
                        mAdapter.changeCursor(c);

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                return false;
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                ((MainActivity)context).changeMainToDrawerIndexedView(MainActivity.PREVIOUS_SORTING_OPTION, null);
                searchView.onActionViewCollapsed();
                menu.findItem(R.id.action_filter).setVisible(true);
                return true;
            }
        });

    }

    public void updateUserPalette(Context context, String paletteKey, String title, List<String> tags, List<PaletteColor> colors, View loader){
        DatabaseReference globalPaletteReference = getFirebaseDatabaseRoot().child("palettes")
                .child(paletteKey);
        DatabaseReference userPaletteReference = getFirebaseDatabaseRoot().child("user-palettes")
                .child(getUid()).child(paletteKey);

        updatePaletteWithTransaction(context, userPaletteReference, title, tags, colors, loader, true);
        updatePaletteWithTransaction(context, globalPaletteReference, title, tags, colors, loader, false);
    }


    public void updatePaletteWithTransaction(final Context context,
                                             final DatabaseReference paletteReference,
                                             final String title,
                                             final List<String> tags,
                                             final List<PaletteColor> colors,
                                             final View loader,
                                             final boolean finishActivity){

        paletteReference.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                FirePalette palette = mutableData.getValue(FirePalette.class);
                if (palette == null) {
                    return Transaction.success(mutableData);
                }

                palette.title = title;
                palette.tags = tags;
                palette.col1 = colors.get(0).getHEX();
                palette.col2 = colors.get(1).getHEX();
                palette.col3 = colors.get(2).getHEX();
                palette.col4 = colors.get(3).getHEX();
                palette.col5 = colors.get(4).getHEX();

                mutableData.setValue(palette);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                Logger.FireLog(TAG, "postTransaction:onComplete: " + databaseError);

                if(finishActivity) {
                    loader.setVisibility(View.VISIBLE);

                    ((AddPaletteActivity) context).setEditingEnabled(true);

                    Intent returnIntent = ((AddPaletteActivity) context).getIntent();
                    ((AddPaletteActivity) context).setResult(AppCompatActivity.RESULT_OK, returnIntent);

                    ((AddPaletteActivity) context).finish();
                }
            }
        });

    }

    public void updatePalettePublicity(String paletteKey) {
        DatabaseReference globalPaletteReference = getFirebaseDatabaseRoot().child("palettes")
                .child(paletteKey);
        DatabaseReference userPaletteReference = getFirebaseDatabaseRoot().child("user-palettes")
                .child(getUid()).child(paletteKey);

        updatePalettePublicWithTransaction(globalPaletteReference);
        updatePalettePublicWithTransaction(userPaletteReference);
    }

    private void updatePalettePublicWithTransaction(DatabaseReference paletteReference) {
        paletteReference.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                FirePalette palette = mutableData.getValue(FirePalette.class);
                if (palette == null) {
                    return Transaction.success(mutableData);
                }

                palette.isPublic = !palette.isPublic;

                mutableData.setValue(palette);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                Logger.FireLog(TAG, "postTransaction:onComplete: " + databaseError);
            }
        });
    }

    public void updateHearts(String ownerUID, String paletteKey) {
        // Get global and user references of palette
        final DatabaseReference globalPaletteReference = getFirebaseDatabaseRoot().child("palettes")
                .child(paletteKey);
        final DatabaseReference userPaletteReference = getFirebaseDatabaseRoot().child("user-palettes")
                .child(ownerUID).child(paletteKey);

        updatePaletteHearts(globalPaletteReference);
        updatePaletteHearts(userPaletteReference);
    }

    private void updatePaletteHearts(DatabaseReference paletteReference) {
        paletteReference.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                FirePalette palette = mutableData.getValue(FirePalette.class);
                if (palette == null) {
                    return Transaction.success(mutableData);
                }

                if (palette.hearts.containsKey(PaletteBase.getInstance().getUid())) {
                    palette.heartCount = palette.heartCount - 1;
                    palette.hearts.remove(PaletteBase.getInstance().getUid());
                } else {
                    palette.heartCount = palette.heartCount + 1;
                    palette.hearts.put(PaletteBase.getInstance().getUid(), true);
                }

                mutableData.setValue(palette);

                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                Logger.FireLog(TAG, "postTransaction:onComplete: " + databaseError);
            }
        });
    }

    public void deleteUserPalette(String paletteKey, List<String> tags, String title) {
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/palettes/" + paletteKey, null);
        childUpdates.put("/user-palettes/" + getUid() + "/" + paletteKey, null);

        getFirebaseDatabaseRoot().updateChildren(childUpdates);

        updateKeywordsReference("tags", paletteKey, new ArrayList<String>(), tags);
        updateKeywordsReference("names", paletteKey,
                new LinkedList<String>(), new LinkedList<String>(Arrays.asList(title)));

        ImageStorage.getInstance().DeleteImage(paletteKey);
    }
}
