/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ge.init5.spektra.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import ge.init5.spektra.palettes.palette.RecyclerViewPalette;

/**
 *
 * @author ted
 */
public class AseCreator{

    /**
     * @param context context
     */
    public static void SendAse(final Context context, RecyclerViewPalette palette, int mode) {
        try {
            ByteArrayOutputStream baus = new ByteArrayOutputStream();
            DataOutputStream stream = new DataOutputStream(baus);
            
//            int[] colors = new int[]{
//                    0xfe4365,
//                    0xfc9d9a,
//                    0xf9cdad,
//                    0xc8c8a9,
//                    0x83af9b
//            };


            int[] colorsInt = palette.getColorInts();
            String[] colorsHex = palette.getColorsHex();

            encode(stream, colorsInt, colorsHex);

            FileOutputStream outputStream;
            if(mode < 2) {// creative cloud
                File file = File.createTempFile(palette.getTitle(), ".ase", context.getExternalCacheDir());
                outputStream = new FileOutputStream(file);

                if(mode == 0){
                    Toast.makeText(context, "Coming Soon", Toast.LENGTH_LONG).show();
                }

                if(mode == 1) {// email

                    Uri path = Uri.fromFile(file);
                    Intent emailIntent = new Intent(Intent.ACTION_SEND);

                    emailIntent.setType("vnd.android.cursor.dir/email");

                    emailIntent.putExtra(Intent.EXTRA_STREAM, path);

                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
                    context.startActivity(Intent.createChooser(emailIntent, "Send email..."));

//                    Intent intent = new Intent(Intent.ACTION_SENDTO);
//                    intent.setType("text/plain");
//                    intent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
//                    intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+path));
////                    intent.setData(Uri.parse("mailto:xyz@gmail.com"));
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    context.startActivity(intent);

                }
            }
            else{

                File f = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), palette.getTitle() + ".ase");

                outputStream = new FileOutputStream(f);
                Toast.makeText(context, "Saved " + palette.getTitle() + ".ase", Toast.LENGTH_LONG).show();
            }

            baus.writeTo(outputStream);
            outputStream.close();

//            AdobeAssetFile.create(palette.getTitle() + ".ase",
//                    AdobeAssetFolder.getRoot(),
//                    new File(file.getAbsolutePath()).toURI().toURL(),
//                    "ASEF",
//                    new IAdobeGenericRequestCallback<AdobeAssetFile, AdobeCSDKException>() {
//                        @Override
//                        public void onCompletion(AdobeAssetFile adobeAssetFile) {
//                            Toast.makeText(context, "onCompletion", Toast.LENGTH_SHORT).show();
//
//                        }
//
//                        @Override
//                        public void onError(AdobeCSDKException e) {
//                            Toast.makeText(context, "onError", Toast.LENGTH_SHORT).show();
//                        }
//
//                        @Override
//                        public void onCancellation() {
//                            Toast.makeText(context, "onCancellation", Toast.LENGTH_SHORT).show();
//                        }
//
//                        @Override
//                        public void onProgress(double v) {
//                            Toast.makeText(context, "onProgress", Toast.LENGTH_SHORT).show();
//                        }
//                    },
//                    new Handler());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void encode(DataOutputStream ase, int[] colorsInt, String[] colorsHex) throws IOException {
        // Write Header ASEF
        ase.writeBytes("ASEF");
        // Write Version
        ase.writeInt(0x10000);
        // Write Number Of Blocks
        ase.writeInt(colorsInt.length * 2);


        // Iterate over colors writing them
        for (int i = 0; i < colorsInt.length; i++) {
            int eachInt = colorsInt[i];
            String eachHex = colorsHex[i];

            ByteArrayOutputStream pusi = new ByteArrayOutputStream();
            DataOutputStream swatch = new DataOutputStream(pusi);

            // Start of group
            ase.writeShort(49153);
            ase.writeInt(0);
            ase.writeShort(1);

            swatch.writeShort(eachHex.length() + 1);
            for (int n = 0; n < eachHex.length(); n++)
                swatch.writeShort(eachHex.charAt(n));
            swatch.writeShort(0);

            // Colors
            swatch.writeBytes("RGB ");
            swatch.writeFloat((eachInt >> 16 & 0xFF) / 255f);
            swatch.writeFloat((eachInt >> 8 & 0xFF) / 255f);
            swatch.writeFloat((eachInt & 0xFF) / 255f);
            swatch.writeShort(2);

            // Write Swatch
            ase.writeInt(swatch.size());
            ase.write(pusi.toByteArray());
            pusi = null;
            swatch = null;
        }
    }

}
