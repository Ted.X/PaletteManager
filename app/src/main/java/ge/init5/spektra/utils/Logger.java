package ge.init5.spektra.utils;

import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.database.DatabaseException;

/**
 * Created by __ted__ on 2/24/17.
 */

public class Logger {
    public static boolean ENABLED = false;
    public static boolean FIRE_CRASH_LOG_ENABLED = true;
    public static boolean FIRE_CRASH_REPORTER_ENABLED = true;


    public static void LogDebug(String TAG, String Message){
        if(ENABLED)
            Log.d(TAG, Message);
    }

    public static void LogError(String TAG, String Message){
        if(ENABLED)
            Log.e(TAG, Message);
    }

    public static void FireLog(String TAG, String Message){
        if(FIRE_CRASH_LOG_ENABLED)
            FirebaseCrash.logcat(Log.DEBUG, TAG, Message);
    }

    public static void FireCrash(Throwable e){
        if(FIRE_CRASH_REPORTER_ENABLED)
            FirebaseCrash.report(e);
    }

    public static void LogWarn(String TAG, String MESSAGE, Exception e) {
        if(ENABLED)
            Log.w(TAG, MESSAGE, e);
    }
}
