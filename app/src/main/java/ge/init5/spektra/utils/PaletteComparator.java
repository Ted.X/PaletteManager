package ge.init5.spektra.utils;

import java.util.Comparator;
import java.util.Map;

import ge.init5.spektra.models.FirePalette;

/**
 * Created by redpix_ on 1/19/17.
 */

public class PaletteComparator implements Comparator<Map.Entry<String, FirePalette>>{

    public static final int NO_SORT = 0;
    public static final int SORT_BY_TITLE = 1;
    public static final int SORT_BY_HEARTS_COUNT = 2;
    public static final int SORT_BY_DATE = 3;
    int orderBy;

    public PaletteComparator(int orderBy) {
        this.orderBy = orderBy;
    }

    @Override
    public int compare(Map.Entry<String, FirePalette> map1, Map.Entry<String, FirePalette> map2) {
        FirePalette o1 = map1.getValue();
        FirePalette o2 = map2.getValue();
        return o1.myCompareTo(o2, orderBy);
    }

}
