package ge.init5.spektra.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.getkeepsafe.taptargetview.TapTargetView;

import ge.init5.spektra.MainActivity;
import ge.init5.spektra.R;
import ge.init5.spektra.palettes.palette.PaletteHolder;
import ge.init5.spektra.services.FirebasePaletteDatabase.PaletteBase;

/**
 * Created by __ted__ on 3/5/17.
 */

public class Tutorial {

    private static String TOOLBAR_ITEMS = "tutorial_toolbar_items";
    private static String FAB_BUTTON = "tutorial_fab_button";
    private static String MY_PALETTE_CARD = "tutorial_my_palette_card";


    public static void myPaletteCardTutorial(Context context, final PaletteHolder holder){

        boolean myPaletteCard = getBoolean(MY_PALETTE_CARD);

        if(myPaletteCard) {
            //TODO Create Tutorial Class
            new TapTargetSequence(((MainActivity) context))
                    .targets(
                            TapTarget.forView(holder.shareButton,
                                    "Share Button", "Share your color values")
                                    .outerCircleColor(R.color.md_teal_300)
                                    .targetCircleColor(R.color.md_yellow_300)
                                    .textColor(android.R.color.white).cancelable(false),

                            TapTarget.forView(holder.copyButton,
                                    "Copy Button", "Copy your color values such ass Hexes or RGB's")
                                    .outerCircleColor(R.color.md_yellow_700)
                                    .targetCircleColor(R.color.grey)
                                    .textColor(android.R.color.white).cancelable(false),

                            TapTarget.forView(holder.editButton,
                                    "Edit Button", "Edit Your Palette")
                                    .outerCircleColor(R.color.md_red_700)
                                    .targetCircleColor(R.color.accent_light)
                                    .textColor(android.R.color.white).cancelable(false),

                            TapTarget.forView(holder.deleteButton,
                                    "Delete Button", "Delete Palette")
                                    .outerCircleColor(R.color.com_facebook_button_send_background_color)
                                    .targetCircleColor(R.color.black)
                                    .textColor(android.R.color.white).cancelable(false),

                            TapTarget.forView(holder.mImageViews.get(0),
                                    "Select Color", "and show values of it")
                                    .outerCircleColor(R.color.accent)
                                    .textColor(android.R.color.white)
                                    .targetRadius(60)
                                    .transparentTarget(true)
                                    .tintTarget(false).cancelable(false)
                    )
                    .listener(new TapTargetSequence.Listener() {
                        // This listener will tell us when interesting(tm) events happen in regards
                        // to the sequence
                        @Override
                        public void onSequenceFinish() {
                            // Yay
                            holder.mImageViews.get(0).performClick();
                        }

                        @Override
                        public void onSequenceStep(TapTarget lastTarget) {
                            // Perfom action for the current target
                        }

                        @Override
                        public void onSequenceCanceled(TapTarget lastTarget) {
                            // Boo
                        }
                    }).start();
            seen(MY_PALETTE_CARD);
        }
    }

    public static void fabButtonTutorial(final MainActivity activity) {

        boolean fabButton = getBoolean(FAB_BUTTON);

        if(fabButton) {
            final View v = activity.findViewById(R.id.fab);

            TapTargetView.showFor(activity,
                    TapTarget.forView(v,
                            "Click Here to Create Palette",
                            "You can pick image from gallery, take one from camera or create palette manually")
                            // All options below are optional
                            .outerCircleColor(R.color.accent)
                            .targetCircleColor(R.color.white)
                            .titleTextSize(25)
                            .titleTextColor(R.color.white)
                            .descriptionTextSize(15)
                            .descriptionTextColor(R.color.white)
                            .textColor(R.color.white)
                            .textTypeface(Typeface.SANS_SERIF)
                            .dimColor(R.color.black)
                            .drawShadow(true)
                            .cancelable(false)
                            .tintTarget(false)
                            .transparentTarget(true),
                    new TapTargetView.Listener() {
                        @Override
                        public void onTargetClick(TapTargetView view) {
                            super.onTargetClick(view);
                            toolbarButtonTutorial(activity);
                        }
                    });
            seen(FAB_BUTTON);
        }
    }


    public static void toolbarButtonTutorial(MainActivity activity){

        boolean t = getBoolean(TOOLBAR_ITEMS);


        if(t){

            Toolbar toolbar = (Toolbar) activity.findViewById(R.id.toolbar);

            new TapTargetSequence(activity)
                    .targets(
                            TapTarget.forToolbarMenuItem(toolbar, R.id.action_search,
                                    "Search Button", "Search palette with name or tag")
                                    .outerCircleColor(R.color.md_yellow_900)
                                    .targetCircleColor(R.color.primary)
                                    .tintTarget(false)
                                    .textColor(android.R.color.white).cancelable(false),

                            TapTarget.forToolbarMenuItem(toolbar, R.id.action_filter,
                                    "Filter Button", "Order by Title, Heart Count or Date")
                                    .outerCircleColor(R.color.com_facebook_blue)
                                    .targetCircleColor(R.color.primary)
                                    .tintTarget(false)
                                    .textColor(android.R.color.white).cancelable(false)
                    )
                    .listener(new TapTargetSequence.Listener() {

                        @Override
                        public void onSequenceFinish() {}

                        @Override
                        public void onSequenceStep(TapTarget lastTarget) {}

                        @Override
                        public void onSequenceCanceled(TapTarget lastTarget) {}

                    }).start();

            seen(TOOLBAR_ITEMS);
        }
    }


    private static boolean getBoolean(String key){
        return PaletteBase.getInstance().sharedPreferences.getBoolean(key, true);
    }

    private static void seen(String key){
        PaletteBase.getInstance().sharedPreferences.edit().putBoolean(key, false).apply();
    }
}
