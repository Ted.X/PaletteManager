package ge.init5.spektra.utils.colorUtils;

import android.graphics.Color;

import ge.init5.spektra.models.ColorModel;
import ge.init5.spektra.services.FirebasePaletteDatabase.PaletteBase;

/**
 * Created by __ted__ on 12/18/16.
 * Static class for converting between android int, rgb, hsv, hex
 */

public final class ColorConverter {

    public static final String TAG = ColorConverter.class.getSimpleName();

    /**
     * Uses Android Color class to extract red, green and blue values from Color int and return them
     * as array of short rgb values.
     * @param val android.graphics.Color's int value
     * @return short[] { red, green, blue }
     */
    public static short[] InttoRGB(int val){
        return new short[]{ (short)Color.red(val), (short)Color.green(val), (short)Color.blue(val) };
    }

    /**
     * Uses InttoRGB static method to get RGB from int and then converts RGB to HSV using also
     * static method RGBtoHSV
     * @param val android.graphics.Color's int value
     * @return float[] { hue, saturation, value }
     */
    public static float[] IntToHSV(int val){
        short[] rgb = InttoRGB(val);
        return RGBtoHSV(rgb);
    }

    /**
     * Uses InttoRGB static method to get RGB from int nad then converts RGB to HEX using also
     * static method RGBtoHEX
     * @param val android.graphics.Color's int value
     * @return String "FFFFFF" without #
     */
    public static String IntToHEX(int val){
        return String.format("%06X", 0xFFFFFF & val);
    }

    /**
     * Uses Android Color class to get Color int value from passed RGB with alpha of one
     * @param r red
     * @param g green
     * @param b blue
     * @return int color value
     */
    public static int RGBtoInt(short r, short g, short b){
        return Color.argb(255, r, g, b);
    }

    /**
     * Uses RGBtoInt static method but with short[] rgb params
     * @param rgb short[] { red, green, blue }
     * @return int color value
     */
    public static int RGBtoInt(short[] rgb){
        return RGBtoInt(rgb[0], rgb[1], rgb[2]);
    }

    /**
     * Uses RGBtoInt static method but with string rgb params
     * @param r red
     * @param g green
     * @param b blue
     * @return int color value
     */
    public static int RGBtoInt(String r, String g, String b){

        short red = r.length() > 0 ? Short.parseShort(r) : 0;
        short green = g.length() > 0 ? Short.parseShort(g) : 0;
        short blue = b.length() > 0 ? Short.parseShort(b) : 0;

        return RGBtoInt(red, green, blue);
    }

    /**
     * Uses an algorithm to computer Hue, Saturation and Value of given RGB color
     * @param r red
     * @param g green
     * @param b blue
     * @return float[] { hue, saturation, value }
     */
    public static float[] RGBtoHSV(short r, short g, short b){

        // Normalize colors
        float rPrime = ((float)r/255.0f);
        float gPrime = ((float)g/255.0f);
        float bPrime = ((float)b/255.0f);

        // Find minimum and maximum
        float cMax = Math.max(rPrime, Math.max(gPrime, bPrime));
        float cMin = Math.min(rPrime, Math.min(gPrime, bPrime));

        // Find Delta
        float deltaMinMax = cMax - cMin;

        // Calculate hue saturation and set value
        float hue = 0;
        float saturation = (cMax == 0) ? 0 : deltaMinMax / cMax;
        float value = cMax;

        if (deltaMinMax == 0)
            hue = 0;
        else if (cMax == rPrime)
            hue = (gPrime - bPrime) / deltaMinMax;
        else if (cMax == gPrime)
            hue = (bPrime - rPrime) / deltaMinMax + 2;
        else if (cMax == bPrime)
            hue = (rPrime - gPrime) / deltaMinMax + 4 ;
        hue *=60;
        if (hue < 0)
            hue += 360;


        return new float[] {
                (float) (Math.round(hue * 100.0) / 100.0),
                (float) (Math.round(saturation * 1000.0) / 1000.0),
                (float) (Math.round(value * 1000.0) / 1000.0)
        };
    }

    /**
     * Uses RGBtoHSV static method but with short[] rgb params
     * @param rgb short[] { red, green, blue }
     * @return float[] { hue, saturation, value }
     */
    public static float[] RGBtoHSV(short[] rgb){
        return RGBtoHSV(rgb[0], rgb[1], rgb[2]);
    }

    /**
     * Uses private static method InttoHexString and concatenates strings creating RGB in HEX
     * @param r red
     * @param g green
     * @param b blue
     * @return String "FFFFFF" without #
     */
    public static String RGBtoHEX(short r, short g, short b){
        return IntToHEX(RGBtoInt(r, g, b));
    }

    /**
     * Uses RGBtoHEX static method but with short[] rgb params
     * @param rgb
     * @return String "FFFFFF" without #
     */
    public static String RGBtoHEX(short[] rgb){
        return RGBtoHEX(rgb[0], rgb[1], rgb[2]);
    }


    public static String RGBtoString(short[] rgb){
        return String.format("(%s, %s, %s)", rgb[0], rgb[1], rgb[2]);
    }

    /**
     * Uses HSVtoRGB static method to get RGB from HSV and then RGBtoInt static method to get int
     * color value from RGB values
     * @param h hue
     * @param s saturation
     * @param v value
     * @return int color value
     */
    public static int HSVtoInt(float h, float s, float v) {
        short[] rgb = HSVtoRGB(h, s, v);
        return RGBtoInt(rgb);
    }

    /**
     * Uses HSVtoRGB static method to get RGB from HSV and then RGBtoInt static method to get int
     * color value from RGB values
     * @param h hue
     * @param s saturation
     * @param v value
     * @return int color value
     */
    public static int HSVtoInt(String h, String s, String v) {

        float hue = h.length() > 0 ? Float.parseFloat(h) : 0;
        float sat = s.length() > 0 ? Float.parseFloat(s) : 0;
        float val=  v.length() > 0 ? Float.parseFloat(v) : 0;
        short[] rgb = HSVtoRGB(hue, sat, val);
        return RGBtoInt(rgb);
    }

    /**
     * Uses HSVtoInt static method but with float[] hsv params
     * @param hsv float[] { hue, saturation, value }
     * @return int color value
     */
    public static int HSVtoInt(float[] hsv){
        return HSVtoInt(hsv[0], hsv[1], hsv[2]);
    }

    /**
     * Uses an algorithm to compute RGB values from given HSV values
     * @param h hue
     * @param s saturation
     * @param v value
     * @return short[] { red, green, blue }
     */
    public static short[] HSVtoRGB(float h, float s, float v){

        float C = v * s;
        float X = C * (1 - Math.abs(((h/ 60) % 2) - 1));
        float m = v - C;

        float rPrime = 0f;
        float gPrime = 0f;
        float bPrime = 0f;

        if (h >= 0 && h < 60){
            rPrime = C;
            gPrime = X;
        }else if (h >= 60 && h < 120){
            rPrime = X;
            gPrime = C;
        }else if (h >= 120 && h < 180){
            gPrime = C;
            bPrime = X;
        }else if (h >= 180 && h < 240){
            gPrime = X;
            bPrime = C;
        }else if (h >= 240 && h < 300){
            rPrime = X;
            bPrime = C;
        }else if (h >= 300 && h < 360){
            rPrime = C;
            bPrime = X;
        }

        return new short[] {
                (short)((rPrime + m) * 255),
                (short)((gPrime + m) * 255),
                (short)((bPrime + m) * 255) };
    }

    /**
     * Uses HSVtoRGB static method to computer RGB values from given HSV values but with float[]
     * params
     * @param hsv float[] { hue, saturation, value }
     * @return short[] { red, green, blue }
     */
    public static short[] HSVtoRGB(float[] hsv){
        return HSVtoRGB(hsv[0], hsv[1], hsv[2]);
    }

    /**
     * Uses HSVtoRGB static method to get rgb values from hsv and then uses also static method
     * RGBtoHEX to get String hex value of given hsv
     * @param h hue
     * @param s saturation
     * @param v value
     * @return
     */
    public static String HSVtoHEX(float h, float s, float v){
        short[] rgb = HSVtoRGB(h, s, v);
        return RGBtoHEX(rgb);
    }

    /**
     * Uses HSVtoHEX static method but with float[] params
     * @param hsv float[] { hue, saturation, value }
     * @return String "FFFFFF" without #
     */
    public static String HSVtoHEX(float[] hsv){
        return HSVtoHEX(hsv[0], hsv[1], hsv[2]);
    }

    public static String HSVtoString(float[] hsv){
        return String.format("(%s, %s, %s)", hsv[0], hsv[1], hsv[2]);
    }


    /**
     * Uses HEXtoRGB static method to get rgb from hex and then RGBtoInt static method to return
     * int color value
     * @param hex String "FFFFFF" without #
     * @return int color value
     */
    public static int HEXtoInt(String hex){
        short[] rgb = HEXtoRGB(hex);
        return RGBtoInt(rgb);
    }

    /**
     * Uses Color validator and parseShort to get RGB values from hex String
     * @param hex String "FFFFFF" without #
     * @return short[] { red, green, blue }
     */
    public static short[] HEXtoRGB(String hex){
        if (ColorValidator.arrayHexCharsValid(hex)){
            short r = Short.parseShort(String.valueOf("" + hex.charAt(0) + hex.charAt(1)), 16);
            short g = Short.parseShort(String.valueOf("" + hex.charAt(2) + hex.charAt(3)), 16);
            short b = Short.parseShort(String.valueOf("" + hex.charAt(4) + hex.charAt(5)), 16);
            return new short[] { r, g, b };
        }
        return new short[] {0, 0, 0};
    }

    /**
     * Uses HEXtoRGB static method to get rgb from hex string and then RGBtoHSV to get hsv values
     * @param hex String "FFFFFFF" without #
     * @return float[] { hue, saturation, value }
     */
    public static float[] HEXtoHSV(String hex){
        short[] rgb = HEXtoRGB(hex);
        return RGBtoHSV(rgb);
    }

    public static String HEXtoString(String hex){
        return "#" + hex;
    }


    public static String fixRange(String string, int val, int min, int max){
        if(val < min)
            string = min+"";
        else if(val > max)
            string  = max+"";
        return string;
    }

    public static String fixRange(String string, float val, float min, float max){
        if (val < min)
            string = min + "";
        else if(val > max)
            string  = max+"";
        return string;
    }


    public static short fixRange(short val, int min, int max){
        if (val < min)
            val = (short) min;
        else if (val > max)
            val = (short)max;

        return val;
    }

    public static int fixRange(int val, int min, int max){
        if (val < min)
            val = min;
        else if (val > max)
            val = max;
        return val;
    }


    public static String RGBtoColorName(short[] rgb) {
        double closestDistance = Double.MAX_VALUE;
        String name = "";
        for (ColorModel color : PaletteBase.getInstance().colorNameDatabase) {
            double distance = distance(rgb, color.ToRGB());
            if (distance < closestDistance) {
                name = color.label;
                closestDistance = distance;
            }
        }
        return name;
    }


    private static double distance(short[] colorA, short[] colorB)
    {
        int redDist = Math.abs(colorA[0] - colorB[0]);
        int greenDist = Math.abs(colorA[1] - colorB[1]);
        int blueDist = Math.abs(colorA[2] - colorB[2]);
        return Math.sqrt(redDist * redDist + greenDist * greenDist + blueDist * blueDist);
    }

}
