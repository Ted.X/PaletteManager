package ge.init5.spektra.utils.colorUtils;

/**
 * Created by __ted__ on 12/18/16.
 */

public final class ColorValidator {
    // -------- Validators --------
    public static boolean isValidRGB(short r, short g, short b) {
        return (r >= 0 && r <= 255 && g >= 0 && g <= 255 && b >= 0 && b <= 255);
    }

    public static boolean isValidHSV(float h, float s, float v){
        return (h >= 0f && h <= 360f && s >= 0f && s <= 1f && v >= 0f && v <= 1f);
    }

    /**
     * Check hex array for valid hex numbers
     * @param charArray
     * @return
     */
    public static boolean arrayHexCharsValid(String charArray){
        if (charArray.length() != 6)
            return false;

        String valid = "0123456789ABCDEF";

        for (int i = 0; i < charArray.length(); i++) {
            if(!valid.contains((charArray.charAt(i)+"").toUpperCase())){
                return false;
            }
        }
        return true;
    }
}
